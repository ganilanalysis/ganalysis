# - Find MFM library
# This module tries to find the MFM lib installation on your system.
# It tries to find the gru-config script which gives you all the needed information.
# If the system variable MFMDIR is set this is straight forward.
# If not the module uses the pathes given in MFM_CONFIG_SEARCHPATH.
# If you need an other path you should add this path to this variable.  
# The root-config script is then used to detect basically everything else.
# This module defines a number of key variables and macros.

# Antoine Lemasson 15/4/2014


MESSAGE(STATUS "Looking for MFM ()...")

 # SET(MFM_CONFIG_SEARCHPATH
 #   $ENV{MFM_DIR}/bin
 #   $ENV{MFM_DIR}/config
 #   )
 # 
 # 
 # SET(MFM_DEFINITIONS "")
 # 
 # SET(MFM_INSTALLED_VERSION_TOO_OLD FALSE)
 # 
 # SET(MFM_CONFIG_EXECUTABLE MFM_CONFIG_EXECUTABLE-NOTFOUND)
 # 
 # FIND_PROGRAM(MFM_CONFIG_EXECUTABLE NAMES mfm-config PATHS
 #   ${MFM_CONFIG_SEARCHPATH}
 #   NO_DEFAULT_PATH)
 # 
 # 
 # IF (${MFM_CONFIG_EXECUTABLE} MATCHES "MFM_CONFIG_EXECUTABLE-NOTFOUND")
 #   MESSAGE( FATAL_ERROR "MFM not installed in the searchpath and MFMDIR is not set. Please
 #  set MFMDIR or add the path to your MFM installation in the Macro FindMFM.cmake in the
 #  subdirectory cmake/modules.")
 # ENDIF (${MFM_CONFIG_EXECUTABLE} MATCHES "MFM_CONFIG_EXECUTABLE-NOTFOUND")  
 # 
 # IF (MFM_CONFIG_EXECUTABLE)
 # 
 # IF(WITH_MFMCONF)
 #   SET(MFM_FOUND FALSE)
 #    MESSAGE(STATUS "Looking for MFM... - found $ENV{MFMDIR}/")
 #    
 #    EXEC_PROGRAM(${MFM_CONFIG_EXECUTABLE} ARGS "--version" OUTPUT_VARIABLE MFMVERSION)
 #    IF (NOT MFMVERSION)
 #       MESSAGE(FATAL_ERROR "Could not determine the version of MFM")
 #    ENDIF (NOT MFMVERSION)
 #    MESSAGE(STATUS "Looking for MFM... - version ${MFMVERSION} ")   
 #    
 #    # we need at least version 13/03, could be set outside
 #    IF (NOT MFM_MIN_VERSION)
 #      SET(MFM_MIN_VERSION "13.03")
 #    ENDIF (NOT MFM_MIN_VERSION)
 #  
 #    # now parse the parts of the user given version string into variables
 #    STRING(REGEX REPLACE "^([0-9][0-9]+)\\.[0-9][0-9]+" "\\1" req_gru_major_vers "${MFM_MIN_VERSION}")
 #    STRING(REGEX REPLACE "^[0-9][0-9]+\\.([0-9][0-9])+" "\\1" req_gru_minor_vers "${MFM_MIN_VERSION}")
 #    #  STRING(REGEX REPLACE "^[0-9]+\\.[0-9][0-9]+\\/([0-9][0-9]+)" "\\1" req_gru_patch_vers "${MFM_MIN_VERSION}")
 #  
 #    # and now the version string given by gru-config
 #    STRING(REGEX REPLACE "^([0-9][0-9]+)\\.[0-9][0-9]+" "\\1" found_gru_major_vers "${MFMVERSION}")
 #    STRING(REGEX REPLACE "^[0-9][0-9]+\\.([0-9][0-9])+" "\\1" found_gru_minor_vers "${MFMVERSION}")
 #    #  STRING(REGEX REPLACE "^[0-9]+\\.[0-9][0-9]+\\/([0-9][0-9]+).*" "\\1" found_gru_patch_vers "${MFMVERSION}")
 #  
 #    IF (found_gru_major_vers LESS 12)
 #      MESSAGE( FATAL_ERROR "Invalid MFM version \"${MFMVERSION}\", at least major version 12 is required, e.g. \"12.00\"")
 #    ENDIF (found_gru_major_vers LESS 12)
 #    # compute an overall version number which can be compared at once
 #    MATH(EXPR req_vers "${req_gru_major_vers}*100 + ${req_gru_minor_vers}")
 #    MATH(EXPR found_vers "${found_gru_major_vers}*100 + ${found_gru_minor_vers}")
 #    IF (found_vers LESS req_vers)
 #      SET(MFM_FOUND FALSE)
 #  	 MESSAGE( FATAL_ERROR "Invalid MFM version \"${MFMVERSION}\", minimum vesion \"${MFM_MIN_VERSION}\" is required")
 #      SET(MFM_INSTALLED_VERSION_TOO_OLD TRUE)
 #    ELSE (found_vers LESS req_vers)
 #      SET(GRU_FOUND TRUE)
 #    ENDIF (found_vers LESS req_vers)
 # 
 # ELSE(WITH_MFMCONF)	
 #   SET(MFM_FOUND TRUE)	
 # ENDIF(WITH_MFMCONF) 
 # ENDIF (MFM_CONFIG_EXECUTABLE)
 # 
 # 
 # IF (MFM_FOUND)
 # 
 # #### Temporary
 # IF(WITH_MFMCONF)
 # 
 #    # Set MFM_LIBRARY_DIR
 #    EXEC_PROGRAM( ${MFM_CONFIG_EXECUTABLE}
 #      ARGS "--libdir"
 #      OUTPUT_VARIABLE MFM_LIBRARY_DIR_TMP )
 #    
 #    IF(EXISTS "${MFM_LIBRARY_DIR_TMP}")
 #      SET(MFM_LIBRARY_DIR ${MFM_LIBRARY_DIR_TMP} )
 #    ELSE(EXISTS "${MFM_LIBRARY_DIR_TMP}")
 #      MESSAGE(FATAL_ERROR "${MFM_CONFIG_EXECUTABLE} reported ${MFM_LIBRARY_DIR_TMP} as library path, but ${MFM_LIBRARY_DIR_TMP} does NOT exist, MFM must NOT be installed correctly.")
 #    ENDIF(EXISTS "${MFM_LIBRARY_DIR_TMP}") 
 #    
 #    # Set MFM_BIN_DIR
 #    EXEC_PROGRAM(${MFM_CONFIG_EXECUTABLE}
 #      ARGS "--bindir"
 #      OUTPUT_VARIABLE gru_bins )
 #    SET(MFM_BIN_DIR ${gru_bins})
 #  	 
 #    # Set MFM_INCLUDE_DIR
 #    EXEC_PROGRAM( ${MFM_CONFIG_EXECUTABLE}
 #      ARGS "--incdir" 
 #      OUTPUT_VARIABLE gru_headers )
 #    SET(MFM_INCLUDE_DIR ${gru_headers})
 #    SET(MFM_INCLUDES ${gru_headers})
 #    
 #    EXEC_PROGRAM( ${MFM_CONFIG_EXECUTABLE}
 #      ARGS "--libs" 
 #      OUTPUT_VARIABLE gru_flags )
 #    
 #    SET(MFM_LIBRARIES ${gru_flags})
 #    MESSAGE(STATUS "Looking for MFM... - found $ENV{MFMDIR}/bin/MFM")
 # 
 # 
 # ELSE(WITH_MFMCONF)
 #   # Set MFM_LIBRARY_DIR

  SET(MFM_LIBRARY_DIR ${MFM_DIR}/lib)
  # Set MFM_BIN_DIR
  SET(MFM_BIN_DIR ${MFM_DIR}/bin)
  # Set MFM_INCLUDE_DIR
  SET(MFM_INCLUDES ${MFM_DIR}/include)
  SET(MFM_INCLUDE_DIRS  ${MFM_DIR}/include )
  
  SET(MFM_LIBRARIES "-L${MFM_LIBRARY_DIR} -lMFM")
  MESSAGE(STATUS "Looking for MFM... - found ${MFM_LIBRARY_DIR}")
  MESSAGE(STATUS " INCLUDE  ${MFM_INCLUDE_DIR}")
  
#ENDIF(WITH_MFMCONF)

  # Make variables changeble to the advanced user
#  MARK_AS_ADVANCED( MFM_LIBRARY_DIR MFM_INCLUDE_DIR MFM_DEFINITIONS)

#ENDIF(MFM_FOUND)
