/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *       
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef __GUser__
#define __GUser__

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
using std::ostream;
#include <TRint.h>
#include "General.h"
#include "GAcq.h"
#include "GDevice.h"
#include <TObject.h>
#include <TString.h>
#include <TH1.h>
#include <TH3.h>
#include <TH2.h>
#include <sys/stat.h>

//#include "GDevice.h"
#include "GParaCaliXml.h"
//#include "GMatacq.h"
#include "GNetServerRoot.h"


#ifndef WITH_NFS
#ifdef WITH_BLM
#include "SimpleAnalysis.hh"
#else
#ifdef WITH_GANA
#include "SimpleAnalysis.hh"
#else
#include "SimpleAnalysis.hh"
#endif
#endif
#else
#include "SimpleAnalysis.hh"
#endif

#ifdef WITH_MFM
#include "GMFMFile.h"
#include "GEventMFM.h"
#include "MFMBasicFrame.h"
#endif

#ifdef WITH_MESYTEC_DATA

#endif

#ifdef WITH_BLM
#include "BLM.hh"
#endif
#include "Defines.hh"
#include "Parameters.hh"
#include "Map.hh"
#ifdef WITH_ROOT
#include "HistoManager.hh"
#endif

using namespace std;
//____________________

//_________________________________________________________________________________________
/*! GUser */

class GUser : public  GAcq{

 
 protected:

  int fMyLabel;
  TString fMyParameterName;
  
  GSpectra* MySpectraList  ; 		
  GNetServerRoot* MySpectraServer ;	
  
  
private:

#ifdef WITH_AGATA
  AGATA *Agata;
  TimeStamp *VTS;
  TimeStamp *LTS;
#endif
 
#ifdef WITH_BLM
  BLM *DBLM;
#endif

public:
   
  GUser(GDevice* _fDevIn= NULL, GDevice* _fDevOut= NULL) ;   // default constructor of GUser object 
  ~GUser() ; 
  


  Parameters *PL;
  Map *M;
#ifdef WITH_ROOT
  HistoManager *H;
#endif
#ifndef WITH_NFS
  Analysis *An;
#else
  Analysis *An;
#endif

  UShort_t Data[2];
  MOFile *L;

  TFile *MyFile;
  TTree *MyTree;

  clock_t start, start_run, end;

  /// Return Label/Data couple at Pos
  UShort_t *GetDataAt(UShort_t Pos); 
  virtual Analysis* GetAnalysis(void){return An;};
  virtual void InitUser();
  virtual void InitUserRun();
  virtual void User();
  
#ifdef WITH_MFM
  virtual void CheckAndAddEBYEData(Analysis* An, MFMEbyedatFrame *AFrame);
#endif
  void RetriveHistograms();
  void RetrivePointers();
  void SetCalcMode(Bool_t v) {CalcMode = v;}
  // template <class T>
  //   void GetHistogram(T * Ptr);
  
  Int_t KBHit(void);
  Int_t NbOfHits;
  Int_t NbEvents;
  
  Bool_t CalcMode;

  Int_t fBadMerge;


#ifdef TRIG_PAT
  Int_t m_Pattern;
  Int_t m_PatMask;
  vector<string> m_TrigName;
  vector<Int_t> m_TrigLbl;
#endif

#ifdef WITH_MFM

  uint64_t  CurrentTS;
  uint64_t  PreviousTS;
  uint64_t  FirstTS;
  MFMCommonFrame * frame;
  MFMEbyedatFrame * ebyframe;
  Bool_t m_hasEBYEDAT;
  ULong64_t m_MFM_TS;
#endif
 UShort_t *LabelData;
 
  virtual void EndUserRun();
  void EndUser();
  
  virtual void InitTTreeUser(); 

#ifdef WITH_FST_DIGIT
  const int FalstaffParOffSet = 20000;
#endif
  
};

#endif
 
