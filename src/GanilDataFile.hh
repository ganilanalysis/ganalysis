/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *       
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _GANILDATAFILE_CLASS
#define _GANILDATAFILE_CLASS
/**
 * @file   GanilDataFile.hh
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   December, 2012
 * @brief  Ganil Data Class Header
 * 
 * 
 */

#include "LocalDefines.hh"
#include "DefinesGT.hh"
#include "Parameters.hh"
/*! GanilDataFile */

class GanilDataFile 
{
public:
  /// Create the reading class over the given file with all defaults values
  GanilDataFile(const string filename);
  ~GanilDataFile(void);
  /// Close data file
  void Close(void);
  /// Close scaler File
  void CloseScaler(void);
  /// Copy Parameter Name, Small utility routine to copy a char string.
  char* CopyParam( char *Dest, char *Source) const;
  /// Return Label/Data couple at Pos
  UShort_t *GetDataAt(UShort_t Pos){return(Data+(2*Pos));}; 
  /// Read a Label/Data pair and put it into Data array and return true until end of event is reached
  bool GetEvent(void);
  /// Return event length
  UShort_t GetNItems(void){return NItems;}; 
  /// Return event length
  UShort_t GetEventLength(void){return EventLength;}; 
  /// Get Parameter Labels from File Header
  void GetParameters(Parameters *Par);
  /// Return Tape status
  Int_t GetStatus (void) const {return(fStatus);}
  /// Returns current run number
  Int_t GetRunNumber (void) const;
  /// Initialize scalers and scale FileUShort_t 
  void InitScaler(const string filename="");
  /// Init Class and defult Tape parameters
  void InitDefault (const Int_t argc=0, char **argv=NULL);
  /// Read an event on tape/file and put in into the event array return true until read fails 
  bool Next(void); 
  /// Open data file
  void Open(void);
  /// Return BytesRead
  inline Int_t GetMBytesRead (void) const {return(fBufSize*fBufferNumber/1024/1024);}
  // /// Return BytesRead
  inline Int_t GetFileSize (void) const {return(fFileSize/1024/1024);}
  inline Bool_t GetIsCtrl (void) const {return(fIsCtrl);}
  inline Int_t GetBadMerge (void) const {return(fBadMerge);}
#ifdef TRIG_PAT
  inline UShort_t GetTrigPat (void) const {return(Pattern);}
#endif
  
private: 
  /// Read a single buffer from file
  void ReadBuffer (void);
  /// Read Scaler Buffer
  bool ReadScalerBuffer(void);
  /// Read Next Event, return true until end of buffer is reached
  bool ReadNextEvent(void);
  /// Utility routine to read events from buffers of type EBEYDAT, return true until end of buffer is reached
  bool ReadNextEvent_EBEYEDAT (void);

public :
  string  fFileName;  ///< Filename, can be a tape drive
  Int_t   fStatus;  ///< Status, 0 is OK, any other value suspect
  Int_t   fLun;   ///< Logical number of the device
  gan_tape_desc *fDevice;  /// Device structure (ganil_tape interface)
  Int_t   fDensity;  ///< Tape density
  in2p3_buffer_struct *fBuffer; /// Brut data buffer (ganil_tape interface)
  char   *fStructEvent; /// ??? (ganil_tape interface)
  Int_t   fBufSize;  /// Buffer size
  Int_t   fCtrlForm;  ///< Fix/variable length events
  Int_t   fEvbsize;  ///< Size of the brut data buffer
  Int_t   fEvcsize;  ///< Size of the ctrl data buffer
  UShort_t  *fEventBrut;  /// Brut data buffer
  UShort_t  *fEventCtrl;  /// Control data buffer 
  bool   fAllocated;  ///< True if the Tape is allocated (useless?)
  Int_t   fRunNumber;  ///< current file run number
  char   fHeader[9];  ///< Buffer header
  Int_t   fEventNumber; ///< Local event number in current buffer (should be renamed)
  Int_t   fBufferNumber; ///< Buffer Number
  Int_t   fEventCount;  ///< Our event counter
  Int_t   fEventSkippedCount; ///< Our skipped event counter
  bool   fIsCtrl; ///< We are currently in a CTRL buffer
  FILE   *fpiScaler;   ///< Scaler Names file
  FILE   *fpoScaler;   ///< Scaler file
  string  fScalerFile;   ///< Scaler file name
  static const int  maxScalerChannels=3*40; ///< Max number of scalers
  char   chScaler[maxScalerChannels][10]; ///< Scaler Names 
  long int  nScaler; ///< Number of scalers
  UShort_t *Data; ///< Label/Data couple
  Int_t EventLength; ///< Eventlength
  Int_t NItems; ///< Number of Data/Label couples
  Int_t  fFileSize;  ///< Data FileSize
  Int_t  fBadMerge;  ///< Number of Events FileSize
#ifdef TRIG_PAT
  UShort_t Pattern; //< Trigger pattern
#endif
};
#endif
