/****************************************************************************
 *    Copyright (C) 2022 by Antoine Lemasson
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/

/**
 * @file   GAna.cpp
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date  Dec, 2022
 * @brief  GAna Software
 *
 */

#include "EnvManager.hh"
#include "LogManager.hh"
#include "DetManager.hh"
#include "ConfManager.hh"
#include "boost/program_options.hpp"
#include "SimpleAnalysis.hh"

namespace po = boost::program_options;

int main(int argc, char* argv[])
{

//    po::options_description desc("\nGana2\n\nUsage");

//    desc.add_options()
//            ("help", "produce this message")
//            ("run", po::value<int>(), "run nr")
//            ("cf", po::value<std::string>(), "config file name")
//            ("i", po::value<std::string>(), "input mfm file")
//            ("o", po::value<std::string>(), "output root file")
//            ("n", po::value<int>(), "Number of Frames")
//            ;

//    po::variables_map vm;
//    try
//    {
//        po::store(po::parse_command_line(argc, argv, desc), vm);
//        po::notify(vm);
//    }
//    catch(...)
//    {
//        // in case of unknown options, print help & exit
//        std::cout << desc << "\n";
//        return 0;
//    }

//    if (vm.count("help")) {
//        std::cout << desc << "\n";
//        return 0;
//    }

//    auto runNr = 0;
//    auto nframes = -1;
//    std::string configfile = "./Config/Conf.confs";
//    std::string filename = "";
//    std::string outfilename = "";

//    if(vm.count("run")) runNr = vm["run"].as<int>();
//    if(vm.count("cf")) configfile = vm["cf"].as<string>();
//    if(vm.count("i")) filename = vm["i"].as<string>();
//    if(vm.count("o")) outfilename = vm["o"].as<string>();
//    if(vm.count("n")) nframes = vm["n"].as<int>();

//    if(runNr>0)
//        std::cout << "Converting Run " << runNr << std::endl;
//    std::cout << " Config File " << configfile << std::endl;
//    std::cout << " Converting file :  " << filename << "->" << outfilename << std::endl;
//    std::cout << " Nframes  :  " << nframes << std::endl;



    LogManager *LM = LogManager::getInstance();
    DetManager *DM = DetManager::getInstance();
    EnvManager *EM = EnvManager::getInstance();
    ConfManager *CM = ConfManager::getInstance();
    EM->setPathVar("ANALYSIS");
    EM->setWatcherServerVar("WATCHER_SERVER");
    EM->print();
    Analysis *A = NULL;
    try
       {
          A = new Analysis(argc, argv);
          A = CleanDelete(A);

       }
    catch(MErr * Er)
       {
          if(Er->FunctionCode==1&&Er->ErrorCode==1)
            ;//O.K. to exit without a Message
          else
            {
               Er->Set(WhoamI);
               Er->Print();
            }
          Er = CleanDelete(Er);
       }
    catch(exception &e)
       {
          cout << "Caught Exception : " << e.what() << endl;
       }
    catch(...)
       {
          cout << "Unkown Exception : "  << endl;
       }


    DM->kill();
    LM->kill();
    CM->Kill();

    return 0;
}
