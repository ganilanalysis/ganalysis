/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *       
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "DataFile.hh"

DataFile::DataFile(const string filename)
{
  START;
  InitDefault();
  fFileName=filename;

  struct stat st;
  stat(fFileName.c_str(), &st);
  fFileSize = st.st_size;
  END;  
}

DataFile::~DataFile(void)
{
  START;
  END;  
}


void DataFile::InitDefault()
{
  START;
  BytesRead = 0;
  END;
}

void DataFile::UnPack(Analysis *An)
{
  START;
  END;
}


void DataFile::GetParametersFromActionFile(Parameters *Par,const Char_t* ActionFileName)
{
  START;

  vector<string> *ParName = new vector<string>();
  vector<UShort_t> *ParId = new vector<UShort_t>();
  vector<UShort_t> *ParSize = new vector<UShort_t>();
	
  Int_t NPars = 0;
  Int_t LineCtr = 0;
  Char_t Name[40];
  Int_t Label = 0;
  Int_t Bits = 0;
  Int_t Dummy = 0;

  MIFile *IF = NULL;
  IF = new MIFile(ActionFileName);

  cout << "<DataFile>  Reading Parameters file " << IF->GetFileName() << endl;
  char Line[255];
  UShort_t Len=255;
  
	while (IF->GetLine(Line,Len)) {
     LineCtr++;
    if((sscanf(Line, "%s\t%d\t%u\t%d",Name,&Label, &Bits, &Dummy) == 4)
        || (sscanf(Line, "%s\t%d\t%u\t%d",Name,&Label, &Bits, &Dummy) == 3))
       {
         ParName->push_back(Name);
         ParId->push_back(Label);
         ParSize->push_back(Bits);
         NPars++;   
       }
     else
       {
         Char_t Message[500];
         sprintf(Message,"Bad Format for action file %s line %d", IF->GetFileName(),LineCtr);
         MErr * Er = new MErr(WhoamI,0,0, Message);
         throw Er;
       }
   }
   IF = CleanDelete(IF);
   Par->SetNParameters(NPars);
   for(UShort_t i=0; i< ParName->size(); i++)
     {
       Par->Add(ParId->at(i),ParName->at(i),ParSize->at(i));
   }
  delete	 ParName;
  delete	 ParId;
  delete	 ParSize;
  
  END;
}

