/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *       
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _MFMDATAFILE_CLASS
#define _MFMDATAFILE_CLASS
/**
 * @file   MFMDataFile.hh
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   April, 2014
 * @brief  MFM Data File  Class Header
 * 
 * 
 */
#define SCALERS 1

#include "DataFile.hh"

#ifdef WITH_MFM
#include "MFMTypes.h"
#include "MFMCommonFrame.h"
#include "MFMBasicFrame.h"
#include "MFMBlobFrame.h"
#include "MFMExogamFrame.h"
#include "MFMEbyedatFrame.h"
#include "MFMMergeFrame.h"
#include "MFMCoboFrame.h"
#include "MFMOscilloFrame.h"
#include "MFMVamosPDFrame.h"
#include "MFMVamosICFrame.h"
#include "MFMReaGenericFrame.h"
#include "MFMMesytecFrame.h"
#ifdef WITH_BLM
#include "MFMS3SynchroFrame.h"
#include "MFMS3DeflectorFrame.h"
#include "MFMS3BaF2Frame.h"
#endif
#ifdef SCALERS
#include "MFMScalerDataFrame.h"
#endif
#ifdef WITH_BLM
#include "BLM.hh"
#endif
#ifdef WITH_PARIS
#include "MFMParisFrame.h"
#endif
#endif
class Analysis;
/*! MFMDataFile */
class MFMDataFile : public DataFile
{
 public:
  
  //! Constructor instantiation
  MFMDataFile(const string filename);
  virtual ~MFMDataFile();
  
  inline MFMCommonFrame* GetCurrentFrame(){return frame;};   
  Bool_t Next();
  /// Return Label/Data couple at Pos
  UShort_t *GetDataAt(UShort_t Pos); 
  /// Get Parameter Labels from File Header
  void GetParameters(Parameters *Par);
  /// Get Current Data TimeStamp
  uint64_t GetTimeStamp(void){return (CurrentTS-FirstTS);};
#ifdef TRIG_PAT
  inline UShort_t GetTrigPat (void)  {return(Pattern);}
#endif

#ifdef WITH_MFM
  void UnPack(Analysis* An, MFMCommonFrame *AFrame);
#endif
  uint64_t  CurrentTS;
  uint64_t  PreviousTS;
  uint64_t  FirstTS;


#ifdef WITH_BLM
  BLM *DBLM;
#endif

private:
  void InitDefault(void);
  Bool_t Open(void);
  Bool_t Close(void);
  
  
  int fLun; // File Descriptor
  int framesize;
  MFMCommonFrame * frame;
  MFMCommonFrame * pInsideFrame;
  MFMEbyedatFrame * ebyframe;
  MFMVamosICFrame* pVamosICFrame;
  MFMVamosPDFrame* pVamosPDFrame;
  MFMMergeFrame* pMergeFrame;
  MFMReaGenericFrame* pREAFrame;
  MFMMesytecFrame* pMesytecFrame;
  UShort_t *LabelData;
#ifdef WITH_BLM
  MFMS3DeflectorFrame *pDeflectorFrame;
  MFMS3BaF2Frame *pBAF2Frame;
  MFMS3SynchroFrame *pSynchroFrame;
#endif
#ifdef WITH_PARIS
  MFMParisFrame *pParisFrame;
#endif
  char* vector;
  char ** pvector;
  int nbofevent;
  int vectorsize;
  int minsizeheader;
  Int_t   fEventCount;  ///< Our event counter
  Int_t   fEventSkippedCount; ///< Our skipped event counter

#ifdef TRIG_PAT
  UShort_t Pattern; //< Trigger pattern
  UShort_t *Data; ///< Label/Data couple
  Int_t  fBadMerge;  ///< Number of Events FileSize
#endif
};
#endif
