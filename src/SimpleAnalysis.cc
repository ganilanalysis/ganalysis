/****************************************************************************
 *    Copyright (C) 2020 by Antoine Lemasson
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "SimpleAnalysis.hh"
using std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using std::chrono::duration;
using std::chrono::milliseconds;
Analysis::Analysis(int argc,char** argv)
{
    START;
    start = clock();
    if(argc < 2)
        Usage();

    InitialiseOptions();
    GetOptions(argc, argv);
    try
    {
        GetEnv();
    }
    catch(MErr * Er)
    {
        EM->kill();
        throw Er;
    }

    IFN = NULL;
    IFN = &IFName;
    OFN = NULL;
    OFN = &OFName;
    ParFN = NULL;
    ParFN = &ParFName;
    Det = NULL;
    Det = &Detectors;

    L=NULL;
    PL=NULL;

    // PM=NULL;
    DM=NULL;

    MyMap=NULL;
    GDF=NULL;
#ifdef WITH_ROOT
    H=NULL;
    OutTFile=NULL;
    OutTTree=NULL;
#endif
#ifdef WITH_MFM

    frame = NULL;
    frame = new MFMCommonFrame();
    ebyframe = NULL;
    ebyframe= new MFMEbyedatFrame();
    pInsideFrame = NULL;
    pInsideFrame = new MFMCommonFrame();

    pVamosICFrame = new  MFMVamosICFrame();
    pVamosPDFrame = new  MFMVamosPDFrame();
    pREAFrame = new MFMReaGenericFrame();
    pExogamFrame =  new MFMExogamFrame();

    pMergeFrame = new  MFMMergeFrame();
    pCoboFrame = new  MFMCoboFrame();

    pMesytecFrame = new  MFMMesytecFrame();
#endif

#ifdef WITH_PARIS
    pParisFrame  = new MFMParisFrame();
    ParisDet = NULL;
#endif
#ifdef WITH_PISTA
    PistaDet = NULL;
#endif
    ResetFileCounter();

    if(fAnMode)
    {
        if(!fModeWhich[0])
        {
            GetFileNames(argc, argv);
            PrintFileNames();
            ProcessFiles();
        }
        else if(fModeWhich[0])
        {
#ifndef NARVAL
            cout << "The library was not compiled with NARWAL Flag ! "<< endl;
#else
            cout << "    **********************************************" <<endl;
            cout << "    *              Narval  Watcher               *" << endl;
            cout << "    **********************************************" <<endl<< endl;
            // Get the Options
            // GetWatcherOptions(argc,argv);
            // RunWatcher();
#endif
        }
    }
    END;
}




Analysis::Analysis(MOFile *Log, Parameters *P, Int_t fMode)
{
    START;
    InitialiseOptions();
    fAnMode = fMode;
    LblDataBuf = AllocateDynamicArray<UShort_t>(2);

    L= NULL;
    L=Log;
    PL = NULL;
    PL = P;
    Det = NULL;
    Det = &Detectors;
    MyMap = NULL;
    MyMap = new Map(PL->GetMaxParameters());
#ifdef WITH_MFM

    frame = NULL;
    frame = new MFMCommonFrame();
    ebyframe = NULL;
    ebyframe= new MFMEbyedatFrame();
    pInsideFrame = NULL;
    pInsideFrame = new MFMCommonFrame();

    pVamosICFrame = new  MFMVamosICFrame();
    pVamosPDFrame = new  MFMVamosPDFrame();
    pREAFrame = new MFMReaGenericFrame();
    pExogamFrame =  new MFMExogamFrame();

    pMergeFrame = new  MFMMergeFrame();
    pCoboFrame = new  MFMCoboFrame();

    pMesytecFrame = new  MFMMesytecFrame();
#endif
    try
    {
        GetEnv();
    }
    catch(MErr * Er)
    {

        LblDataBuf = FreeDynamicArray<UShort_t>(LblDataBuf);
        MyMap = CleanDelete(MyMap);
        EM->kill();
        throw Er;

    }

    END;
}

#ifdef WITH_ROOT
Analysis::Analysis(Parameters *P, HistoManager* HM, Int_t fMode)
{
    START;
    InitialiseOptions();
    fAnMode = fMode;
    LblDataBuf = AllocateDynamicArray<UShort_t>(2);

    L= NULL;
    LogManager *LM = LogManager::getInstance();
    L = LM->GetFilePtr();
    PL = NULL;
    PL = P;
    H = NULL;
    H = HM;
    OutTFile=NULL;
    OutTTree=NULL;
#ifdef WITH_MFM
    frame = NULL;
    frame = new MFMCommonFrame();
    ebyframe = NULL;
    ebyframe= new MFMEbyedatFrame();
    pInsideFrame = NULL;
    pInsideFrame = new MFMCommonFrame();
    pVamosICFrame = new  MFMVamosICFrame();
    pVamosPDFrame = new  MFMVamosPDFrame();
    pREAFrame = new MFMReaGenericFrame();
    pExogamFrame =  new MFMExogamFrame();
    pMergeFrame = new  MFMMergeFrame();
    pCoboFrame = new  MFMCoboFrame();
    pMesytecFrame = new  MFMMesytecFrame();
#endif

    Det = NULL;
    Det = &Detectors;
    MyMap = NULL;
    MyMap = new Map(PL->GetMaxParameters(),60000);

    try
    {
        GetEnv();
#ifdef WITH_NUMEXO
        //cout << getenv("ANALYSIS") << endl;
        NTopo = NumexoTopology::getInstance();
        string NumexoTopoFile= AnalysisPath;
        NumexoTopoFile += CM->Get("NumexoTopologyFile");
        NTopo->Init(NumexoTopoFile.c_str());
        PL_NUMEXO = NUMEXOParameters::getInstance();
#endif
#ifdef WITH_GET
        GTopo = GETTopology::getInstance();
        string GetTopoFile= AnalysisPath;
        GetTopoFile += "/Calibs/GETTopology.txt";
        string GetTopoPath= AnalysisPath;
        GetTopoPath +="/Calibs";
        GTopo->Init(GetTopoFile.c_str(),GetTopoPath.c_str());
        PL_GET = GETParameters::getInstance();
#endif
#ifdef WITH_MESYTEC_DATA
        try{
            InitMesytecData(AnalysisPath);
        }
        catch (MErr * Er)
        {
            Er->Print();
            throw Er;
        }

        // Mesytec Topology // Crate map ?
        MesytecTopo = MesytecTopology::getInstance();
        string MesytecTopoFile= AnalysisPath;
        MesytecTopoFile += CM->Get("MesytecTopologyFile");
        MesytecTopo->Init(MesytecTopoFile.c_str());

        MesytecPar = MesytecParameters::getInstance();
//        MesytecPar->Print();
#endif
    }
    catch(MErr * Er)
    {
        LblDataBuf = FreeDynamicArray<UShort_t>(LblDataBuf);
        MyMap = CleanDelete(MyMap);
        EM->kill();
        throw Er;
    }


    END;
}

// Specific constructor for GammaWare
Analysis::Analysis(TTree *t, Parameters *P, HistoManager* HM)
{
    START;

    InitialiseOptions();
    fAnMode = 2;//fMode;
    LblDataBuf = AllocateDynamicArray<UShort_t>(2);
#ifdef WITH_MFM
    frame = NULL;
    frame = new MFMCommonFrame();
    ebyframe = NULL;
    ebyframe= new MFMEbyedatFrame();
    pInsideFrame = NULL;
    pInsideFrame = new MFMCommonFrame();

    pVamosICFrame = new  MFMVamosICFrame();
    pREAFrame = new MFMReaGenericFrame();
    pExogamFrame = new MFMExogamFrame();
    pVamosPDFrame = new  MFMVamosPDFrame();
    pMergeFrame = new  MFMMergeFrame();
    pCoboFrame = new  MFMCoboFrame();
    pMesytecFrame = new MFMMesytecFrame();
#endif
#ifdef WITH_PARIS
#ifdef WITH_MFM
    pParisFrame  = new MFMParisFrame();
#endif
    ParisDet = NULL;
#endif

    LogManager *LM = LogManager::getInstance();
    L = LM->GetFilePtr();
    PL = P;
    H = NULL;
    H = HM;

    H = NULL;
    H = new HistoManager();
    OutTFile=NULL;
    OutTTree=t;

    Det = NULL;
    Det = &Detectors;
    MyMap = NULL;
    MyMap = new Map(PL->GetMaxParameters());


    // Force reset if used with Start Batch
    ResetFileCounter();



    try
    {
        GetEnv();
#ifdef WITH_NUMEXO
        NTopo = NumexoTopology::getInstance();
        string NumexoTopoFile= AnalysisPath;
        NumexoTopoFile += CM->Get("NumexoTopologyFile");
        NTopo->Init(NumexoTopoFile.c_str());
        PL_NUMEXO = NUMEXOParameters::getInstance();

#endif
    }
    catch(MErr * Er)
    {
        LblDataBuf = FreeDynamicArray<UShort_t>(LblDataBuf);
        MyMap = CleanDelete(MyMap);
#ifdef WITH_MESYTEC_DATA
        MesytecTopo = CleanDelete(MesytecTopo);
#endif
        EM->kill();
        LM->kill();

        throw Er;
    }

    END;
}

#endif

Analysis::~Analysis()
{
    START;

    if(fAnMode == MODE_WATCHER || fAnMode == MODE_CALC)
    {
        LblDataBuf = FreeDynamicArray<UShort_t>(LblDataBuf);
        MyMap = CleanDelete(MyMap);
        DeleteDetectors();
    }
#ifdef WITH_MFM
    frame = CleanDelete(frame);
    ebyframe = CleanDelete(ebyframe);
    pInsideFrame = CleanDelete(pInsideFrame);
    pVamosICFrame = CleanDelete(pVamosICFrame);
    pVamosPDFrame = CleanDelete(pVamosPDFrame);
    pREAFrame = CleanDelete(pREAFrame);
    pExogamFrame = CleanDelete(pExogamFrame);
    pMergeFrame = CleanDelete(pMergeFrame);
    pCoboFrame = CleanDelete(pCoboFrame);
    pMesytecFrame = CleanDelete(pMesytecFrame);
#ifdef WITH_PARIS
    pParisFrame = CleanDelete(pParisFrame);
#endif
#endif
#ifdef WITH_NUMEXO
    if(NTopo) NTopo = CleanDelete(NTopo);
#endif
#ifdef WITH_GET
    if(GTopo) GTopo = CleanDelete(GTopo);
#endif
#ifdef WITH_MESYTEC_DATA
        if(MesytecTopo) MesytecTopo = CleanDelete(MesytecTopo);
#endif
    EM->kill();
    CM->Kill();
    DM->kill();
    PrintRateSummary();
    END;
}
void Analysis::InitialiseOptions()
{
    START;
    ConfigFile = "";
    TotalEvt = 0;
    RunEvt = 0;
    DuplicateCtr = 0;
    UnpackLevel = 0;
    fConfName =false;
    fOption=false;
    for(UShort_t i=0;i<2;i++)
        fOptionWhich[i] = false;

    fAnMode=0;
    for(UShort_t i=0;i<5;i++)
        fModeWhich[i] = false;
    END;
}
void Analysis::GetOptions(int argc,char** argv)
{
    START;
    if(strcmp("-d2Rr", argv[1])==0)
    {
        fModeWhich[1] = true;
        fAnMode = MODE_D2R;
    }
    else if (strcmp("-d2Ar", argv[1])==0)
    {
        fModeWhich[2] = true;
        fAnMode = MODE_D2A;
    }
    else
        Usage();


    for (int i = 2; i < argc; ++i) {
        std::string arg = argv[i];
        if ((arg == "-h") || (arg == "--help")) {
            Usage();
        } else if ((arg == "-cf") || (arg == "--config")) {
            if (i + 1 < argc) { // Make sure we aren't at the end of argv!
                ConfigFile = argv[++i]; // Increment 'i' so we don't get the argument as the next argv[i].
//                cout << "Found config File " << ConfigFile << endl;
                fOption = fOptionWhich[1] = true;

            } else { // Uh-oh, there was no argument to the destination option.
                std::cerr  << "--------------------------------------------" << endl;
                std::cerr << "  !!!!! Parsing Error : -cf option requires one argument." << std::endl;
                std::cerr  << "--------------------------------------------" << endl;
                Usage();
            }
        } else if ((arg == "-f")) {
            if (i + 2 < argc) { // Make sure we aren't at the end of argv!
//                cout << "Input File FIle " << argv[i++] << " OutFile " << argv[i++] << endl;
                fOption = fOptionWhich[0] = true;

            } else { // Uh-oh, there was no argument to the destination option.
                std::cerr  << "--------------------------------------------" << endl;
                std::cerr <<  " !!!!! Parsing Error : -f option requires two arguments." << std::endl;
                std::cerr  << "--------------------------------------------" << endl;
                Usage();
            }

        } else {
//            std::cerr  << "--------------------------------------------" << endl;
//            std::cerr  << " !!!!! Parsing Error : Unexpected Argument <" <<i << "> : " << arg << endl;
//            std::cerr  << "--------------------------------------------" << endl;
//            Usage();
        }
    }
    return;


    END;
}


void Analysis::Usage()
{
    START;

    cout <<
            "================================================================="
         << endl;
    cout << "";
    cout << "Usage: " << endl;

    cout << " Defined detectors : " << endl;
#ifdef WITH_EXOGAM
    cout << "  - EXOGAM defined "<<endl;
#endif
#ifdef WITH_VAMOS
    cout << "  - VAMOS defined "<<endl;
#endif
#ifdef WITH_PISTA
    cout << "  - PISTA defined "<<endl;
#endif
#ifdef WITH_LISE
    cout << "  - LISE defined "<<endl;
#endif
#ifdef WITH_CATS
    cout << "  - CATS defined "<<endl;
#endif
#ifdef WITH_PARIS
    cout << "  - PARIS defined "<<endl;
#endif
#ifdef WITH_BLM
    cout << "  - BLM defined "<<endl;
#endif
#ifdef WITH_NFS
    cout << "  - NFS defined "<<endl;
#endif
#ifdef WITH_FALSTAFF
    cout << "  - FALSTAFF defined "<<endl;
#endif
#ifdef WITH_FTS_DIGIT
    cout << "  - FTS_DIGIT defined "<<endl;
#endif
#ifdef WITH_NPTOOL
    cout << "  - NPTOOL defined "<<endl;
#endif
#ifdef WITH_MFM
    cout << "  - MFM defined "<<endl;
#endif
#ifdef WITH_MESYTEC_DATA
    cout << "  - MESYTEC_DATA defined "<<endl;
#endif
#ifdef WITH_NUMEXO
    cout << "  - NUMEXO defined "<<endl;
#endif
    cout << "GAna mode <options> RunNumbers/FileName " << endl;
    cout << "* Modes: " << endl;
    cout << "   -d2Rr <RunNr> -> Dat File to Raw Root File" << endl;
    cout << "   -d2Ar <RunNr> -> Dat File to Analysed Root File" << endl;
    cout << "* Options: " << endl;
    cout << "   -cf -> Explicit Config File (default ./Confs/Config.conf) " << endl;
    cout << "   -f -> Explicit file names are given " << endl;
    cout << "================================================================="
         << endl;

    Char_t Message[100];
    sprintf(Message," ");
    MErr * Er = new MErr(WhoamI,1,1, Message);
    throw Er;

    END;
}

void Analysis::GetEnv()
{
    START;
    EM = EnvManager::getInstance();
    cout << "Env Manager PATH in GetEnv " << EM->getPathVar() << endl;

    char *Path;
    char *Exp;
    Path = getenv((EM->getPathVar()).c_str());

    if(Path != NULL)
    {

        AnalysisPath = Path;
        cout <<
                "  <Analysis> ================================================================="
             << endl;
        cout << "  <Analysis> Default Analysis Path :";
        cout << AnalysisPath << endl;

        char cfile[500];

        CM = ConfManager::getInstance();
        if(!CM->IsInit)
        {
            if(ConfigFile.size() > 0)
                sprintf(cfile,"%s",ConfigFile.c_str());
            else
                sprintf(cfile,"%s/Confs/Config.conf",AnalysisPath.c_str());
            CM->Init(cfile);
        }
        CM->PrintEnv();


        cout << "  <Analysis> Default Calibration Path :";

        cout << AnalysisPath << CM->Get("Calib_Path") << endl;

        cout << "  <Analysis> Default Dat File Path :";

        cout << AnalysisPath << CM->Get("Raw_Path") <<endl;


        cout << "  <Analysis> Default Raw Root File Path :";
        cout << AnalysisPath << CM->Get("RootR_Path") <<endl;
        cout << "  <Analysis> Default Analysed Root File Path :";
        cout << AnalysisPath << CM->Get("RootA_Path") <<endl;

        cout << "  <Analysis> Analysis Exp Name : ";
        cout << "  <Analysis> Analysis Exp Name : ";
        Analysis_ExpName = CM->Get("ExpName");
        cout << Analysis_ExpName <<endl;

        cout << "  <Analysis> Analysis Exp Name : ";
        Analysis_ExpName = CM->Get("ExpName");
        cout << Analysis_ExpName <<endl;

        cout <<
                "  <Analysis> ================================================================="
             << endl;

    }
    else
    {
        Char_t Message[100];
        sprintf(Message,"Environment variable %s not defined !",(EM->getPathVar()).c_str());
        MErr * Er = new MErr(WhoamI,0,1000, Message);
        throw Er;
    }

    END;
}


void Analysis::PrintFileNames()
{
    START;
    cout << "  <Analysis> List of Input files : "  << endl;

    while(NextFile())
        cout << "     " << GetCurrentInputFile() << " -> " << GetCurrentOutputFile() << endl;
    cout << "  <Analysis> " << CurrentFile-1 << " files to be processed." << endl;
    ResetFileCounter();
    END;
}

void Analysis::GetFileNames(int argc,char** argv)
{
    START;
    string IF;
    string OF;

    if(fOption)
    {
        if(fOptionWhich[1])
        {
            if(fOptionWhich[0])
            {
                if((argc == 7))
                {
                    IF.clear();
                    IF = argv[5];
                    if(FileExists(IF.c_str()))
                    {
                        OF.clear();
                        OF = argv[6];
                        AddFiles(IF,OF);
                        return;
                    }
                    else
                    {
                        Char_t Message[100];
                        sprintf(Message,"Could not find the file : %s",IF.c_str());
                        MErr * Er = new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                    return;
                }
                else
                {
                    Char_t Message[100];
                    sprintf(Message,"Missing File Name ?");
                    MErr * Er = new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
            }

            if((argc >= 5))
            {
                for(UShort_t i = 4; i<argc;i++)
                    GetFileNamesForRuns(argv[i]);
                return;
            }
            else
            {
                Char_t Message[100];
                sprintf(Message,"Missing Run  Number ?");
                MErr * Er = new MErr(WhoamI,0,0, Message);
                throw Er;
            }
        }
        else
            if(fOptionWhich[0])
            {
                if((argc == 5))
                {
                    IF.clear();
                    IF = argv[3];
                    if(FileExists(IF.c_str()))
                    {
                        OF.clear();
                        OF = argv[4];
                        AddFiles(IF,OF);
                    }
                    else
                    {
                        Char_t Message[100];
                        sprintf(Message,"Could not find the file : %s",IF.c_str());
                        MErr * Er = new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                    return;
                }
                else
                {
                    Char_t Message[100];
                    sprintf(Message,"Missing File Name ?");
                    MErr * Er = new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
            }
    }
    if(argc >=3)
    {
        for(UShort_t i = 2; i<argc;i++)
            GetFileNamesForRuns(argv[i]);
    }
    else
    {
        Char_t Message[100];
        sprintf(Message,"Missing Run Numbers ?");
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    END;
}

void Analysis::GetFileNamesForRuns(char* RName)
{
    START;
    char *Ptr;
    UShort_t RNumber;
    UShort_t RNumber1;
    if((Ptr = strstr(RName,"-")))
    {
        RNumber=atoi(RName);
        if(!(RNumber>0))
        {
            Char_t Message[100];
            sprintf(Message,"Run Number Not Defined ?");
            MErr * Er = new MErr(WhoamI,0,0, Message);
            throw Er;
        }
        RNumber1=atoi(Ptr+1);
        if(!(RNumber1>0))
        {
            Char_t Message[100];
            sprintf(Message,"Run Number Not Defined ?");
            MErr * Er = new MErr(WhoamI,0,0, Message);
            throw Er;
        }
        GetFileNamesForRuns(RNumber, RNumber1);
    }
    else
    {
        RNumber=atoi(RName);
        if(!(RNumber>0))
        {
            Char_t Message[100];
            sprintf(Message,"Run Number Not Defined ?");
            MErr * Er = new MErr(WhoamI,0,0, Message);
            throw Er;
        }
        GetFileNamesForRuns(RNumber);


    }

    END;
}


void Analysis::GetFileNamesForRuns(UShort_t Run, UShort_t Run1)
{
    START;

    if(!(Run1>0))
        Run1=Run;

    if(Run1>=Run)
        for(UShort_t i=Run;i<=Run1;i++)
            AddFiles(i);
    else
        for(UShort_t i=Run1;i<=Run;i++)
            AddFiles(i);
    END;
}
void Analysis::AddFiles(string IF, string OF)
{
    START;
    string IPath;
    string ParFileName;
    string ParFName;
    IPath.clear();
    if(fModeWhich[1] || fModeWhich[2])
       IPath = AnalysisPath + CM->Get("Raw_Path");
    if(fModeWhich[3])
       IPath = AnalysisPath + CM->Get("RootR_Path");
    if(fModeWhich[4])
       IPath = AnalysisPath + CM->Get("RootA_Path");


    IFN->push_back(IF);
    OFN->push_back(OF);

    ParFileName = GetParFileName(GetRunNr(IF),IPath.c_str());
    if (!ParFileName.empty())
    {
        ParFName.clear();
        ParFName = IPath;
        ParFName += "/";
        ParFName += ParFileName;
        ParFN->push_back(ParFName);
    }
    else
    {
        Char_t Message[100];
        sprintf(Message,"Could not Find Parameter File in ./run directory  for Run Nr %d/",GetRunNr(IF));
        MErr * Er = new MErr(WhoamI,0,0, Message);
        //throw Er;
        delete Er;

    }
    BrhoRef.push_back(GetBrhoRef(IF));

    END;
}
void Analysis::AddFiles(UShort_t Run)
{
    START;

    string FileName;
    string FName;
    string ParFName;
    string ParFileName;
    char InputFileName[500];
    char OutputFileName[500];
    string IPath;
    string OPath;
    Float_t Brho = -1.;

    IPath.clear();
    OPath.clear();
    FileName.clear();
    ParFileName.clear();

    if(fModeWhich[1] || fModeWhich[2])
       IPath = AnalysisPath + CM->Get("Raw_Path");
    if(fModeWhich[3])
       IPath = AnalysisPath + CM->Get("RootR_Path");
    if(fModeWhich[4])
       IPath = AnalysisPath + CM->Get("RootA_Path");

    if(fModeWhich[2] || fModeWhich[3])
       OPath = AnalysisPath + CM->Get("RootA_Path");
    if(fModeWhich[1])
       OPath = AnalysisPath + CM->Get("RootR_Path");



    if(fModeWhich[1] || fModeWhich[2])
    {
        FileName = GetFileName(Run,IPath.c_str());
        ParFileName = GetParFileName(Run,IPath.c_str());
        if(!FileName.empty())
        {
            FName.clear();
            FName = IPath;
            FName += "/";
            FName += FileName;
            IFN->push_back(FName);
            if(fModeWhich[1])
            {
                sprintf(OutputFileName,"%s/r%04d_000r.root",OPath.c_str(), Run);
            }
            if(fModeWhich[2])
            {
                sprintf(OutputFileName,"%s/r%04d_000a.root",OPath.c_str(), Run);

            }
            try{
                Brho = GetBrhoRef(Run);
            }
            catch(MErr* Er)
            {
                Er->Print();
                delete Er;
                Brho = 0 ;
            }

            FName.clear();
            FName = OutputFileName;
            OFN->push_back(FName);
            if(fModeWhich[2] || fModeWhich[1] )
               BrhoRef.push_back(Brho);

            // Add ParamterFile
            if (!ParFileName.empty())
            {
                ParFName.clear();
                ParFName = IPath;
                ParFName += "/";
                ParFName += ParFileName;
                ParFN->push_back(ParFName);
            }
            else
            {
                ParFN->push_back("");
                //                Char_t Message[100];
                //                sprintf(Message,"Could not Find Parameter File in ./run directory  for Run Nr %d/",Run);
                //                MErr * Er = new MErr(WhoamI,0,0, Message);
                //                //	      throw Er;
            }

            Bool_t fExist = true;
            for(UShort_t i=1;i<999&&fExist;i++)
            {
                FName.clear();
                FName = IPath;
                FName += "/";
                FName += FileName;
                Char_t CNr[5];
                sprintf(CNr,".%d",i);
                FName += CNr;
                if((fExist = FileExists(FName.c_str())))
                {
                    IFN->push_back(FName);
                    ParFN->push_back(ParFName);
                    if(fModeWhich[1])
                        sprintf(OutputFileName,"%s/r%04d_%03dr.root",OPath.c_str(), Run,i);
                    if(fModeWhich[2])
                        sprintf(OutputFileName,"%s/r%04d_%03da.root",OPath.c_str(), Run,i);
                    FName.clear();
                    FName = OutputFileName;
                    OFN->push_back(FName);
                    if(fModeWhich[2] || fModeWhich[1])
                       BrhoRef.push_back(Brho);
                }
            }
        }
    }






    END;
}


Int_t DirSelect(const struct dirent *entry)
{
    START;
    if ((strcmp(entry->d_name, ".") == 0) ||
            (strcmp(entry->d_name, "..") == 0))
        return (0);
    else
        if((strncmp(entry->d_name, "run_",4) == 0))
        {
            return (1);
        }
        else
            return (0);
    END;
}
Int_t FileSelect(const struct dirent *entry)
{
    START;
    if ((strcmp(entry->d_name, ".") == 0) ||
            (strcmp(entry->d_name, "..") == 0))
        return (0);
    else
        if(
                (strncmp(entry->d_name, "run_",4) == 0)
                &&
                (strstr(entry->d_name, ".dat"))
                )
        {
            return (1);
        }
        else
            return (0);
    END;
}

Int_t ParFileSelect(const struct dirent *entry)
{
    START;
    if ((strcmp(entry->d_name, ".") == 0) ||
            (strcmp(entry->d_name, "..") == 0))
        return (0);
    else
        if(
                (
                    (strncmp(entry->d_name, "run_",4) == 0)
                    &&
                    (strstr(entry->d_name, ".dat"))
                    )
                ||
                (
                    (strncmp(entry->d_name, "ACTIONS",7) == 0)
                    &&
                    (strstr(entry->d_name, ".dat"))
                    )
                )
        {
            return (1);
        }
        else
            return (0);
    END;
}

Bool_t Analysis::FileExists(const char* FName)
{
    START;
    ifstream ifile;
    ifile.open(FName);
    if(ifile)
    {
        ifile.close();
        return(true);
    }
    else
    {
        return(false);
    }
    END;
}
string Analysis::GetParFileName(UShort_t RunNumber, const Char_t * Path)
{
    START;
    Int_t nFiles;
    struct dirent **Files;
    Char_t Name[200];
    Char_t Name1[200];
    Char_t Name2[200];
    string FileName;
    Bool_t Found=false;
    FileName.clear();
    sprintf(Name,"ACTIONS_%s.CHC_PAR.run_%04d.dat",Analysis_ExpName.c_str(), RunNumber);
    sprintf(Name1,"ACTIONS_%s.CHC_PAR.run_%03d.dat",Analysis_ExpName.c_str(), RunNumber);
    sprintf(Name2,"ACTIONS_%s.CHC_PAR",Analysis_ExpName.c_str());
    nFiles = scandir(Path, &Files, ParFileSelect , alphasort);

    if (nFiles <= 0)
    {
        Char_t Message[100];
        sprintf(Message,"No File in  %s/",Path);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    else
    {
        for(UShort_t i=0;i<nFiles && !Found;i++ )
        {
            if(strncmp(Files[i]->d_name, Name,strlen(Name)) == 0)
            {
                FileName = Files[i]->d_name;
                Found=true;
            }
            if(strncmp(Files[i]->d_name, Name1,strlen(Name1)) == 0)
            {
                FileName = Files[i]->d_name;
                Found=true;
            }
            if(strcmp(Files[i]->d_name, Name2) == 0)
            {
                FileName = Files[i]->d_name;
                Found=true;
            }
        }

        // Free memory of the Struct
        for(UShort_t i=0;i<nFiles;i++ )
            free (Files [i]);
        free (Files);
    }

    return(FileName);
    END;
}

string Analysis::GetDirName(UShort_t RunNumber, const Char_t * Path)
{
    START;
    Int_t nFiles;
    struct dirent **Files;
    Char_t Name[40];
    string FileName;
    Bool_t Found=false;

    FileName.clear();
    sprintf(Name,"run_%04d.dat", RunNumber);
    nFiles = scandir(Path, &Files, DirSelect , alphasort);
    if (nFiles <= 0)
    {
        Char_t Message[100];
        sprintf(Message,"No directory in  %s/ matching run_* pattern",Path);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    else
    {
        for(UShort_t i=0;i<nFiles && !Found;i++ )
        {
            if(strncmp(Files[i]->d_name, Name,12) == 0)
            {
                FileName = Files[i]->d_name;
                Found=true;
                return(FileName);
            }
        }

        // Free memory of the Struct
        for(UShort_t i=0;i<nFiles;i++ )
            free (Files [i]);
        free (Files);
    }

    return(FileName);
    END;
}

string Analysis::GetFileName(UShort_t RunNumber, const Char_t * Path)
{
    START;
    Int_t nFiles;
    struct dirent **Files;
    Char_t Name[40];
    Char_t Name1[40];
    string FileName;
    Bool_t Found=false;

    FileName.clear();
    sprintf(Name,"run_%04d.dat", RunNumber);
    sprintf(Name1,"run_%03d.dat", RunNumber);
    nFiles = scandir(Path, &Files, FileSelect , alphasort);
    if (nFiles <= 0)
    {
        Char_t Message[100];
        sprintf(Message,"No File in  %s/",Path);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    else
    {
        for(UShort_t i=0;i<nFiles && !Found;i++ )
        {
            // cout << Files[i]->d_name << endl;
            if(strstr(Files[i]->d_name, Name))
            {
                FileName = Files[i]->d_name;
                Found=true;
            }
            if(strstr(Files[i]->d_name, Name1))
            {
                FileName = Files[i]->d_name;
                Found=true;
            }
        }

        // Free memory of the Struct
        for(UShort_t i=0;i<nFiles;i++ )
            free (Files [i]);
        free (Files);
    }

    return(FileName);
    END;
}

#ifdef NARVAL
void Analysis::RunWatcher()
{
    START;

    END;
}

void Analysis::GetWatcherOptions(int argc,char** argv)
{
    START;

    END;
}
#endif

#ifdef WITH_ROOT
void Analysis::AddBranches(TTree *tree)
{
    START;
    if(tree)
    {
        // for(UShort_t i=0;i<Det->size();i++)
        //   {
        // 	 Det->at(i)->AddBranches(tree);
        //   }

    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set attach branch in %s while it is not initialized",WhoamI);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}


void Analysis::SetOutTFile(TFile *OF)
{
    START;
    if(!OutTFile)
        OutTFile = OF;
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set OutTfile in %s while it is already initialized",WhoamI);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}
#endif

void Analysis::ProcessFiles()
{
    START;

    Run();

    END;
}

void Analysis::SetUpUnAssignedParams(void)
{
    START;
//    // Lookup for Unassigned Parameters
//    Int_t NPar = 0;
//    Int_t UnAssigned[MAX_PAR];
//    for (Int_t i=0; i<MAX_PAR; i++)
//    {
//        if((PL->GetParName(i)).size() > 0 && PL->GetParSize(i) > 0)
//        {
//            // The Label is not in the Mapp
//            if(!MyMap->GetAddress(i))
//            {
//                // cout << i << " " << PL->GetParName(i) << endl;
//                UnAssigned[NPar] = i;
//                NPar++;
//            }
//            // else
//            // 	cout << " --> Present" << endl;
//        }
//    }
//    // cout << NPar << " Unused Parameters" << endl;

//    BaseDetector *ADet = NULL;
//    ADet = new BaseDetector("UnAssigned",NPar,true,false,false,false,"");
//    for(Int_t i=0; i< NPar; i++)
//        ADet->SetParameterName(PL->GetParName(UnAssigned[i]).c_str(),i);

//    ADet->SetParameters(PL,MyMap);
//#ifdef WITH_ROOT
//    ADet->SetHistogramsRaw(false);
//    ADet->SetOutAttachRawI(true);
//    ADet->SetOutAttachCalV(false);
//    ADet->SetOutAttachCalF(false);
//    ADet->SetOutAttachCalI(false);
//    ADet->SetMainHistogramFolder("UnAssigned");
//    ADet->OutAttach(OutTTree);
//    //	ADet->CreateHistograms(H, OutTFile->GetDirectory(""));
//#endif
//    AddComponent(ADet);

    END;
}

void Analysis::DeleteDetectors()
{
    START;
    for(UShort_t i=0;i<Det->size();i++)
    {
        Det->at(i) = CleanDelete(Det->at(i));
    }
    Det->clear();
    END;
}

void Analysis::UpdateCounters()
{
    START;
#ifdef PRESORT_DC
    if(!PreSortExist)
        FP->UpdateCounters();
    else
#endif
    {
        for(UShort_t i=0;i<Det->size();i++)
            Det->at(i)->UpdateCounters();
    }

    RunEvt++;
    TotalEvt++;

    END;
}

void Analysis::Reset()
{
    START;
    start = clock();

    for(UShort_t i=0;i<Det->size();i++)
        Det->at(i)->Reset();
    RunEvt = 0 ;
    END;
}

void Analysis::PrintCounters()
{
    START;
    for(UShort_t i=0;i<Det->size();i++)
        Det->at(i)->PrintCounters();
    RunEvt = 0 ;
    END;
}

void Analysis::Clear()
{
    START;
#ifdef PRESORT_DC
    if(!PreSortExist && (fAnMode > 1 || fAnMode == MODE_WATCHER))
        FP->Clear();
    else
#endif
        for(UShort_t i=0;i<Det->size();i++)
            Det->at(i)->Clear();


    END;
}


void Analysis::Treat()
{
    START;
    for(UShort_t i=0;i<Det->size();i++)
    {
        Det->at(i)->Treat();

    }

    END;
}

void Analysis::FillHistograms()
{
    START;
#ifdef WITH_ROOT
#ifdef PRESORT_DC
    if(PreSortExist || fAnMode < 2)
#endif
        for(UShort_t i=0;i<Det->size();i++)
        {
            Det->at(i)->FillHistograms();
        }
#endif
    END;
}

void Analysis::FillTree()
{
    START;
#ifdef WITH_ROOT
#ifdef PRESORT_DC
    if(PreSortExist || fAnMode < 2)
#endif
        OutTTree->Fill();
#endif
    END;
}

void Analysis::PrintRaw()
{
    START;
    for(UShort_t i=0;i<Det->size();i++)
        Det->at(i)->PrintRaw();

    END;
}

void Analysis::PrintCal()
{
    START;
    for(UShort_t i=0;i<Det->size();i++)
        Det->at(i)->PrintCal();

    END;
}

void Analysis::Add(UShort_t *Data, const ULong64_t& TS)
{
    START;
    MyMap->AddData(Data,TS);
    END;
}

#ifdef WITH_MESYTEC_DATA
void Analysis::AddMesytecData(const Int_t& Addr, const Int_t& Bus, const Int_t& Channel, const UShort_t& Data, const ULong64_t& TS, const UShort_t& Time) {
    START;
    Int_t key = -1;
    try{
        key = MesytecPar->GetParId(Addr, Bus, Channel);

        if (key>-1)
        {
            //cout << "Key " << key << endl;
            MyMap->AddMesytecData(key, Data, TS, Time);//, 0, 0, 0);
        }
    }
    catch(MErr * Er)
    {
        //Er->Print();
        throw Er;
        //delete Er;
    }
    END;
}
#endif

#ifdef WITH_NUMEXO

void Analysis::AddNUMEXO(const UShort_t& Board, const UShort_t& Channel, const Int_t& Sample, const UShort_t& Data, const ULong64_t& TS, const Int_t& DataValid, const Int_t& PileUp, const Int_t& Time) {
    START;
    try{
        Int_t key = PL_NUMEXO->GetParId(Board, Channel, Sample);

        if (key>-1)
            MyMap->AddNUMEXOData(key, Data, TS, DataValid, PileUp,Time);
    }
    catch(MErr * Er)
    {
        DuplicateCtr++;
        // Er->Print();
        delete Er;
    }
    END;
}
#endif

#ifdef WITH_GET
void Analysis::AddGET(const Int_t& CoboId,
                      const Int_t& AsadId,
                      const Int_t& AgetId,
                      const Int_t& ChannelId,
                      const Int_t& SampleId,
                      const UShort_t& Data,
                      const ULong64_t& TS,
                      const Int_t& Time)
{
    START;

    try{
        Int_t key = PL_GET->GetParId(CoboId, AsadId, AgetId, ChannelId, SampleId);

        //cout<<key<<" *** "<<CoboId<<" "<< AsadId<<" "<< AgetId<<" "<< ChannelId<<" "<< SampleId<<endl;
        if (key>-1)
        {
            MyMap->AddGETData(key, Data, TS,Time);
        }
        //else
        //{
        //cout<<"Cobodata without the map: CoboID: "<<CoboId<<" AsadID: "<< AsadId<<" AgetID: "<< AgetId<<" ChannelID: "<< ChannelId<<" BuquetID: "<< Time<<" Data: "<<Data<<endl;
        //}
    }
    catch(MErr * Er)
    {
        DuplicateCtr++;
        Er->Print();
        delete Er;
    }

    END;
}
#endif

#ifdef WITH_PARIS
void Analysis::AddParisData(MFMParisFrame *frame)
{
    START;
     try{
        UShort_t BoardId = frame->GetBoardId();
        UShort_t ChannelId = frame->GetTGCristalId();
        UShort_t ClusterNr = 1000;
        UShort_t DetNr = 1000;
        if(BoardId == 4 && ChannelId > 11)
        {

        }
        else
        {
            ClusterNr = ParisDet->GetClusterNr(BoardId,ChannelId);
            DetNr = ParisDet->GetDetectorNr(BoardId,ChannelId);
        }
        UShort_t QShort = frame->GetQShort();
        UShort_t QLong = frame->GetQLong();
        Float_t Cfd = frame->GetCfd();
        ULong64_t TS= frame->GetTimeStamp();
        Int_t EN = frame->GetEventNumber();
#ifdef DEBUG
        cout << std::dec << "------------- Paris Frame " << endl;
        cout << "\n Chan Id :" << pParisFrame->GetTGCristalId()
             << "\n Board Id : " << pParisFrame->GetBoardId()
             << " \n \t <ClNr><DetNr> : <" << ClusterNr << "><"<< DetNr << ">"<< endl
             << "\n QShort : " << pParisFrame->GetQShort()
             << "\n QLong  : " << pParisFrame->GetQLong()
             << "\n CFD  : " << pParisFrame->GetCfd()
             << "\n --> TS " << TS
             << "\n --> EN " << EN << endl;
        pParisFrame->HeaderDisplay();
        cout << endl;
#endif

        if(EN != 1) // Skip Idle Frame EN
        {
            if(ClusterNr < ParisDet->GetNumberOfSubDetectors())
            {      // Set TheS Value of QShort, QLong and Cfd ?
                ParisDet->GetSubDet(ClusterNr)->GetSubDet(0)->AddRawData(DetNr,QShort,TS);
                ParisDet->GetSubDet(ClusterNr)->GetSubDet(1)->AddRawData(DetNr,QLong,TS);
                ParisDet->GetSubDet(ClusterNr)->GetSubDet(2)->SetCalData(DetNr,Cfd,TS);
            }
            else
            {

                // Last 4 Channels are Time References
                if((BoardId==4)&&((ChannelId) > 11 && (ChannelId <16)))
                {
                    ParisDet->SetTSRef(15-ChannelId,TS,Cfd);
                }
            }
        }
    }
    catch(MErr * Er)
    {

        Er->Print();
        pParisFrame->HeaderDisplay();
        delete Er;
    }

    END;
}
#endif

#ifdef WITH_GET
void Analysis::AddCoboData(MFMCoboFrame *frame, bool reducedFrame)
{
    START;
    Int_t coboIdx = frame->CoboGetCoboIdx();
    Int_t asadIdx = frame->CoboGetAsaIdx();
    Int_t agetIdx, chanIdx,buckIdx;
    UShort_t coboSample;
    ULong64_t TS = frame->GetTimeStamp();
    const int coboNItems =frame->GetNbItems();

    MFM_coboItem *coboitem=NULL;
    uint32_t cobosample;
    uint32_t cobobuckidx;
    uint32_t cobochanidx;
    uint32_t coboagetidx;

    for(int it=0;it<coboNItems;it++)
    {
        coboitem = (MFM_coboItem *)frame->GetItem(it);
        frame->CoboGetParametersByItem(coboitem,&cobosample,&cobobuckidx, &cobochanidx,&coboagetidx);
        agetIdx = (Int_t) coboagetidx;
        chanIdx = (Int_t) cobochanidx;
        buckIdx = (Int_t) cobobuckidx;
        coboSample = (UShort_t) cobosample;

        if(reducedFrame == true)
        {
            //in case of single buckIdx per frame [reduced cobo frame]
            AddGET(coboIdx,asadIdx,agetIdx,chanIdx,-1,coboSample,TS,buckIdx);
        }
        else
        {
            AddGET(coboIdx,asadIdx,agetIdx,chanIdx,buckIdx,coboSample,TS,buckIdx);
        }
    }

    END;
}
#endif

void Analysis::Run()
{
    START;
    LogManager *LM = LogManager::getInstance();
    TotalEvt = 0;
    fNEvents = (ULong64_t) atoi(CM->Get("NEvents").c_str()) ;
    while(NextFile() && TotalEvt < fNEvents)
    {
        string LogFileName;
#ifdef WITH_MFM
        GDF = new MFMDataFile(GetCurrentInputFile());
        ULong64_t FS = GDF->GetFileSize();
#else
        GDF = new GanilDataFile(GetCurrentInputFile());
#endif
        Double_t Frac = 0.0;
        Double_t Time = 0.0;
        Double_t TimeLeft = 0.0;

#ifdef WITH_ROOT
        OutTFile = new TFile((GetCurrentOutputFile()).c_str(),"recreate");
        cout << "  <Analysis> " << (GetCurrentOutputFile()).c_str() << " " << " created" << endl;
    //            UShort_t CompressionLevel = 0;
    //         OutTFile->SetCompressionLevel(CompressionLevel);
    //        cout << "  <Analysis> Set Compression Level " << CompressionLevel  << endl;
        if(fAnMode == MODE_D2R)
            OutTTree = new TTree("RD","Raw Data Tree");
        else
            OutTTree = new TTree("AD","Analysed Data Tree");

        // OutTTree->SetCacheSize(10000000);
        // OutTTree->SetCompressionLevel(10000000);
#endif
        LogFileName = GetCurrentOutputFile();
        LogFileName += ".log";
        LM->SetLogFile(LogFileName.c_str());
        L = LM->GetFilePtr();


        PL = new Parameters(50000);
        //PL = new Parameters(MAX_PAR);

#ifdef WITH_NUMEXO
        NTopo = NumexoTopology::getInstance();
        string NumexoTopoFile= AnalysisPath;
        NumexoTopoFile += CM->Get("NumexoTopologyFile");
        NTopo->Init(NumexoTopoFile.c_str());
        PL_NUMEXO = NUMEXOParameters::getInstance();
#endif

#ifdef WITH_GET
        GTopo = GETTopology::getInstance();
        string GetTopoFile= AnalysisPath;
        GetTopoFile += CM->Get("GetTopologyFile");
        string GetTopoPath= AnalysisPath;
        GetTopoPath += CM->Get("GetTopologyPath");
        GTopo->Init(GetTopoFile.c_str(),GetTopoPath.c_str());
        PL_GET = GETParameters::getInstance();
#endif

#ifdef WITH_MESYTEC_DATA
        try{
            InitMesytecData(AnalysisPath);
        }
        catch (MErr * Er)
        {
            Er->Print();
            throw Er;
        }

        // Mesytec Topology // Crate map ?
        MesytecTopo = MesytecTopology::getInstance();
        string MesytecTopoFile= AnalysisPath;
        MesytecTopoFile += CM->Get("MesytecTopologyFile");
        MesytecTopo->Init(MesytecTopoFile.c_str());

        MesytecPar = MesytecParameters::getInstance();
//        MesytecPar->Print();
#endif


#ifdef WITH_MFM
        try
        {
            GDF->GetParametersFromActionFile(PL,(GetCurrentParFile()).c_str());
        }
        catch(MErr * Er)
        {

            Er->Print();
        }
#else
        GDF->GetParameters(PL);
#endif


#ifdef WITH_ROOT
        H = new HistoManager();
        if(!H)
        {
            MErr * Er = new MErr(WhoamI,0,0, "Coud not allocate memory to hold HistoManager !");
            throw Er;
        }
#endif
        MyMap = new Map(PL->GetMaxParameters(),60000,-1,60000);
        MyMap->DataOp(PL);

        // Setup Detectors
        SetUpAnalysis();
#ifdef WITH_BLM
        DM->Print();
        //GDF->DBLM = (BLM*)DM->GetDetector("BLM");
#endif

#ifdef WITH_PISTA
    try{
      PistaDet = (Pista*) DM->GetDetector("PISTA");
    }
    catch(MErr *Er)
      {
        PistaDet = NULL;
        Er->Print();
        delete Er;
      }
#endif

#ifdef WITH_PARIS
    try{
	  ParisDet = (Paris*) DM->GetDetector("Paris");
	}
	catch(MErr *Er)
	  {
	    ParisDet = NULL;
	    Er->Print();
	    delete Er;
	  }
#endif




#ifdef DEGUG
        PL->Print();
        MyMap->Print();
#endif
        PL = CleanDelete(PL);

        start_run = clock();

        RunEvt =0;
        cout << "  Current Run : " << GetCurrentInputFile() << endl;
        double ms_double_av=0;
        while(GDF->Next())
        {
            TotalEvt++;
            if(!GDF->GetIsCtrl())
            {
                cout << " Not Controlled buffer" << endl;
                continue;
            }

            auto t1 = high_resolution_clock::now();

            Clear();

            if(TotalEvt%10000==0)
            {
                end = clock();

//                cout << "\r    Frame    : " << TotalEvt
//                     << " --- Read : "<< std::setprecision(2) << std::fixed <<  (double)GDF->GetMBytesRead()/((double)(FS))*100. << "%"
//                     << " --- " << (double)GDF->GetMBytesRead()/((double)(end-start_run)/CLOCKS_PER_SEC)
//                     << " Mb/sec " << flush;

                Frac = (double)GDF->GetMBytesRead()/((double)(FS));
                Time = ((double) (end-start_run)/CLOCKS_PER_SEC);
                TimeLeft = Time*(1./Frac - 1.);

                cout << std::dec
                     << std::setprecision(2) << "\r    [" << CurrentFile << "/" <<  IFN->size() << "] Events    : " << TotalEvt
                     << " --- " << std::setprecision(2) << std::fixed << (double)TotalEvt/Time
                     << " RunEvt/sec"
                     << " --- Read : "<< std::setprecision(2) << std::fixed <<  Frac*100. << "%"
                     << " (" << (double)GDF->GetMBytesRead()/Time
                     << " Mb/sec) --- Time Left : "
                     << std::setprecision(2) << std::fixed << (int) (TimeLeft/60)
                     << " min "
                     <<  std::setprecision(2) << std::fixed <<  ((int)TimeLeft % 60) << " sec. - av/cycle " << std::setprecision(10) << std::fixed << ms_double_av << "  \r   " << std::setprecision(2) << flush;


            }
            UnpackLevel = 0;
            try{
#ifdef WITH_MFM
                UnPack(GDF->GetCurrentFrame());
#endif
            }
            catch(MErr *Er)
            {
               cout<< "Caught Exception at Event " << TotalEvt << endl;
                Er->Print();
                delete Er;
                // Skip Event From Fill
                // continue;
            }

            if(GDF->GetIsCtrl())
            {

                Treat();

#ifdef DEBUG
                PrintRaw();
#endif
#ifdef WITH_ROOT

                FillTree();
                RunEvt++;
#endif
                FillHistograms();
            }
            if(RunEvt > fNEvents -1)
                break;
            auto t2 = high_resolution_clock::now();
            duration<double, std::milli> ms_double = t2 - t1;
            ms_double_av = (1-0.2) * ms_double_av + 0.2*ms_double.count();

        }
        end = clock();

        cout << "\r Controlled  Frames    : " << RunEvt << endl;
        cout << "\r Total Frames    : " << TotalEvt << endl;
        cout << "Frames/sec : " << (double)TotalEvt/((double)(end-start)/CLOCKS_PER_SEC) << endl;
        cout << "Frames/sec (noInit): " << (double)TotalEvt/((double)(end-start_run))/CLOCKS_PER_SEC << endl;
        cout << "Time required for execution: "
             << (double)(end-start)/CLOCKS_PER_SEC
             << " seconds." << "\n\n";

        MyMap = CleanDelete(MyMap);
#ifdef WITH_ROOT
        if(OutTFile)
            OutTFile->Write();
#endif
        DeleteDetectors();
#ifdef WITH_ROOT
        H = CleanDelete(H);
        OutTTree = CleanDelete(OutTTree);
        if(OutTFile)
        {
            OutTFile->Close();
            OutTFile = CleanDelete(OutTFile);
        }
#endif

#ifdef WITH_NUMEXO
        NTopo = CleanDelete(NTopo);
#endif
#ifdef WITH_GET
        GTopo = CleanDelete(GTopo);
#endif
#ifdef WITH_MESYTEC_DATA
        MesytecTopo = CleanDelete(MesytecTopo);
#endif
        LM->CloseFile();
        GDF = CleanDelete(GDF);
    }
    LM->kill();
    END;
}

void Analysis::SetUpDetectorsFromFile(string fname)
{
    START;


    string DetFileName = AnalysisPath + fname;
    cout << "Parsing Configuration File : " << DetFileName << endl;
    L->File << "Parsing Configuration File : " << DetFileName << endl;
    Char_t * Ptr = NULL;
    Char_t Line[255];
    UShort_t Len = 255;
    MIFile IF(DetFileName.c_str());
    Bool_t IsNumexo = false;
    Bool_t IsGet = false;
    Bool_t IsMesytec = false;
    Char_t PName[255];
    Char_t FName[255];
    Char_t CalName[500];
    Int_t  NCh;
    Int_t  MaxMult = 1;
    Int_t  NBins;
    Int_t  Ch;
    Int_t  UL;
    Int_t  LL;
    Int_t  Th;
    Float_t  ULF;
    Float_t  LLF;
    Int_t NChX;
    Int_t NChY;
    Int_t RefMTDC_TMW[2];
    Int_t RefCh_TMW[2];
    Bool_t RawData, CalData, DefCal,PosInfo;
    RawData = true;
    DefCal  = true;
    PosInfo = false;

    Int_t  Cal;
    Int_t Charges;
    Int_t  Fl;
    Char_t dname[255];
    Char_t dtype[255];
    Char_t path[500];
    Bool_t Calib;

    Bool_t FlagOutRawI = true;
    Bool_t FlagOutRawF = false;
    Bool_t FlagOutRawV  =false;

    Bool_t FlagOutCalI = true;
    Bool_t FlagOutCalF = false;
    Bool_t FlagOutCalV  =false;

    BaseDetector *BDet = nullptr;

    while(IF.GetLine(Line,Len)){
        if( (Ptr = strstr(Line,"//")))
        {
            // cout << Line << endl;

        }
        else
        {
            IsNumexo = false;
            IsGet = false;
	    IsMesytec = false;
            if(strcmp(Line, "****Detector****") == 0)
            {
                // First Get Required Fields
                NCh = -1;
                Cal = 0;
                Calib = false;
                Charges=0;
		NChX = -1;
                NChY = -1;
		for(int i=0;i<2;i++)
		  {
		    RefMTDC_TMW[i] =-1;
		    RefCh_TMW[i] =-1;
		  }
		RawData = true;
                DefCal  = true;
                PosInfo = false;
                // Default Behaviour
                FlagOutRawI = true;
                FlagOutRawF = false;
                FlagOutRawV = false;


                FlagOutCalI = true;
                FlagOutCalF = false;
                FlagOutCalV  =false;

                IF.GetLine(Line,Len);
                if( ! (sscanf(Line,"Name : %s",dname) == 1))
                {
                    Char_t Message[500];
                    sprintf(Message,"Config File %s is corrupted expecting Name : ",DetFileName.c_str());
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                IF.GetLine(Line,Len);
                if( ! (sscanf(Line,"Type : %s",dtype) == 1))
                {
                    Char_t Message[500];
                    sprintf(Message,"Config File %s - <%s> is corrupted expecting Type : ",DetFileName.c_str(),dname);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                IF.GetLine(Line,Len);
                if( ! (sscanf(Line,"Calib : %d",&Cal) == 1))
                {
                    Char_t Message[500];
                    sprintf(Message,"Config File %s - <%s> is corrupted expecting Calib : ",DetFileName.c_str(),dname);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                else
                    Calib = Cal;

                Calib = Calib || (fAnMode==MODE_D2A);

                cout << "-------------------------------------" << endl;
                cout << " Setting Detector " << dname << endl;
                cout << "-------------------------------------" << endl;
                cout << " Detector type : "<< dtype
                     << " Calib : " << Calib << endl;

                // Create the detector
                if(strcmp(dtype,"Generic") == 0 || strcmp(dtype,"GenericTS") == 0 || strcmp(dtype,"GenericNumexo") == 0 || strcmp(dtype,"GenericMDPP") == 0 || strcmp(dtype,"GenericMMR") == 0)
                {

                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    Bool_t hasTS = false;
                    if(strcmp(dtype,"GenericNumexo")==0)
                    {

                        hasTS = true;
                        IsNumexo = true;
                    }
                    else if(strcmp(dtype,"GenericTS")==0)
                    {

                        hasTS = true;
                    }
                    else if(strcmp(dtype,"GenericMDPP")==0 || strcmp(dtype,"GenericMMR")==0)
                    {
		      IsMesytec = true;
		      hasTS = true;
                    }
		    if( strcmp(dtype,"GenericMMR")==0)
		      BDet = new BaseDetector(dname,NCh,RawData,Calib,DefCal,PosInfo,"",true,hasTS);
		    else
		      BDet = new BaseDetector(dname,NCh,RawData,Calib,DefCal,PosInfo,"",false,hasTS);		      
		    
                    FlagOutRawI = false;
                    FlagOutRawF = false;
                    FlagOutRawV = true;
                    if(strcmp(dname,"TAC") == 0)
                    {
                        FlagOutRawI=true;
                        FlagOutRawV = false;
                        FlagOutRawF=false;
                        FlagOutCalI = false;
                        FlagOutCalF = false;
                        FlagOutCalV = false;

                        cout << "TAC Config" << endl;
                    }
                    if(strcmp(dname,"DATATRIG_CATS1") == 0)
                    {
                        FlagOutRawI=true;
                        FlagOutRawV = false;
                        FlagOutRawF=false;
                        FlagOutCalI = false;
                        FlagOutCalF = false;
                        FlagOutCalV = false;
                        cout << "DATATRIG_CATS1 Config" << endl;
                    }
                    if(strcmp(dname,"DATATRIG_CATS2") == 0)
                    {
                        FlagOutRawI=true;
                        FlagOutRawV = false;
                        FlagOutRawF=false;
                        FlagOutCalI = false;
                        FlagOutCalF = false;
                        FlagOutCalV = false;
                        cout << "DATATRIG_CATS2 Config" << endl;
                    }
                    if(strcmp(dname,"DATATRIG_CAVIAR") == 0)
                    {
                        FlagOutRawI=true;
                        FlagOutRawV = false;
                        FlagOutRawF=false;
                        FlagOutCalI = false;
                        FlagOutCalF = false;
                        FlagOutCalV = false;
                        cout << "DATATRIG_CAVIAR Config" << endl;
                    }
                    if(strcmp(dname,"DCX_ZDD") == 0)
                    {
                        FlagOutRawI=false;
                        FlagOutRawV = false;
                        FlagOutRawF=true;
                        FlagOutCalI = false;
                        FlagOutCalF = false;
                        FlagOutCalV = false;
                        cout << "DCX ZDD Config" << endl;
                    }
                    if(strcmp(dname,"DCY_ZDD") == 0)
                    {
                        FlagOutRawI=false;
                        FlagOutRawV = false;
                        FlagOutRawF=true;
                        FlagOutCalI = false;
                        FlagOutCalF = false;
                        FlagOutCalV = false;

                        cout << "DCY ZDD Config" << endl;
                    }
                    if(strcmp(dname,"IC_ZDD") == 0)
                    {
                        FlagOutRawI=false;
                        FlagOutRawV = false;
                        FlagOutRawF=true;
                        FlagOutCalI = false;
                        FlagOutCalF = false;
                        FlagOutCalV = false;

                        cout << "IC_ZDD" << endl;
                    }
                    if(strcmp(dname,"Plastic") == 0)
                    {
                        FlagOutRawI=false;
                        FlagOutRawV = false;
                        FlagOutRawF=true;
                        FlagOutCalI = false;
                        FlagOutCalF = false;
                        FlagOutCalV = false;

                        cout << "Plastic Config" << endl;
		    }   
                    if(strcmp(dtype,"GenericMMR")==0)
                    {
                        FlagOutRawI=false;
                        FlagOutRawV = true;
                        FlagOutRawF=false;
                        FlagOutCalI = false;
                        FlagOutCalF = false;
                        FlagOutCalV = true;
			BDet->HasCalCharges(true);
			
                        cout << "GenericMMR Config" << endl;
		    }   

                    if(atoi(CM->Get("IsWatcher").c_str()))
		      {
                        BDet->SetAnalysisMode(MODE_WATCHER);
			if(strcmp(dtype,"GenericMMR")==0)
			{
			  if(BDet->HasRawData())
			    BDet->SetHistogramsRawSummary(true);
			  if(BDet->HasCalData())
			    BDet->SetHistogramsCalSummary(true);
			}
		      }
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }

                    for(UShort_t i=0; i < NCh; i++)
                    {
                        IF.GetLine(Line,Len);
                        if( ! (sscanf(Line,"Ch %d : %s %d %d %d ",&Ch,PName,&NBins,&LL,&UL) == 5))
                        {
                            Char_t Message[500];
                            sprintf(Message,"Config File %s is corrupted (%s): expecting Ch_%02d : ParName Nbins LL UL",DetFileName.c_str(),dname,i);
                            MErr * Er= new MErr(WhoamI,0,0, Message);
                            throw Er;
                        }
                        if(Ch != i)
                        {
                            Char_t Message[500];
                            sprintf(Message,"Config File %s is corrupted (%s): expecting Ch_%02d : ParName Nbins LL UL",DetFileName.c_str(),dname,i);
                            MErr * Er= new MErr(WhoamI,0,0, Message);
                            throw Er;
                        }
                        cout << "\t Ch " << Ch << " : " <<  PName  << " "  << NBins << " " << LL << " " << UL << endl;
                        BDet->SetParameterName(PName,i);
                        BDet->SetRawHistogramsParams(NBins,LL,UL);
                        if(Calib)
                        {
                            sprintf(CalName,"%s_C",PName);
                            BDet->SetCalName(CalName,i);
                        }
                    }
                    //                    BDet->SetHistogramsRaw(true);
                    //                    BDet->SetHistogramsRaw1D(true);
                    //                   // BDet->SetHistogramsRawSummary(true);
                    BDet->SetHistogramsRaw1DFolder("Raw");
                    BDet->SetHistogramsRaw2DFolder("Raw");

                    if(Calib)
                    {
                        BDet->SetHistogramsCal(true);
                        BDet->SetHistogramsCal1DFolder("Cal");
                        BDet->SetHistogramsCal2DFolder("Cal");
                    }

                }
#ifdef WITH_VAMOS
                else if(strcmp(dtype,"MTDC") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    //   UShort_t MaxMult = 1;
                    IsNumexo = false;
                    if(Calib)
                        CalData=true;
                    BDet = new MTDC(dname,NCh,RawData,CalData,DefCal);
                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                        {
                            BDet->SetAnalysisMode(MODE_D2A);
                            FlagOutRawI = false;
                            FlagOutRawF = false;
                            FlagOutRawV = true;
                        }
                        else
                        {
                            BDet->SetAnalysisMode(MODE_D2R);
                            FlagOutRawI = false;
                            FlagOutRawF = false;
                            FlagOutRawV = true;
                        }
                    }
                    if(BDet->HasRawData())
                    {
                        BDet->SetHistogramsRaw(true);
                        BDet->SetHistogramsRaw1DFolder("Raw");
                        BDet->SetHistogramsRaw2DFolder("Raw");
                    }
                    if(Calib)
                    {
                        BDet->SetHistogramsCal(true);
                        BDet->SetHistogramsCal1DFolder("Cal");
                        BDet->SetHistogramsCal2DFolder("Cal");
                    }
                }
#endif
#ifdef WITH_VAMOS
                else if(strcmp(dtype,"MTOF") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
		    IF.GetLine(Line,Len);
		    cout<<Line<<endl;
		    if( ! (sscanf(Line,"RefMTDC_TMW1 : %d",&RefMTDC_TMW[0]) == 1))
		      {
			Char_t Message[500];
			sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
			MErr * Er= new MErr(WhoamI,0,0, Message);
			throw Er;
		      }
		    IF.GetLine(Line,Len);
		    cout<<Line<<endl;
		    if( ! (sscanf(Line,"RefCh_TMW1 : %d",&RefCh_TMW[0]) == 1))
		      {
			Char_t Message[500];
			sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
			MErr * Er= new MErr(WhoamI,0,0, Message);
			throw Er;
		      }
		    IF.GetLine(Line,Len);
		    cout<<Line<<endl;
		    if( ! (sscanf(Line,"RefMTDC_TMW2 : %d",&RefMTDC_TMW[1]) == 1))
		      {
			Char_t Message[500];
			sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
			MErr * Er= new MErr(WhoamI,0,0, Message);
			throw Er;
		      }
		    IF.GetLine(Line,Len);
		    cout<<Line<<endl;
		    if( ! (sscanf(Line,"RefCh_TMW2 : %d",&RefCh_TMW[1]) == 1))
		      {
			Char_t Message[500];
			sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
			MErr * Er= new MErr(WhoamI,0,0, Message);
			throw Er;
		      }
		    
		    
                    //   UShort_t MaxMult = 1;
                    IsNumexo = false;
                    if(Calib)
                        CalData=true;
                    BDet = new MTOF(dname,NCh,RawData,CalData,DefCal,"");
		    for(int k=0;k<2;k++)
		      {
			((MTOF*) BDet)->SetRef(k,RefMTDC_TMW[k],RefCh_TMW[k]);
		      }
                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                        {
                            BDet->SetAnalysisMode(MODE_D2A);
                            FlagOutRawI = false;
                            FlagOutRawF = false;
                            FlagOutRawV = true;
                        }
                        else
                        {
                            BDet->SetAnalysisMode(MODE_D2R);
                            FlagOutRawI = false;
                            FlagOutRawF = false;
                            FlagOutRawV = true;
                        }
                    }
                    if(BDet->HasRawData())
                    {
                        BDet->SetHistogramsRaw(true);
                        BDet->SetHistogramsRaw1DFolder("Raw");
                        //BDet->SetHistogramsRaw2DFolder("Raw");
                    }
                    if(Calib)
                    {
                        BDet->SetHistogramsCal(true);
                        BDet->SetHistogramsCal1DFolder("Cal");
			// BDet->SetHistogramsCal2DFolder("Cal");
                    }
                }
#endif
#ifdef WITH_VAMOS
                else if(strcmp(dtype,"TargetSi") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    //   UShort_t MaxMult = 1;
                    IsNumexo = false;
                    if(Calib)
                        CalData=true;
                    BDet = new TargetSi(dname,NCh,RawData,CalData,DefCal);
                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                        {
                            BDet->SetAnalysisMode(MODE_D2A);
                            FlagOutRawI = false;
                            FlagOutRawF = false;
                            FlagOutRawV = true;
                        }
                        else
                        {
                            BDet->SetAnalysisMode(MODE_D2R);
                            FlagOutRawI = false;
                            FlagOutRawF = false;
                            FlagOutRawV = true;
                        }
                    }
                    if(BDet->HasRawData())
                    {
                        BDet->SetHistogramsRaw(true);
                        BDet->SetHistogramsRaw1DFolder("Raw");
                        BDet->SetHistogramsRaw2DFolder("Raw");
                    }
                    if(Calib)
                    {
                        BDet->SetHistogramsCal(true);
                        BDet->SetHistogramsCal1DFolder("Cal");
                        BDet->SetHistogramsCal2DFolder("Cal");
                    }
                }
#endif
#ifdef WITH_VAMOS
                else if(strcmp(dtype,"TMW") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"ChannelsX : %d",&NChX) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted expecting ChannelsX : ",DetFileName.c_str());
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"ChannelsY : %d",&NChY) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted expecting ChannelsY : ",DetFileName.c_str());
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"StoreCharges : %d",&Charges) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted expecting StoreCharges : ",DetFileName.c_str());
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    BDet = new TMW(dname,NChX,NChY,true,Calib,true,true,"");
                    for(Int_t j=0;j<2;j++)
                    {
                        BDet->GetSubDet(j)->SetOutAttachCalI(false);
                        BDet->GetSubDet(j)->SetOutAttachCalF(false);
                        BDet->GetSubDet(j)->SetOutAttachCalV(Charges);

                        BDet->GetSubDet(j)->SetOutAttachRawI(false);
                        BDet->GetSubDet(j)->SetOutAttachRawF(false);
                        BDet->GetSubDet(j)->SetOutAttachRawV(Charges);
                    }

                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
			  {
			    BDet->SetAnalysisMode(MODE_D2A);			    
			    FlagOutCalI = true;
			    FlagOutCalF = false;
			    FlagOutCalV = false;
			    
			  }
			else
			  {
			    BDet->SetAnalysisMode(MODE_D2R);
			    FlagOutRawI = true;
			    FlagOutRawF = false;
			    FlagOutRawV = false;
			  }
                    }
                }
#endif
#ifdef WITH_FALSTAFF
                else if(strcmp(dtype,"FSTPosition") == 0)
                {
                    IsGet = true;

                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"ChannelsX : %d",&NChX) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted expecting ChannelsX : ",DetFileName.c_str());
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"ChannelsY : %d",&NChY) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted expecting ChannelsY : ",DetFileName.c_str());
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    BDet = new FST_Position(dname,NChX,NChY,true,Calib,false,"");

                    //BDet->SetParametersGET(PL_GET, MyMap);
                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }
                }

                else if(strcmp(dtype,"FSTID") == 0)
                {
                    BDet = new FST_ID(dname);
                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }
                }

#endif
#ifdef WITH_VAMOS
                else if(strcmp(dtype,"IC") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"NRows : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting NRows : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"MaxMult : %d",&MaxMult) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting MaxMult : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    IsNumexo = true;

                    BDet = new IonisationChamberNUMEXO(dname,NCh,NCh,RawData,Calib,DefCal,"",MaxMult);

                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }
                    BDet->SetHistogramsRaw(true);
                    BDet->SetHistogramsRaw1DFolder("Raw");
                    BDet->SetHistogramsRaw2DFolder("Raw");

                    if(Calib)
                    {
                        BDet->SetHistogramsCal(true);
                        BDet->SetHistogramsCal1DFolder("Cal");
                        BDet->SetHistogramsCal2DFolder("Cal");
                    }
                    BDet->SetParameters(PL_NUMEXO, MyMap);
                }
#endif
#ifdef WITH_VAMOS
                else if(strcmp(dtype,"nDET") == 0)
                {
                    IsNumexo = true;

                    BDet = new nDetector(dname,1,RawData,Calib,DefCal,"");

                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }
                    BDet->SetHistogramsRaw(true);
                    BDet->SetHistogramsRaw1DFolder("Raw");
                    BDet->SetHistogramsRaw2DFolder("Raw");

                    if(Calib)
                    {
                        BDet->SetHistogramsCal(true);
                        BDet->SetHistogramsCal1DFolder("Cal");
                        BDet->SetHistogramsCal2DFolder("Cal");
                    }
                    BDet->SetParameters(PL_NUMEXO, MyMap);
                }
#endif
#ifdef WITH_CATS
                else if(strcmp(dtype,"CATS") == 0)
                {

                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted expecting Channels : ",DetFileName.c_str());
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    IsNumexo = false;

                    BDet = new CATS(dname,NCh,RawData,Calib,DefCal,"");

                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }
                    BDet->SetHistogramsRaw(true);
                    BDet->SetHistogramsRaw1DFolder("Raw");
                    BDet->SetHistogramsRaw2DFolder("Raw");

                    if(Calib)
                    {
                        BDet->SetHistogramsCal(true);
                        BDet->SetHistogramsCal1DFolder("Cal");
                        BDet->SetHistogramsCal2DFolder("Cal");
                    }
                }
#endif
#ifdef WITH_VAMOS
                else if(strcmp(dtype,"VamosTP") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"StoreCharges : %d",&Charges) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted expecting StoreCharges : ",DetFileName.c_str());
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
		  
                    BDet = new TargetPos("TP",2,RawData,Calib,DefCal,"");
		    for(Int_t k=0;k<2;k++)
		      {
			BDet->GetSubDet(k)->SetOutAttachCalI(true);			    
			for(Int_t j=0;j<2;j++)
			  {
			    BDet->GetSubDet(k)->GetSubDet(j)->SetOutAttachCalI(false);
			    BDet->GetSubDet(k)->GetSubDet(j)->SetOutAttachCalF(false);
			    BDet->GetSubDet(k)->GetSubDet(j)->SetOutAttachCalV(Charges);

                BDet->GetSubDet(k)->GetSubDet(j)->SetOutAttachRawI(false);
                BDet->GetSubDet(k)->GetSubDet(j)->SetOutAttachRawF(false);
                BDet->GetSubDet(k)->GetSubDet(j)->SetOutAttachRawV(Charges);
			  }
		      }
		    
                    if(atoi(CM->Get("IsWatcher").c_str()))
		      BDet->SetAnalysisMode(MODE_WATCHER);
                    else
		      {
			if(Calib)
			  {
			    BDet->SetAnalysisMode(MODE_D2A);
			    
			    FlagOutCalI = true;
			    FlagOutCalF = false;
			    FlagOutCalV = false;
			    
			  }
			else
			  {
			    BDet->SetAnalysisMode(MODE_D2R);
			    FlagOutRawI = false;
			    FlagOutRawF = false;
			    FlagOutRawV = false;
			  }
		      }

                }
#endif
#ifdef WITH_CATS
                else if(strcmp(dtype,"CATSTracker") == 0)
                {


                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted expecting Channels : ",DetFileName.c_str());
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    BDet = new CATSTracker(dname,NCh,RawData,Calib,DefCal,"");

                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }
                }
#endif
#ifdef WITH_LISE
                else  if(strcmp(dtype,"DriftChamberZDD") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    UShort_t MaxMult = 1;
                    IsNumexo = true;
                    BDet = new DriftChamberZDD(dname,NCh,true,Calib,false,"",MaxMult);
                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }
                    BDet->SetHistogramsRaw(true);
                    BDet->SetHistogramsRaw1DFolder("Raw");
                    BDet->SetHistogramsRaw2DFolder("Raw");

                    if(Calib)
                    {
                        BDet->SetHistogramsCal(true);
                        BDet->SetHistogramsCal1DFolder("Cal");
                        BDet->SetHistogramsCal2DFolder("Cal");
                    }



                }
#endif
#ifdef WITH_VAMOS
                else if(strcmp(dtype,"MWPat") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"MaxMult : %d",&MaxMult) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting MaxMult : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    IsNumexo = true;
                    BDet = new FPMWPattern(dname,NCh,RawData,Calib,DefCal,"",MaxMult);

                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }
                    BDet->SetParameters(PL_NUMEXO, MyMap);
                }
                else if(strcmp(dtype,"VamosFP") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"StoreCharges : %d",&Charges) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted expecting StoreCharges : ",DetFileName.c_str());
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                    BDet =  new FocalPositionMW("FP",NCh,RawData,Calib,DefCal,"");
		    for(Int_t k=0;k<NCh;k++)
		      {
			BDet->GetSubDet(k)->SetOutAttachCalI(true);			    
			for(Int_t j=0;j<2;j++)
			  {
                BDet->GetSubDet(k)->GetSubDet(j)->SetOutAttachCalI(false);
                BDet->GetSubDet(k)->GetSubDet(j)->SetOutAttachCalF(false);
                BDet->GetSubDet(k)->GetSubDet(j)->SetOutAttachCalV(Charges);

                BDet->GetSubDet(k)->GetSubDet(j)->SetOutAttachRawI(false);
                BDet->GetSubDet(k)->GetSubDet(j)->SetOutAttachRawF(false);
                BDet->GetSubDet(k)->GetSubDet(j)->SetOutAttachRawV(Charges);
              }
		      }
		    
                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                        {
                            BDet->SetAnalysisMode(MODE_D2A);

                            FlagOutCalI = true;
                            FlagOutCalF = false;
                            FlagOutCalV = false;

                        }
                        else
                        {
                            BDet->SetAnalysisMode(MODE_D2R);
                            FlagOutRawI = false;
                            FlagOutRawF = false;
                            FlagOutRawV = false;
                        }
                    }
                }
                else if(strcmp(dtype,"VamosRec") == 0)
                {
                    RawData=false;
                    DefCal=false;
                    if (Calib)
                        CalData = true;
                    BDet =  new Reconstruction(dname,0,RawData,CalData,DefCal,"");
                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }
                    ((Reconstruction*) BDet)->SetBrhoRef(GetCurrentBrhoRef());

                }
                // else if(strcmp(dtype,"TMW") == 0)
                // {
                //     Int_t NWiresX;
                //     Int_t NWiresY;
                //     IF.GetLine(Line,Len);
                //     if( ! (sscanf(Line,"NWiresX : %d",&NWiresX) == 1))
                //     {
                //         Char_t Message[500];
                //         sprintf(Message,"Config File %s - <%s>  is corrupted expecting NWiresX : ",DetFileName.c_str(),dname);
                //         MErr * Er= new MErr(WhoamI,0,0, Message);
                //         throw Er;
                //     }
                //     IF.GetLine(Line,Len);
                //     if( ! (sscanf(Line,"NWiresY : %d",&NWiresY) == 1))
                //     {
                //         Char_t Message[500];
                //         sprintf(Message,"Config File %s - <%s>  is corrupted expecting NWiresY : ",DetFileName.c_str(),dname);
                //         MErr * Er= new MErr(WhoamI,0,0, Message);
                //         throw Er;
                //     }
		//     IF.GetLine(Line,Len);
                //     if( ! (sscanf(Line,"StoreCharges : %d",&Charges) == 1))
		//       {
                //         Char_t Message[500];
                //         sprintf(Message,"Config File %s is corrupted expecting StoreCharges : ",DetFileName.c_str());
                //         MErr * Er= new MErr(WhoamI,0,0, Message);
                //         throw Er;
		//       }
		    
                //     PosInfo = true;
                //     DefCal  = true;
                //     if (Calib)
                //         CalData = true;

                //     BDet =  new TMW(dname,NWiresX,NWiresY,RawData,CalData,DefCal,PosInfo,"");
		    
		//     for(Int_t j=0;j<2;j++)
		//       {
		// 	BDet->GetSubDet(j)->SetOutAttachCalI(false);
		// 	BDet->GetSubDet(j)->SetOutAttachCalF(false);
		// 	BDet->GetSubDet(j)->SetOutAttachCalV(Charges);
		//       }

		//     if(atoi(CM->Get("IsWatcher").c_str()))
                //         BDet->SetAnalysisMode(MODE_WATCHER);
                //     else
                //     {
                //         if(Calib)
		// 	  {
		// 	    BDet->SetAnalysisMode(MODE_D2A);			    
		// 	    FlagOutCalI = true;
		// 	    FlagOutCalF = false;
		// 	    FlagOutCalV = false;
			    
		// 	  }
		// 	else
		// 	  {
		// 	    BDet->SetAnalysisMode(MODE_D2R);
		// 	    FlagOutRawI = true;
		// 	    FlagOutRawF = false;
		// 	    FlagOutRawV = false;
		// 	  }
                //     }
                // }
                else if(strcmp(dtype,"VamosId") == 0)
                {



                }
#endif
#ifdef WITH_FALSTAFF
                else if(strcmp(dtype,"FALSTAFF_CHIO") == 0)
                {

		  IF.GetLine(Line,Len);
		  if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
		      Char_t Message[500];
		      sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
		      MErr * Er= new MErr(WhoamI,0,0, Message);
		      throw Er;
                    }

		  BDet = new FST_Chio(dname,NCh,true,false,false,"",FalstaffParOffSet);

		  //BDet->SetParametersGET(PL_GET, MyMap);
		  if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }
                }
#endif
#ifdef WITH_PARIS
                else if(strcmp(dtype,"PARIS") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Clusters : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Clusters : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    //   UShort_t MaxMult = 1;
                    IsNumexo = true;
                    BDet = new Paris(dname,NCh,true,Calib,false,"");
                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }
                    BDet->SetHistogramsRaw(true);
                    BDet->SetHistogramsRaw1DFolder("Raw");
                    BDet->SetHistogramsRaw2DFolder("Raw");

                    if(Calib)
                    {
                        BDet->SetHistogramsCal(true);
                        BDet->SetHistogramsCal1DFolder("Cal");
                        BDet->SetHistogramsCal2DFolder("Cal");
                    }
                }
#endif
#ifdef WITH_EXOGAM
                else if(strcmp(dtype,"EXOGAM") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Clovers : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    //   UShort_t MaxMult = 1;
                    IsNumexo = true;
                    BDet = new Exogam(dname,NCh,true,Calib,false,"");

                    for(UShort_t i=0; i < NCh; i++)
                    {
                        IF.GetLine(Line,Len);
                        if( ! (sscanf(Line,"Cl %d : %d",&Ch,&Fl) == 2))
                        {
                            Char_t Message[500];
                            sprintf(Message,"Config File %s is corrupted (%s): expecting Cl_%02d : FlangeNr",DetFileName.c_str(),dname,i);
                            MErr * Er= new MErr(WhoamI,0,0, Message);
                            throw Er;
                        }
                        if(Ch != i)
                        {
                            Char_t Message[500];
                            sprintf(Message,"Config File %s is corrupted (%s): expecting Cl_%02d : FlangeNr",DetFileName.c_str(),dname,i);
                            MErr * Er= new MErr(WhoamI,0,0, Message);
                            throw Er;
                        }
                        cout << "\t Cl " << Ch << " <-> Flange : " <<  Fl  << endl;
                        ((Exogam*) BDet)->SetFlangeNr(i,Fl);
                        if(Calib)
                        {
                            sprintf(CalName,"%s_C",PName);
                            BDet->SetCalName(CalName,i);
                        }
                    }

		    // ExtendedBranches (Inner6, Inner20, Outers, BGO, CSI, ...)
		    IF.GetLine(Line,Len);
		    if( ! (sscanf(Line,"AllBranches : %d",&Charges) == 1))
		      {
			Char_t Message[500];
			sprintf(Message,"Config File %s - <%s>  is corrupted expecting AllBranches : ",DetFileName.c_str(),dname);
			MErr * Er= new MErr(WhoamI,0,0, Message);
			throw Er;
		      }

                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
		      for(Int_t k=0; k < BDet->GetNumberOfSubDetectors();k++)
			{
			  if(Calib)
			    {
			      BDet->GetSubDet(k)->SetOutAttachCalV(Charges);
			      BDet->GetSubDet(k)->SetOutAttachTime(Charges);
			      BDet->GetSubDet(k)->SetOutAttachCalTS(Charges);
			      BDet->GetSubDet(k)->SetOutAttachCalI(false);
			    }
			  else
			    { 
			      BDet->GetSubDet(k)->SetOutAttachRawV(Charges);
			      BDet->GetSubDet(k)->SetOutAttachRawI(false);
			   
			    }
			}
		      
		      if(Calib)
                        {
                            BDet->SetAnalysisMode(MODE_D2A);
                            FlagOutCalI = false;
                            FlagOutCalF = false;
                            FlagOutCalV = false;

                        }
                        else
                        {
                            BDet->SetAnalysisMode(MODE_D2R);
                            FlagOutRawI = false;
                            FlagOutRawF = false;
                            FlagOutRawV = false;
                        }
                    }
                    BDet->SetHistogramsRaw(true);
                    BDet->SetHistogramsRaw1DFolder("Raw");
                    BDet->SetHistogramsRaw2DFolder("Raw");

                    if(Calib)
                    {
                        BDet->SetHistogramsCal(true);
                        BDet->SetHistogramsCal1DFolder("Cal");
                        BDet->SetHistogramsCal2DFolder("Cal");
                    }
                }
#endif
#ifdef WITH_LISE
                else  if(strcmp(dtype,"AnnularS1") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    UShort_t MaxMult = 1;
                    IsNumexo = true;
                    BDet = new SiAnnular(dname,NCh,true,Calib,false,"",MaxMult);
                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }
                    BDet->SetHistogramsRaw(true);
                    BDet->SetHistogramsRaw1DFolder("Raw");
                    BDet->SetHistogramsRaw2DFolder("Raw");

                    if(Calib)
                    {
                        BDet->SetHistogramsCal(true);
                        BDet->SetHistogramsCal1DFolder("Cal");
                        BDet->SetHistogramsCal2DFolder("Cal");
                    }
                }
                else  if(strcmp(dtype,"Caviar") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    UShort_t MaxMult = 1;
                    BDet = new Caviar(dname,NCh,true,Calib,false,"");
                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }
                    BDet->SetHistogramsRaw(true);
                    BDet->SetHistogramsRaw1DFolder("Raw");
                    BDet->SetHistogramsRaw2DFolder("Raw");

                    if(Calib)
                    {
                        BDet->SetHistogramsCal(true);
                        BDet->SetHistogramsCal1DFolder("Cal");
                        BDet->SetHistogramsCal2DFolder("Cal");
                    }
                }
                else  if(strcmp(dtype,"IDLISE") == 0)
                {
                    Int_t NCuts=0;
//                    IF.GetLine(Line,Len);
//                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
//                    {
//                        Char_t Message[500];
//                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
//                        MErr * Er= new MErr(WhoamI,0,0, Message);
//                        throw Er;
//                    }

                    // Read Cuts :

                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"NCuts : %d",&NCuts) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting NCuts : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    UShort_t MaxMult = 1;
                    BDet = new IdLISE(dname,NCuts,true,Calib,false,"");


                    if(NCuts>0)
                    {
                      cout << " Loading " << NCuts << " dE-ToF identification cut(s)" << endl;

                      for(int k=0;k <NCuts; k++)
                      {
                          IF.GetLine(Line,Len);
                          cout << Line << endl;
                          if( ! (sscanf(Line,"Cut %d : %s %s ",&Ch,PName,FName) == 3))
                          {
                              Char_t Message[500];
                              sprintf(Message,"Config File %s - <%s>  is corrupted expecting Cut %02d : Tag ./Path/CutName.root",DetFileName.c_str(),dname,k+1);
                              MErr * Er= new MErr(WhoamI,0,0, Message);
                              throw Er;
                          }
                          if(Ch == k+1 && Ch < NCuts+1)
                          {
                                cout << "Adding Cut " << PName << endl;
                               ((IdLISE*)BDet)->AddCut(k,PName,FName);

                          }
                          else
                          {
                              Char_t Message[500];
                              sprintf(Message,"Config File %s - <%s>  is corrupted expecting Cut %02d (while read %02d) ",DetFileName.c_str(),dname, k+1, Ch);
                              MErr * Er= new MErr(WhoamI,0,0, Message);
                              throw Er;
                          }
                      }
                    }

                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }
                    BDet->SetHistogramsRaw(true);
                    BDet->SetHistogramsRaw1DFolder("Raw");
                    BDet->SetHistogramsRaw2DFolder("Raw");

                    if(Calib)
                    {
                        BDet->SetHistogramsCal(true);
                        BDet->SetHistogramsCal1DFolder("Cal");
                        BDet->SetHistogramsCal2DFolder("Cal");
                    }
                    ((IdLISE*) BDet)->SetBrhoRef(GetCurrentBrhoRef());

                }
#endif
#ifdef WITH_NFS
                else if(strcmp(dtype,"NFS_MEDLEY") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    cout << "Calib " << Calib <<  endl;
                    BDet = new Medley(dname,NCh,true,Calib,false,"");

                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }


                }
#endif
#ifdef WITH_NFS
                else if(strcmp(dtype,"NFS_TDC") == 0)
                {
		  cout << "Calib " << Calib <<  endl;
		  BDet = new NFSTDC(dname,1,true,Calib,false,"");
		  
		  if(atoi(CM->Get("IsWatcher").c_str()))
		    BDet->SetAnalysisMode(MODE_WATCHER);
		  else
                    {
		      if(Calib)
			BDet->SetAnalysisMode(MODE_D2A);
		      else
			BDet->SetAnalysisMode(MODE_D2R);
                    } 
                }
                else if(strcmp(dtype,"NFS_PPACS_TDC") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    cout << "Calib " << Calib <<  endl;
                    BDet = new NFSPPACS_tdc(dname,NCh,true,Calib,false,"");

                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }
                }
                else if(strcmp(dtype,"NFS_PPACS") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    cout << "Calib " << Calib <<  endl;
                    BDet = new NFSPPACS(dname,NCh,true,Calib,false,"");

                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }
                }
#endif
#ifdef WITH_NFS
                else if(strcmp(dtype,"NFS_PPACSN") == 0)
		  {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
		      {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    cout << "Calib " << Calib <<  endl;
                    BDet = new NFSPPACSN(dname,NCh,true,Calib,false,"");

                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }


                }
#endif		
#ifdef WITH_NFS
                else if(strcmp(dtype,"NFS_NeutronDetector") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Channels : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    BDet = new NFSNeutronDet(dname,NCh,true,Calib,false,"");

                    for(UShort_t i=0; i < NCh; i++)
                    {
                        IF.GetLine(Line,Len);
                        if( ! (sscanf(Line,"PH %d : %s %d",&Ch,PName,&Th) == 3))
                        {
                            Char_t Message[500];
                            sprintf(Message,"Config File %s is corrupted (%s): expecting PH_%02d : ParName Threshold",DetFileName.c_str(),dname,i);
                            MErr * Er= new MErr(WhoamI,0,0, Message);
                            throw Er;
                        }
                        if(Ch != i)
                        {
                            Char_t Message[500];
                            sprintf(Message,"Config File %s is corrupted (%s): expecting PH_%02d : ParName Threshold",DetFileName.c_str(),dname,i);
                            MErr * Er= new MErr(WhoamI,0,0, Message);
                            throw Er;
                        }
                        cout << "\t PH " << Ch << " : " <<  PName  << " "  << Th << endl;
                        BDet->GetSubDet(0)->SetParameterName(PName,i);
                        sprintf(PName,"PH_%01d",Ch+1);
                        BDet->GetSubDet(0)->SetParameterNameInRawTree(PName,i);

                        ((NFSNeutronDet*) BDet)->SetNeutronThreshold(i,Th);

                        IF.GetLine(Line,Len);
                        if( ! (sscanf(Line,"ZC %d : %s %d ",&Ch,PName,&Th) == 3))
                        {
                            Char_t Message[500];
                            sprintf(Message,"Config File %s is corrupted (%s): expecting ZC_%02d : ParName Threshold",DetFileName.c_str(),dname,i);
                            MErr * Er= new MErr(WhoamI,0,0, Message);
                            throw Er;
                        }
                        if(Ch != i)
                        {
                            Char_t Message[500];
                            sprintf(Message,"Config File %s is corrupted (%s): expecting ZC_%02d : ParName Threshold",DetFileName.c_str(),dname,i);
                            MErr * Er= new MErr(WhoamI,0,0, Message);
                            throw Er;
                        }
                        cout << "\t ZC " << Ch << " : " <<  PName  << " "  << Th << endl;
                        BDet->GetSubDet(1)->SetParameterName(PName,i);
                        sprintf(PName,"ZC_%01d",Ch+1);
                        BDet->GetSubDet(1)->SetParameterNameInRawTree(PName,i);

                        // if(Calib)
                        // 	{
                        // 	  sprintf(CalName,"%s_C",PName);
                        // 	  BDet->SetCalName(CalName,i);
                        // 	}
                    }

                    // IF.GetLine(Line,Len);
                    // if( ! sscanf(Line,"NeutronTOFROI : %f %f ",&LLF,&ULF) == 2)
                    // {
                    //     Char_t Message[500];
                    //     sprintf(Message,"Config File %s is corrupted (%s): expecting NeutronTOFROI : LL UL",DetFileName.c_str(),dname);
                    //     MErr * Er= new MErr(WhoamI,0,0, Message);
                    //     throw Er;
                    // }



                    for(UShort_t i=0; i < NCh; i++)
                    {
                        IF.GetLine(Line,Len);
                        if( ! (sscanf(Line,"NeutronTOFROI %d : %f %f",&Ch,&LLF,&ULF) == 3))
                        {
                            Char_t Message[500];
                            sprintf(Message,"Config File %s is corrupted (%s): expecting NeutronTOFROI %02d : LL UL",DetFileName.c_str(),dname,i);
                            MErr * Er= new MErr(WhoamI,0,0, Message);
                            throw Er;
                        }
                        if(Ch != i)
                        {
                            Char_t Message[500];
                            sprintf(Message,"Config File %s is corrupted (%s): expecting NeutronTOFROI %02d : LL UL",DetFileName.c_str(),dname,i);
                            MErr * Er= new MErr(WhoamI,0,0, Message);
                            throw Er;
                        }
                        cout << "\t NeutronTOFROI " << Ch << " : [" << LLF << " : " <<  ULF<< "] "<< endl;
                        ((NFSNeutronDet*) BDet)->SetNeutronTOFOI(i,LLF,ULF);

                    }


                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Neutron2DCuts : %d ",&Cal) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted (%s): expecting NeutronTOFROI : LL UL",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                    Int_t Has2DCuts = Cal;
                    cout << "\t NeutronCuts : " << Cal << endl;

                    if(Has2DCuts)
                    {
                        IF.GetLine(Line,Len);
                        if( ! (sscanf(Line,"Neutron2DCutsFile : %s",path) == 1))
                        {
                            Char_t Message[500];
                            sprintf(Message,"Config File %s is corrupted (%s): expecting Neutron2DCutsFile : path/to/cuts.root",DetFileName.c_str(),dname);
                            cout << Message << endl;
                            MErr * Er= new MErr(WhoamI,0,0, Message);
                            throw Er;
                        }else{
                            cout << "\t Neutron2DCutsFile : " << path << endl;
                            ((NFSNeutronDet*) BDet)->SetNeutron2DCutsFile(path);
                        }
                    }


                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                            BDet->SetAnalysisMode(MODE_D2A);
                        else
                            BDet->SetAnalysisMode(MODE_D2R);
                    }

                    //                    for(UShort_t i=0; i < NCh; i++)
                    //                    {
                    //                        IF.GetLine(Line,Len);
                    //                        if( ! sscanf(Line,"Ch %d : %s %d %d %d ",&Ch,&PName,&NBins,&LL,&UL) == 5)
                    //                        {
                    //                            Char_t Message[500];
                    //                            sprintf(Message,"Config File %s is corrupted (%s): expecting Ch_%02d : ParName Nbins LL UL",DetFileName.c_str(),dname,i);
                    //                            MErr * Er= new MErr(WhoamI,0,0, Message);
                    //                            throw Er;
                    //                        }
                    //                        if(Ch != i)
                    //                        {
                    //                            Char_t Message[500];
                    //                            sprintf(Message,"Config File %s is corrupted (%s): expecting Ch_%02d : ParName Nbins LL UL",DetFileName.c_str(),dname,i);
                    //                            MErr * Er= new MErr(WhoamI,0,0, Message);
                    //                            throw Er;
                    //                        }
                    //                        cout << "\t Ch " << Ch << " : " <<  PName  << " "  << NBins << " " << LL << " " << UL << endl;
                    //                        BDet->SetParameterName(PName,i);
                    //                        BDet->SetRawHistogramsParams(NBins,LL,UL);
                    //                    }



                }
#endif
#ifdef WITH_PISTA
                else if(strcmp(dtype,"PISTA") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Telescopes : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting Telescopes : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"NStripsDE : %d",&NChX) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting NStripsDE : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"NStripsE : %d",&NChY) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting NStripsE : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }

                    //   UShort_t MaxMult = 1;
                    IsNumexo = false;
                    if(Calib)
                        CalData=true;
                    BDet = new Pista(dname,NCh,NChX,NChY,RawData,CalData,DefCal,"");
                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                    {
                        if(Calib)
                        {
                            BDet->SetAnalysisMode(MODE_D2A);
                            FlagOutRawI = false;
                            FlagOutRawF = false;
                            FlagOutRawV = true;
                        }
                        else
                        {
                            BDet->SetAnalysisMode(MODE_D2R);
                            FlagOutRawI = false;
                            FlagOutRawF = false;
                            FlagOutRawV = true;
                        }
                    }
                    if(BDet->HasRawData())
                    {
                        BDet->SetHistogramsRaw(true);
                        BDet->SetHistogramsRaw1DFolder("Raw");
                        BDet->SetHistogramsRaw2DFolder("Raw");
                    }
                    if(Calib)
                    {
                        BDet->SetHistogramsCal(true);
                        BDet->SetHistogramsCal1DFolder("Cal");
                        BDet->SetHistogramsCal2DFolder("Cal");
                    }
                }
#endif
#ifdef WITH_NPTOOL
                else if(strcmp(dtype,"NPINTERFACE") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"ConfFileName : %s",&FName) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s - <%s>  is corrupted expecting ConfFileName : ",DetFileName.c_str(),dname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                    IsNumexo = false;
                    BDet = new NPInterface(dname,1,RawData,false,false,FName);
                    if(atoi(CM->Get("IsWatcher").c_str()))
                        BDet->SetAnalysisMode(MODE_WATCHER);
                    else
                        BDet->SetAnalysisMode(MODE_D2A);

               }
#endif
                else
                {
                    Char_t Message[500];
                    sprintf(Message,"Config File %s is corrupted unrecognized Type of Detector in Detector Config File %s",DetFileName.c_str(), dtype);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                    /// Other Types ///
                }
                BDet->SetHistoManager(H);


                if(IsNumexo)
		  {
                    //PL_NUMEXO->Print();
                    BDet->SetParametersNUMEXO(PL_NUMEXO, MyMap);
		    
		  }
#ifdef WITH_MESYTEC_DATA
		else if(IsMesytec)
		  {
		    BDet->SetParametersMesytec(MesytecPar,MyMap);
		  }
#endif
                else if(IsGet)
                {
#ifdef WITH_GET
                    BDet->SetParametersGET(PL_GET, MyMap);
#else

#endif
                }
                else
		  {
                    BDet->SetParameters(PL, MyMap);
		  }



                if(atoi(CM->Get("IsWatcher").c_str()) == 0)
                {

                    if(BDet->HasRawData())
                    {
                        BDet->SetOutAttachRawI(FlagOutRawI);
                        BDet->SetOutAttachRawV(FlagOutRawV);
                        BDet->SetOutAttachRawF(FlagOutRawF);
                    }
                    if(BDet->HasTS())
                    {
                        //BDet->SetOutAttachRawV(true);
                        BDet->SetOutAttachTS(true);
                    }
                    if(BDet->HasTime())
                    {
                        BDet->SetOutAttachTime(true);

                    }
                    if(BDet->HasCalData())
                    {
                        BDet->SetOutAttachCalI(FlagOutCalI);
                        BDet->SetOutAttachCalF(FlagOutCalF);
                        BDet->SetOutAttachCalV(FlagOutCalV);
                    }
                }

#ifdef WITH_ROOT
                BDet->SetIOOptions(OutTFile,dname,H,OutTTree,InTTree);
#endif


                if(BDet->GetVerbose() >= V_INFO)
                    BDet->PrintOptions(cout);
                BDet->PrintOptions(L->File);
		//BDet->PrintOptions(cout);
                AddComponent(BDet);
            }


        }

    }
    IF.Close();

    END;
}

void Analysis::SetUpAnalysis()
{
    START;
    DM = DetManager::getInstance();

    // Confiugration Manager
    SetUpDetectorsFromFile(CM->Get("DetectorsFile"));
    // Eventually Fetch the detector Pointers;
#ifdef WITH_PARIS
	try{
	  ParisDet = (Paris*) DM->GetDetector("Paris");
	}
	catch(MErr *Er)
	  {
	    ParisDet = NULL;
	    Er->Print();
	    delete Er;
	  }
#endif
    END;
}

Bool_t Analysis::NextFile()
{
    START;
    CurrentFile++;
    if(CurrentFile <= IFN->size())
        return(true);
    else
        return(false);
    END;
}

string Analysis::GetCurrentInputFile(void)
{
    START;
    if(CurrentFile <= IFN->size())
    {
        return(IFN->at(CurrentFile-1));
    }
    else
    {
        Char_t Message[100];
        sprintf(Message,"Wrong Call to %s",WhoamI);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

string Analysis::GetCurrentOutputFile(void)
{
    START;
    if(CurrentFile <= OFN->size())
    {
        return(OFN->at(CurrentFile-1));
    }
    else
    {
        Char_t Message[100];
        sprintf(Message,"Wrong Call to %s",WhoamI);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

string Analysis::GetCurrentParFile(void)
{
    START;
    if(CurrentFile <= ParFN->size())
    {
        return(ParFN->at(CurrentFile-1));
    }
    else
    {
        Char_t Message[100];
        sprintf(Message,"Wrong Call to %s",WhoamI);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

Float_t Analysis::GetCurrentBrhoRef(void)
{
   START;
   if(CurrentFile == 0) // When no data file are handled by Analysis Class (Wtacher, Calc or GW)
   {
      if(BrhoRef.size() == 1)
      {
         return BrhoRef.at(0);
      }
      else
      {
         Char_t Message[100];
         sprintf(Message,"Wrong Call to %s - Brho Vaue not set in Watcher",WhoamI);
         MErr * Er = new MErr(WhoamI,0,0, Message);
         throw Er;
      }
   }
   else
   {
       cout << CurrentFile << " " << BrhoRef.size()<<endl;
      if(CurrentFile <= BrhoRef.size())
      {
         L->File << "BrhoRef : " << BrhoRef[CurrentFile-1] << endl;
         return(BrhoRef.at(CurrentFile-1));
      }
      else
      {
         Char_t Message[100];
         sprintf(Message,"Wrong Call to %s - Current File %d",WhoamI,CurrentFile);
         MErr * Er = new MErr(WhoamI,0,0, Message);
         throw Er;
      }
   }
   END;
}


Float_t Analysis::GetBrhoRef(UShort_t RunNr)
{
   START;
   char Line[300];
   stringstream *InOut;
   Float_t Brho;

   InOut = new stringstream();
   *InOut  << getenv((EM->getPathVar()).c_str()) << "/Calibs/ListBrho.dat";
   *InOut>>Line;
   MIFile *IF = new MIFile(Line);
   delete InOut;
   cout << "  <Analysis><Brho> Reading " << Line << endl;
   Brho=0.0;
   IF->GetLine(Line,300);
   while(IF->GetLine(Line,300)&&Brho<0.1)
   {
      if(strncmp(Line,"//",2)!=0)
      {
         stringstream InOut;
         InOut << Line;
         Int_t Run;
         Float_t Br;
         InOut >> Run >> Br;
         if(Run == RunNr)
         {
            Brho = Br;
            cout << "  <Analysis><Brho> Found Match : " ;
            cout << Line << endl;
         }

      }
   }
   if(Brho > 0.0)
   {
      cout << "  <Analysis><Brho>  RUN Number " << RunNr << endl;
      while(IF->GetLine(Line,300)&&(strncmp(Line,"//",2)==0))
      {
         cout << Line << endl;
      }

   }
   delete IF;
   if(Brho == 0.0)
   {
      char ErrLine[200];
      stringstream ErrL;
      ErrL << "Could not find the BrhoRef for the run " << RunNr;
      ErrL.getline(ErrLine,sizeof ErrLine);
      MErr * Er = new MErr(WhoamI,0,0,ErrLine);
      throw Er;
   }


   return Brho;
   END;
}



Float_t Analysis::GetBrhoRef(string IF)
{
   START;
   Char_t RunNrS[5];
   UShort_t  RunNr=0;
   size_t Start=std::string::npos;
   size_t Stop=std::string::npos;
   Char_t fmt[10];
   // Data file
   if (fModeWhich[2])
   {
      Start = IF.find("run_");
      Stop = IF.find(".dat.");
      if ((Start!=std::string::npos) && (Stop!=std::string::npos))
      {
         Start += 4;
      }
   }
   else if(fModeWhich[3])
   {
      Start = IF.find("r");
      Stop = IF.find(".root");
      if ((Start!=std::string::npos) && (Stop!=std::string::npos))
      {
         Start +=1;
         Stop -=5;
      }

   }
   if(!fModeWhich[1])
   {
      if ((Start!=std::string::npos) && (Stop!=std::string::npos))
      {
         IF.copy(RunNrS,Stop-Start,Start);
         RunNrS[Stop-Start]='\0';
         sprintf(fmt,"%%0%dd",(Int_t)(Stop-Start));
         if(sscanf(RunNrS,fmt,&RunNr))
         {
            return GetBrhoRef(RunNr);
         }
         else
         {
            char ErrLine[200];
            sprintf(ErrLine,"Could not extract Run number from input file name %s", IF.c_str());
            MErr * Er = new MErr(WhoamI,0,0,ErrLine);
            throw Er;
         }
      }
      else
      {
         char ErrLine[200];
         sprintf(ErrLine,"Could not extract Run number from input file name %s", IF.c_str());
         MErr * Er = new MErr(WhoamI,0,0,ErrLine);
         throw Er;
      }
   }
   return 0;
   END;
}

Int_t Analysis::GetRunNr(string IF)
{
    START;
    Char_t RunNrS[5];
    UShort_t  RunNr=0;
    size_t Start=std::string::npos;
    size_t Stop=std::string::npos;
    Char_t fmt[10];
    // Data file
    if (fModeWhich[2])
    {
        Start = IF.find("run_");
        Stop = IF.find(".dat.");
        if ((Start!=std::string::npos) && (Stop!=std::string::npos))
        {
            Start += 4;
        }
    }
    else if(fModeWhich[3])
    {
        Start = IF.find("r");
        Stop = IF.find(".root");
        if ((Start!=std::string::npos) && (Stop!=std::string::npos))
        {
            Start +=1;
            Stop -=5;
        }

    }
    if(!fModeWhich[1])
    {
        if ((Start!=std::string::npos) && (Stop!=std::string::npos))
        {
            IF.copy(RunNrS,Stop-Start,Start);
            RunNrS[Stop-Start]='\0';
            sprintf(fmt,"%%0%dd",(Int_t)(Stop-Start));
            if(sscanf(RunNrS,fmt,&RunNr))
            {
                return RunNr;
            }
            else
            {
                char ErrLine[200];
                sprintf(ErrLine,"Could not extract Run number from input file name %s", IF.c_str());
                MErr * Er = new MErr(WhoamI,0,0,ErrLine);
                throw Er;
            }
        }
        else
        {
            char ErrLine[200];
            sprintf(ErrLine,"Could not extract Run number from input file name %s", IF.c_str());
            MErr * Er = new MErr(WhoamI,0,0,ErrLine);
            throw Er;
        }
    }
    return 0;
    END;
}

void Analysis::OpenInputTree(void)
{
    START;
#ifdef WITH_ROOT

    cout << "Openning : " << GetCurrentInputFile();
    L->File << "Openning : " <<GetCurrentInputFile();

    InTFile = new TFile(GetCurrentInputFile().c_str(),"read");

    if(!InTFile)
    {
        char ErrLine[200];
        stringstream ErrL;
        ErrL << "Could not open input file " << GetCurrentInputFile();
        ErrL.getline(ErrLine,sizeof ErrLine);
        MErr * Er = new MErr(WhoamI,0,0,ErrLine);
        throw Er;
    }
    cout << " O.K." << endl;

#ifdef DEBUG
    InTFile->ls();
#endif

    cout << "Allocating Raw_Data from " << GetCurrentInputFile();
    L->File << "Allocating Raw_Data from " << GetCurrentInputFile();

    if(fAnMode == MODE_R2A)
        InTTree = (TTree *) InTFile->Get("RD");

    if(fAnMode == MODE_RECAL)
        InTTree = (TTree *) InTFile->Get("AD");

    if(!InTTree)
    {
        char ErrLine[200];
        stringstream ErrL;
        if(fAnMode == MODE_R2A)
            ErrL << "Could not allocate RD Tree from " << GetCurrentInputFile();
        if(fAnMode == MODE_RECAL)
        {
            InTFile->ls();
            ErrL << "Could not allocate AD Tree from " << GetCurrentInputFile();

        }

        ErrL.getline(ErrLine,sizeof ErrLine);
        MErr * Er = new MErr(WhoamI,0,0,ErrLine);
        throw Er;
    }
    cout << " O.K." << endl;
    L->File << " O.K." << endl;


#ifdef ACTIVEBRANCHES
    InTTree->SetBranchStatus("*",0);
#endif
#endif
    END;
}

void Analysis::SetBrhoWatcher(Float_t Brho)
{
    START;
    cout << " Set brho " << Brho << " And Mode " << fAnMode << endl;
    BrhoRef.push_back(Brho);
    ResetFileCounter();
    END;
}


void Analysis::CloseInputTree(void)
{
    START;
#ifdef WITH_ROOT
    if(InTFile)
        if(InTFile->IsOpen())
        {
            cout << "Deleting inTree : " << endl;
            L->File << "Deleting inTree : " << endl;
            InTTree = CleanDelete(InTTree);
            cout << "Closing file : " << GetCurrentInputFile() <<endl;
            L->File << "Closing file : " << GetCurrentInputFile() <<endl;
            InTFile->Close();
            InTFile = CleanDelete(InTFile);
        }
#endif
    END;
}

void Analysis::SetUpGeneralParameters(UShort_t fMode)
{
    START;

    BaseDetector *An = new BaseDetector("An",1,true,false,false,false,"");
    // Define General Parameter
    An->SetAnalysisMode(fMode);
    if(fMode == MODE_D2R || fMode ==MODE_D2A ||fMode == MODE_R2A ||fMode == MODE_WATCHER ||  fMode == MODE_CALC)
    {
        An->SetParameterName("GATCONF_MASTER",0);
        An->SetParameters(PL, MyMap);
    }

#ifdef WITH_ROOT
    An->SetMainHistogramFolder("An");
    An->SetHistogramsRaw1DFolder("");
    An->SetHistogramsRaw2DFolder("");
    An->SetHistoManager(H);
    An->SetHistogramsRaw1D(true);
    An->SetHistogramsRaw2D(true);
    An->SetOutAttachRawI(true);
    // An->SetOutAttachCalV(false);
    // An->SetOutAttachCalI(false);

    if(fMode == MODE_D2R || fMode ==MODE_D2A ||fMode == MODE_R2A )
        An->OutAttach(OutTTree);
    if(fMode == MODE_R2A)
        An->InAttach(InTTree);

    if(An->GetVerbose() >= V_INFO)
        An->PrintOptions(cout);
    An->PrintOptions(L->File);
    if(fMode !=4)
    {
        An->CreateHistograms(OutTFile->GetDirectory(""));
    }
#endif
    AddComponent(An);

#ifdef D_AGAVA_VAMOS
    VamosTS = new TimeStamp("AGAVA_VAMOSTS",3,true,false,false,"");
    if(VamosTS)
    {
        VamosTS->SetAnalysisMode(fMode);
        if(fMode == MODE_D2R || fMode ==MODE_D2A ||fMode == MODE_R2A ||fMode == MODE_WATCHER ||  fMode == MODE_CALC)
        {
            VamosTS->SetLowWord("AGAVA_VAMOS_LTAGL");
            VamosTS->SetMidWord("AGAVA_VAMOS_LTAGL_UP");
            VamosTS->SetHighWord("AGAVA_VAMOS_LTAGH");
            VamosTS->SetParameters(PL, MyMap);
        }
#ifdef WITH_ROOT
        VamosTS->SetMainHistogramFolder("An");
        VamosTS->SetHistogramsRaw1DFolder("");
        VamosTS->SetHistogramsRaw2DFolder("");
        VamosTS->SetHistoManager(H);
        if(fMode == MODE_D2R || fMode ==MODE_D2A ||fMode == MODE_R2A )
            VamosTS->OutAttach(OutTTree);
        if(fMode == MODE_R2A)
            VamosTS->InAttach(InTTree);
        // if(fMode == MODE_RECAL)
        //   An->InAttachCal(InTTree);

        if(VamosTS->GetVerbose() >= V_INFO)
            VamosTS->PrintOptions(cout);
        VamosTS->PrintOptions(L->File);
        if(fMode !=4)
        {
            VamosTS->CreateHistograms(OutTFile->GetDirectory(""));
        }
#endif
        AddComponent(VamosTS);

    }
#endif

#ifdef D_CENTRUM
    TimeStamp* CentrumTS = new TimeStamp("CentTS",3,true,true,false,"");
    CentrumTS->SetAnalysisMode(fMode);
    if(fMode == MODE_D2R || fMode ==MODE_D2A ||fMode == MODE_R2A ||fMode == MODE_WATCHER ||  fMode == MODE_CALC)
    {
        CentrumTS->SetLowWord("TIMEML");
        CentrumTS->SetMidWord("TIMEML_UP");
        CentrumTS->SetHighWord("TIMEH");
        CentrumTS->SetParameters(PL, MyMap);
    }
#ifdef WITH_ROOT
    CentrumTS->SetMainHistogramFolder("An");
    CentrumTS->SetHistogramsRaw1DFolder("");
    CentrumTS->SetHistogramsRaw2DFolder("");
    CentrumTS->SetHistoManager(H);
    if(fMode == MODE_D2R || fMode ==MODE_D2A ||fMode == MODE_R2A )
        CentrumTS->OutAttach(OutTTree);
    if(fMode == MODE_R2A)
        CentrumTS->InAttach(InTTree);
    // if(fMode == MODE_RECAL)
    //   An->InAttachCal(InTTree);

    if(CentrumTS->GetVerbose() >= V_INFO)
        CentrumTS->PrintOptions(cout);
    CentrumTS->PrintOptions(L->File);
    if(fMode !=4)
    {
        CentrumTS->CreateHistograms(OutTFile->GetDirectory(""));
    }
#endif

    AddComponent(CentrumTS);
#endif
    END;
}

#ifdef WITH_MFM

void Analysis::AddMFMData(MFMEbyedatFrame* frame)
{
    START;
    //UShort_t NItems = frame->GetNbItemsAttribut();
    //    UShort_t NItems = frame->GetNbItems();

    //    cout << NItems << endl;;
    //    for(UShort_t i=0;i<NItems;i++)
    //    {
    //        frame->EbyedatGetParameters(i, LblDataBuf, LblDataBuf+1);
    //        //      cout << "<" << *(LblDataBuf) << ", " << *(LblDataBuf+1) << ">, Address "  << MyMap->GetAddress(*LblDataBuf) << endl;;
    //        Add(LblDataBuf);
    //    }
    END;
}

MFMEbyedatFrame * Analysis::GetCalcFrame(void)
{
    START;
    MFMEbyedatFrame* CalcFrame=NULL;



    return CalcFrame;
    END;
}





void Analysis::BufferOut(char* OutBufPtr,
                         unsigned int OutBufSize,
                         unsigned int *used_size_of_output_buffer,
                         MFMEbyedatFrame * inFrame,
                         MFMEbyedatFrame * CalcFrame)
{
    START;
    MFMMergeFrame* MergedFrame = new MFMMergeFrame();

    try{
        // uint64_t timestamp = 0;
        // uint32_t eventnumber = 0;
        uint32_t unitBlock_size = 0;
        // uint32_t itemsize = 0;
        // uint32_t nbitem = 0;
        // uint32_t revision = 0;
        uint32_t framesize = 0;
        uint32_t headersize = 0;
        void* MemCpyVerif;
        char* CurrPtr;

        unitBlock_size = 2;
        // itemsize = 0;
        // nbitem = 2;
        // revision = 1;
        headersize = MERGE_TS_HEADERSIZE;
        framesize = MERGE_TS_HEADERSIZE;


#ifdef DEBUG_ACTOR
        cout << " in Frame Size  in BufferOut " << inFrame->GetFrameSize() << endl;
#endif
        framesize += inFrame->GetFrameSize();
        framesize += inFrame->GetFrameSize();

#ifdef DEBUG_ACTOR
        cout << " Buffer Header " <<  sizeof(*used_size_of_output_buffer) << endl;
        cout << " Total Frame Size " << framesize << " / " << *used_size_of_output_buffer<< " --  Header " <<  MERGE_TS_HEADERSIZE << endl;
#endif

        MergedFrame->MFM_make_header(unitBlock_size,
                                     0,
                                     MFM_MERGE_TS_FRAME_TYPE,
                                     -1,
                                     (int) (framesize/ unitBlock_size),
                                     (headersize / unitBlock_size),
                                     0,
                                     2);
        MergedFrame->SetAttributs();


#ifdef DEBUG_ACTOR
        MergedFrame->HeaderDisplay((char*)"-- Header of Merge Frame --");
#endif

        MergedFrame->SetEventNumber(inFrame->GetEventNumber());
        MergedFrame->SetTimeStamp(inFrame->GetTimeStamp());
        // Add Two Frames
        MergedFrame->AddFrame(inFrame);
        inFrame->SetEventNumber(2*inFrame->GetEventNumber());
        MergedFrame->AddFrame(inFrame);

#ifdef DEBUG_ACTOR

        cout << " timestamp = " << MergedFrame->GetTimeStamp() << "\n";
        cout << " event number = " << MergedFrame->GetEventNumber()  << " Not Implemented in this frame\n";
        // MergedFrame->DumpRaw(framesize,0);
#endif

        if( (*used_size_of_output_buffer + framesize) <  OutBufSize)
        {
            CurrPtr = OutBufPtr + (*used_size_of_output_buffer);
            //Copy to Buffer :
            MemCpyVerif = memcpy (CurrPtr, MergedFrame->GetPointHeader(), framesize );
            if(MemCpyVerif)
            {
                *used_size_of_output_buffer += framesize;
#ifdef DEBUG_ACTOR
                cout << "Used Size " << *used_size_of_output_buffer << "/" << OutBufSize << " --  FrameSize " << framesize << endl;
#endif
            }
            else
            {
                Char_t Message[100];
                sprintf(Message,"Error in Copy to BufferOut memory  ");
                MErr * Er = new MErr(WhoamI,1,200, Message);
                throw Er;
            }
        }
        else
        {
            Char_t Message[100];
            sprintf(Message,"Error in Copy to BufferOut memory :  Beyond size of Output Buffer :%u / %u ", OutBufSize,*used_size_of_output_buffer + framesize);
            MErr * Er = new MErr(WhoamI,1,200, Message);
            throw Er;
        }

        MergedFrame = CleanDelete(MergedFrame);
    }
    catch(MErr * Er)
    {
        MergedFrame = CleanDelete(MergedFrame);
        throw Er;
    }

    END;
}


void Analysis::TreatMFMBuffer(char* InBuf,
                              unsigned int size_of_input_buffer,
                              char* OutBuf,
                              unsigned int size_of_output_buffer,
                              unsigned int *used_size_of_output_buffer
                              )
{
    START;
    MFMCommonFrame *  frame = new MFMCommonFrame();
    MFMEbyedatFrame * ebyeframe = new MFMEbyedatFrame();

    try
    {
        int SizeReadInBuf = 0;
        int framesize = 0;
        char* currptr;
        long long type =0;
        int SizetoRead =0;

        // memcpy(&SizetoRead, InBuf, sizeof(SizetoRead));
        // SizeReadInBuf +=  sizeof(SizetoRead);
        SizetoRead = size_of_input_buffer;

#ifdef DEBUG_ACTOR
        cout << "Size of buffer TreatMFM " << SizetoRead << endl;
#endif

        //Prepare Output Buffer
        //      void* VerifMemCpy;
        //      char * OutBufStartPtr = OutBuf;
        *used_size_of_output_buffer = 0; //sizeof(*used_size_of_output_buffer);

        unsigned short ctr=0;;

        while(SizeReadInBuf< SizetoRead)
        {
            currptr = InBuf+SizeReadInBuf;

#ifdef DEBUG_ACTOR
            cout << " ==================================================================" << endl;
            cout << "     Event    " << endl;
            cout << " ==================================================================" << endl;
            cout << "  -> In Memory Frame " << ctr << " adr " << &currptr << endl;
#endif
            framesize = frame->ReadInMem(&currptr);
            if(framesize > 0)
            {
                SizeReadInBuf+= framesize;
                //type = frame->GetFrameTypeAttribut();
                type = frame->GetFrameType();

                if ((type == MFM_EBY_EN_FRAME_TYPE) ||
                        (type == MFM_EBY_TS_FRAME_TYPE) ||
                        (type == MFM_EBY_EN_TS_FRAME_TYPE))
                {

                    ebyeframe->SetAttributs(frame->GetPointHeader());
#ifdef DEBUG_ACTOR
                    ebyeframe->HeaderDisplay((char*)"-- Header of Mem Ebyedat event number Frame --");
                    cout << "TimeStamp = " << ebyeframe->GetTimeStamp() << "\n";
                    cout << "EventNumber = " << ebyeframe->GetEventNumber() << "\n";
#endif
                    // Treat Event at this level
                    Clear();
                    AddMFMData(ebyeframe);
                    Treat();
#ifdef DEBUG_ACTOR
                    // PrintRaw();
#endif
                    UpdateCounters();
                    GetCalcFrame();

                    BufferOut(OutBuf,
                              size_of_output_buffer,
                              used_size_of_output_buffer,
                              ebyeframe,
                              ebyeframe);
                    RunEvt++;

                    if(TotalEvt%10000==0)
                    {
                        DisplayRate();
                    }

                }
                else
                {
                    Char_t Message[100];
                    sprintf(Message," <libVActor> MFM Frame Type Not implemented : %lld ", type);
                    MErr * Er = new MErr(WhoamI,1,200, Message);
                    throw Er;

                }
            }
            else
            {
                Char_t Message[100];
                sprintf(Message," <libVActor> Error in Reading frame from memory : Zero size ");
                MErr * Er = new MErr(WhoamI,1,200, Message);
                throw Er;
            }
            ctr++;
        }

        // Write Output Buffer Header (Size to read)
        // VerifMemCpy = memcpy (OutBufStartPtr,
        //                 used_size_of_output_buffer,
        //                 sizeof(*used_size_of_output_buffer) );
        // if(!VerifMemCpy)
        //   cout << "Error in Copy to Used Buff size to memory " << endl;

        frame = CleanDelete(frame);
        ebyeframe = CleanDelete(ebyeframe);
    }
    catch(MErr * Er)
    {
        frame = CleanDelete(frame);
        ebyeframe = CleanDelete(ebyeframe);

        throw Er;
    }
    END;
}




#endif

void Analysis::DisplayRate(void)
{
    START;
    Double_t Time = 0.0;

    end = clock();
    // Frac = (double)GDF->GetMBytesRead()/((double)(FS));
    Time = ((double) (end-start)/CLOCKS_PER_SEC);
    // TimeLeft = Time*(1./Frac - 1.);
    // cout << "\r    [" << CurrentFile << "/" <<  IFN->size() << "] Events    : " << RunEvt
    cout << std::dec
         << "\r    Events    : " << RunEvt
         << " --- " << std::setprecision(2) << std::fixed << (double)RunEvt/Time
         << " RunEvt/sec"
            // << " --- Read : "<< std::setprecision(2) << std::fixed <<  Frac*100. << "%"
            // << " (" << (double)GDF->GetMBytesRead()/Time
            // << " Mb/sec) --- Time Left : "
            // << std::setprecision(2) << std::fixed << (int) (TimeLeft/60)
            // << " min "
            // <<  std::setprecision(2) << std::fixed <<  ((int)TimeLeft % 60) << " sec.
         << "   \r" << flush;

    END;
}

void Analysis::PrintRateSummary(void)
{
    START;
    end = clock();
    cout << "\n\n" << endl;
    cout << "=========================================================" <<endl;
    cout << "=                     Counters                          =" << endl;
    cout << "=========================================================" <<endl;
    cout << "  Total Events Treated     : " << TotalEvt << endl;
    cout << "  Events/sec               : " << (double)TotalEvt/((double)(end-start)/CLOCKS_PER_SEC) << endl;
    cout << "  Events/sec (noInit)      : " << (double)TotalEvt/((double)(end-start_run)/CLOCKS_PER_SEC) << endl;
    cout << "  Total Exec. Time (sec)   : "
         << (double)(end-start)/CLOCKS_PER_SEC
         << "\n";
    cout << "=========================================================" <<endl;

    END;
}

#ifdef WITH_MFM
void Analysis::UnPack(MFMCommonFrame *AFrame) {
    START;

    //    MFMMergeFrame MergedFrame;

//    cout << "  ****** Enterring Unpack with Ptr : " <<AFrame <<endl;
//    cout << " Frame Type: " << AFrame->GetFrameType() << endl;
    Int_t FrameType = AFrame->GetFrameType();
    UShort_t NItems = 0;
    Int_t Channel = -1;
    Int_t Board = -1;
    Int_t Sample = -1;
    UShort_t Energy = 0;
    Int_t Time = -1;
    Int_t PileUp = -1;
    Int_t DataValid = -1;
    ULong64_t TS = 0;
    uint16_t value, label;
    UShort_t LblData[2];
    int nbinsideframe = 0;
    UShort_t ExoSegStatus;
    Bool_t ExoSegCharge[4];

#ifdef WITH_FALSTAFF
    UShort_t it = 0;
    UShort_t it2 = 0;
#endif
    switch (FrameType) {
    case MFM_COBOT_FRAME_TYPE:
        break;
    case MFM_COBOF_FRAME_TYPE:
        break;
    case MFM_COBO_FRAME_TYPE:
        pCoboFrame->SetAttributs(AFrame->GetPointHeader());
        //cout<<"*********COBO FRAME************"<<endl;
#ifdef WITH_GET
        AddCoboData(pCoboFrame,true);
#endif
        break;
    case MFM_EBY_EN_FRAME_TYPE:
    case MFM_EBY_TS_FRAME_TYPE:
    case MFM_EBY_EN_TS_FRAME_TYPE:
#ifdef WITH_FALSTAFF
        it = 0;
        it2 = 0;
#endif
        ebyframe->SetAttributs(AFrame->GetPointHeader());
        NItems = ebyframe->GetNbItems();
        TS = ebyframe->GetTimeStamp();
        for (UShort_t i = 0; i < NItems; i++) {
            ebyframe->EbyedatGetParameters(i,&label, &value);
            //                cout << "<" << label << ", " << value << ">, " << endl;;
            LblData[0] = label ;
            LblData[1]  = value;

#ifdef WITH_FALSTAFF
#ifdef WITH_FST_DIGIT

            if((label) == 1) // First IC Readout Channel
            {
                LblData[0] =  FalstaffParOffSet + it;
                it++;
            }
            else if ((label) == 3 )
            {
                LblData[0] =  FalstaffParOffSet + it2 + it;
                it2++;
            }

            //            cout << "it" << it2 + it << " " << it << " " << it2 << endl;
#endif
#endif

            //                cout << "<" << (*(LblData))<< ", " << (*(LblData+1)) << ">, " << TS <<  endl;;

            Add(LblData,TS);
        }
        //        cout << "----" << endl;
        break;
    case MFM_VAMOSIC_FRAME_TYPE: // Deprecated after 2020
        pVamosICFrame->SetAttributs(AFrame->GetPointHeader());
        Channel = pVamosICFrame->GetTGCristalId();
        Board = pVamosICFrame->GetBoardId();
        Energy = pVamosICFrame->GetEnergy();
        DataValid =  pVamosICFrame->GetStatus(0);
        PileUp =  pVamosICFrame->GetStatus(1);
        TS  = pVamosICFrame->GetTimeStamp();
#ifdef DEBUG
        cout << "VAMOS IC " << endl;
        pVamosICFrame->HeaderDisplay();
        cout << "IC B : " << Board << " Chan " << Channel << "Energy " << Energy <<  " TS " << TS <<endl;
#endif
#ifdef WITH_NUMEXO
        AddNUMEXO(Board,Channel,-1,Energy,TS,DataValid,PileUp);
#endif
        break;
    case MFM_VAMOSPD_FRAME_TYPE:
        pVamosPDFrame->SetAttributs(AFrame->GetPointHeader());
        TS  = pVamosPDFrame->GetTimeStamp();
        Channel = pVamosPDFrame->GetTGCristalId(); // Not Relevant, As Smaple Tell you whant should be used
        Board = pVamosPDFrame->GetBoardId();
#ifdef DEBUG
        cout << "VAMOS PD " << endl;
        pVamosPDFrame->HeaderDisplay();
#endif
        for (int i = 0; i < VAMOSPD_NB_VALUE; i++) {
            pVamosPDFrame->GetParameters(i, &label, &value);
            if (label == 57072) // 0xDEF0 invalid sequence missing clocks
            {
                cout << " /!\\ ERROR in NUMEXO Demultiplexing "
                     << "\n Missing Clocks "
                     << endl;
                break;

            }
            else if (label != 0xffff) {
                // Recalculate Sample Number in Natural order 0 -> 96
                // pos  = (label - 1) / 16 + 1 + ((label - 1) % 16)*96 - 1;
                Channel = (label - 1) % 16;
                Sample = (label - 1) / 16;

#ifdef DEBUG
                cout << "IC B : " << Board << " label " << label << " Chan " << Channel << " Sample " << Sample <<endl;
                cout << i << "/" << VAMOSPD_NB_VALUE << " : " << Sample << " => " << value << endl;
#endif
#ifdef WITH_NUMEXO
                AddNUMEXO(Board,Channel,Sample,value,TS);
#endif
            }
        }
        break;
    case MFM_REA_GENE_FRAME_TYPE:
        pREAFrame->SetAttributs(AFrame->GetPointHeader());
        Channel = pREAFrame->GetTGCristalId();
        Board = pREAFrame->GetBoardId();
        Energy = pREAFrame->GetEnergy();
        Time = pREAFrame->GetTime();
        DataValid =  pREAFrame->GetStatus(0);
        PileUp =  pREAFrame->GetStatus(1);
        TS  = pREAFrame->GetTimeStamp();
#ifdef DEBUG
        cout << "------------- VAMOS REA " << endl;
        pREAFrame->HeaderDisplay();
        cout << "\nIC B : " << Board << " Chan " << Channel << " Energy " << Energy << " Time " << Time << " DataValid : " <<  DataValid  << " Pileup " <<  PileUp <<  " --> TS " << TS <<endl;
#endif

#ifdef WITH_NUMEXO
        if(pREAFrame->GetTypeTns() == REA_GENERIC_TIME_TYPE) // Tacs
            AddNUMEXO(Board,Channel,-1,Time,TS,DataValid,PileUp,Time); // Time in Energy !
        else
            AddNUMEXO(Board,Channel,-1,Energy,TS,DataValid,PileUp,Time);
#endif
        break;
#ifdef WITH_PARIS
    case MFM_PARIS_FRAME_TYPE:
        //SetIsCtrl(true);
        pParisFrame->SetAttributs(AFrame->GetPointHeader());
        TS  = pParisFrame->GetTimeStamp();
        if(ParisDet)
            AddParisData(pParisFrame);

        break;
#endif
    case MFM_MERGE_EN_FRAME_TYPE:
    case MFM_MERGE_TS_FRAME_TYPE:
        UnpackLevel++;
        nbinsideframe = 0;
//                cout << "  <----  Start Unpack Level  ***** "  << UnpackLevel << endl;


//        AFrame->HeaderDisplay();
        //        (&MergedFrame)->SetAttributs(AFrame->GetPointHeader());
        //        nbinsideframe =(&MergedFrame)->GetNbItems();
        //            (&MergedFrame)->HeaderDisplay();

        //        (&MergedFrame)->ResetReadInMem();
        //        for (int i = 0; i < nbinsideframe; i++) {
        //            (&MergedFrame)->ReadInFrame(pInsideFrame);
        //            (&MergedFrame)->HeaderDisplay((char*)"-- Header of Inside Frame --");
        //            UnPack(pInsideFrame);
        //        }

        pMergeFrame->SetAttributs(AFrame->GetPointHeader());
        nbinsideframe = pMergeFrame->GetNbItems();
#ifdef DEBUG
        cout << "\033[31m-- MFM_MERGE_XX_FRAME_TYPE with Nb of inside frames = "
             << nbinsideframe << "\033[0m  Ptr : "<< pMergeFrame << "\n";
#endif


        pMergeFrame->ResetReadInMem();
        for (int i = 0; i < nbinsideframe; i++) {
            pMergeFrame->ReadInFrame(pInsideFrame);
            //            pInsideFrame->HeaderDisplay((char*)"-- Header of Inside Frame --");
            UnPack(pInsideFrame);
        }

        //       cout << "  ----> Exit Unpack Level  ***** "  << UnpackLevel << endl;
        UnpackLevel--;


        break;
    case MFM_EXO2_FRAME_TYPE:
        // cout << "\Exogam2 Frame Not Controlled" << endl;
        pExogamFrame->SetAttributs(AFrame->GetPointHeader());
        Channel = (pExogamFrame->ExoGetTGCristalId())/8;
        Board = pExogamFrame->ExoGetBoardId();
        TS  = pExogamFrame->GetTimeStamp();
        ExoSegStatus = pExogamFrame->ExoGetStatus(2);

        for(Int_t seg=0;seg<4;seg++){
                ExoSegCharge[seg]= !(((ExoSegStatus>>(8*Channel))>>seg+2) &1);

        }
        //        pExogamFrame->HeaderDisplay();
#ifdef DEBUG
        cout << "=== Exogam Frame ===" << endl;
        cout << "Board Id : " << Board << endl;
        cout << "Channel : " << Channel  << endl;
        cout << "Inner 6M : " << pExogamFrame->ExoGetInnerM(0) << endl;
        cout << "Inner 20M: " << pExogamFrame->ExoGetInnerM(1) << endl;
        cout << "DeltaT: " << pExogamFrame->ExoGetDeltaT() << endl;
        cout << "Status3: " << ExoSegStatus << " " << std::bitset<16>(ExoSegStatus) << endl;
        cout << "Outers: " << endl;
        for(Int_t i=0; i<4; i++)
        {
            cout << "  Outers_" << i << " : " << pExogamFrame->ExoGetOuter(i) <<  " Charge  : " <<  ExoSegCharge[i] << endl;
        }
#endif
        // Inner6MeV
        AddNUMEXO(Board,Channel,0,pExogamFrame->ExoGetInnerM(0),TS);
        // Inner20MeV
        AddNUMEXO(Board,Channel,1,pExogamFrame->ExoGetInnerM(1),TS);
        // DeltaT
        AddNUMEXO(Board,Channel,2,pExogamFrame->ExoGetDeltaT(),TS);

        // Outers 1-3 // Only Fill NetCharges not mirror
        for(Int_t i=0; i<4; i++)
        {
            if(pExogamFrame->ExoGetOuter(i)>0 && ExoSegCharge[i]==1)
                AddNUMEXO(Board,Channel,3+i,pExogamFrame->ExoGetOuter(i),TS);
        }
        // BGO
        AddNUMEXO(Board,Channel,7,pExogamFrame->ExoGetBGO(),TS);
        // CsI
        AddNUMEXO(Board,Channel,8,pExogamFrame->ExoGetCsi(),TS);


        break;
    case MFM_MESYTEC_FRAME_TYPE:
//        cout << "\n Mesytec Frame Type in " << __PRETTY_FUNCTION__ << endl;
//        cout << pMesytecFrame << endl;
//        cout << AFrame << endl;
//        cout << AFrame->GetPointHeader() << endl;
        pMesytecFrame->SetAttributs(AFrame->GetPointHeader());
        TS = pMesytecFrame->GetTimeStamp();
#ifdef WITH_PISTA
       if(PistaDet)
          PistaDet->SetTS(TS);
//       cout << "TS " << TS << " "<< PistaDet->GetTS() <<  endl;
#endif


       try{

#ifdef WITH_MESYTEC_DATA
//        AFrame->HeaderDisplay();

//        cout << "Blob Size " << pMesytecFrame->GetFrameSize() - (int)((unsigned char*)pMesytecFrame->GetPointUserData() - (unsigned char*)pMesytecFrame->GetPointHeader()) <<endl;
//        cout << "EvtSize " << pMesytecFrame->GetEventSize() <<endl;//        fMesytecReader.read_event_in_buffer((const uint8_t*) pMesytecFrame->GetPointUserData(),
//                                            pMesytecFrame->GetFrameSize() - (int)((unsigned char*)pMesytecFrame->GetPointUserData() - (unsigned char*)pMesytecFrame->GetPointHeader()),
//                                            [=,this](mesytec::event& evt, mesytec::experimental_setup& config)
        fMesytecReader.read_event_in_buffer((const uint8_t*) pMesytecFrame->GetPointUserData(),
                                            pMesytecFrame->GetEventSize(),
                                            [=,this](mesytec::event& evt, mesytec::experimental_setup& config)
        {
#ifdef DEBUG
            evt.ls(config);
#endif
            UShort_t Time = 0;
            for (auto& mod_dat : evt.get_module_data())
            {
                auto mod_id = mod_dat.get_module_id();
                if(config.has_module(mod_dat.get_module_id()))               // => sanity check: does data come from a known module?
                {

                    auto& mod = config.get_module(mod_dat.get_module_id());   // => find module (mesytec::module) in crate config


                    for(auto& chan_dat : mod_dat.get_channel_data())          // Treat data from module item by item (mesytec::channel_data)
                    {
                        auto b = (int)chan_dat.get_bus_number();         // => for VMMR: bus number (for MDPP=0)
                        auto c = (int)chan_dat.get_channel_number();     // => for VMMR: bus subaddress (0-127), for MDPP: channel number
                        auto e = (int)chan_dat.get_data();               // => 16 bit data
                        mod.set_data_word(chan_dat.get_data_word());
                        auto t = mod.get_data_type();         // => data type (mesytec_module::datatype_t)
                        if( t == mesytec::module::ADC )
                        {

//                           std::cout << "Energy Event ? " << TotalEvt << std::endl;
//                           std::cout << "Mod Id = "<< std::hex << std::showbase << (int)mod_id << std::endl;
//                           std::cout << "BUS  = " << std::dec << b << std::endl;
//                           std::cout << "CHAN = " << std::dec << c << std::endl;
//                           std::cout << "data = " << std::dec << e << std::endl;
//                           std::cout << "type = " << std::dec << t << std::endl;


                            if( mod.is_mdpp_module() ) {
                                /* data from MDPP */
//                                cout << "--------------"<<endl;
//                                cout << "MDDP Fill" << (int) chan_dat.get_channel_number() << " " <<  chan_dat.get_data() << " " << Time << endl;

                               this->AddMesytecData((int)mod_id ,chan_dat.get_channel_number(), -1, chan_dat.get_data(),TS,Time);

                            }
                            if( mod.is_vmmr_module() ) {
                                /* data from VMMR */

//                                std::cout << "MMR Event ?" << std::endl;

                               this->AddMesytecData((int)mod_id ,chan_dat.get_bus_number(),chan_dat.get_channel_number(), chan_dat.get_data(),TS);
                                }
                        }
                        else if( t == mesytec::module::TDC )
                        { /* channel time (relative to window of interest) :
                                     N.B. for VMMR, only bus number is defined in this case (c = 0) */
                           /* N.B. chan_dat.get_data_type_name() returns "adc", "tdc", "QDC_long", etc. */

                           if( mod.is_vmmr_module() ) {
//                              std::cout << "VMMR Time Event ?" << std::endl;
                               //                              std::cout << "Mod Id = "<< std::hex << std::showbase << (int)mod_id << std::endl;
                               //                              std::cout << "BUS  = " << std::dec << b << std::endl;
                               //                              std::cout << "CHAN = " << std::dec << c << std::endl;
                               //                              std::cout << "data = " << std::dec << e << std::endl;
                               //                              std::cout << "type = " << std::dec << t << std::endl;
                              this->AddMesytecData((int)mod_id ,chan_dat.get_bus_number(),128, chan_dat.get_data(),TS);
                           }
                           if( mod.is_mdpp_module() ) {
                               Time = chan_dat.get_data();
                           }
                           if(mod.is_mtdc_module())
                           {
//                               std::cout << "MTDC Time Event ?" << std::endl;
//                               std::cout << "Mod Id = "<< std::hex << std::showbase << (int)mod_id << std::endl;
//                               std::cout << "BUS  = " << std::dec << b << std::endl;
//                               std::cout << "CHAN = " << std::dec << c << std::endl;
//                               std::cout << "data = " << std::dec << e << std::endl;
//                               std::cout << "type = " << std::dec << t << std::endl;
                               this->AddMesytecData((int)mod_id ,chan_dat.get_channel_number(), -1, chan_dat.get_data(),TS);

                           }
                        }
                    }


                }
           }
        }
        ,pMesytecFrame->GetRevision());
#endif
    }
       catch(const std::exception &e)
    {

        cout << e.what() <<endl;
    }

        break;
    case MFM_XML_DATA_DESCRIPTION_FRAME_TYPE:
        cout << "\nHeader Data Descr. Frame " << endl;
        AFrame->HeaderDisplay();
        break;
    case MFM_XML_FILE_HEADER_FRAME_TYPE:
        cout << "\nHeader Frame " << endl;
        AFrame->HeaderDisplay();
        break;
    case MFM_HELLO_FRAME_TYPE:
        break;
    default:
        cout << "\nUnknown Frame " << std::hex << FrameType << std::dec <<  endl;

        AFrame->HeaderDisplay();
        break;
    }


    END;
}
#endif
