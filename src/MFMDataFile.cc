/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "MFMDataFile.hh"
#include "SimpleAnalysis.hh"


MFMDataFile::MFMDataFile(const string filename)
    : DataFile(filename)
{
    START;
    InitDefault();
    Open();
    END;
}

MFMDataFile::~MFMDataFile()
{
    START;
    Close();
    LabelData = CleanDeleteA(LabelData);

#ifdef WITH_MFM
    frame = CleanDelete(frame);
    ebyframe = CleanDelete(ebyframe);
    pInsideFrame = CleanDelete(pInsideFrame);
    pVamosICFrame = CleanDelete(pVamosICFrame);
    pVamosPDFrame = CleanDelete(pVamosPDFrame);
    pMergeFrame = CleanDelete(pMergeFrame);
    pREAFrame = CleanDelete(pREAFrame);

#ifdef WITH_BLM
    pDeflectorFrame = CleanDelete(pDeflectorFrame);
    pBAF2Frame = CleanDelete(pBAF2Frame) ;
    pSynchroFrame = CleanDelete(pSynchroFrame);
#endif
#ifdef WITH_PARIS
    pParisFrame = CleanDelete(pParisFrame);;
#endif
#endif
    free(vector);
    END;
}

void MFMDataFile::InitDefault()
{
    START;
    CurrentTS = 0;
    PreviousTS = 0;
    FirstTS = 0;

    nbofevent = 0;
    framesize = 0;
    pvector = &vector;
    vector = (char*) (malloc(vectorsize));
    LabelData = new UShort_t[2];

#ifdef WITH_MFM
    minsizeheader = MFM_BLOB_HEADER_SIZE; // =8
    vectorsize = minsizeheader;
    frame = NULL;
    frame = new MFMCommonFrame();
    ebyframe = NULL;
    ebyframe= new MFMEbyedatFrame();
    pInsideFrame = NULL;
    pInsideFrame = new MFMCommonFrame();

    pVamosICFrame = new  MFMVamosICFrame();
    pVamosPDFrame = new  MFMVamosPDFrame();
    pMergeFrame = new  MFMMergeFrame();
    pREAFrame = new MFMReaGenericFrame();

#ifdef WITH_BLM
     pDeflectorFrame = new MFMS3DeflectorFrame();
     pBAF2Frame = new MFMS3BaF2Frame() ;
     pSynchroFrame = new MFMS3SynchroFrame();
#endif
#ifdef WITH_PARIS
    pParisFrame = new MFMParisFrame();
#endif
#endif
    fEventCount = 0;
    fEventSkippedCount = 0;
#ifdef TRIG_PAT
    Pattern = 0;
    fBadMerge = 0;
#endif

    END;
}


Bool_t MFMDataFile::Open()
{
    START;

    // Open File

    fLun = open(GetFileName(), (O_RDONLY));
    if (fLun <= 0)
    {
        Char_t Message[100];
        sprintf(Message,"Could not open Input File : %s ", GetFileName());
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    return true;

    END;
}


Bool_t MFMDataFile::Close()
{
    START;

    // Close File

    cout << endl <<
            "From GanilDataFile: "<<
            "Events Read :" << fEventCount << " -- Events Skipped :" << fEventSkippedCount
        #ifdef TRIG_PAT
         << " -- BadMerge :" << fBadMerge
        #endif
         << endl;




    if(close(fLun) != 0)
    {

        Char_t Message[100];
       cout << Message<<endl;
               MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    return 1;

    END;
}


UShort_t* MFMDataFile::GetDataAt(UShort_t Pos) 
{
    START;
    // Length is number of items compared to size
#ifdef WITH_MFM
    ebyframe->EbyedatGetParameters(Pos, LabelData, LabelData+1);
#endif
    return LabelData;
    END;
}


void MFMDataFile::GetParameters(Parameters *Par)
{
    START;
    struct stat FileStat;
    char* HomeDir = getenv("HOME");
    ConfManager *CM = ConfManager::getInstance();


    cout << "Home " << HomeDir << endl;
    char actionFilePAR[500];
    char actionFilePAR2[100];
    strcpy(actionFilePAR, "");
    sprintf(actionFilePAR, "ACTIONS_%s.CHC_PAR", CM->Get("ExpName").c_str());
    strcpy(actionFilePAR2, actionFilePAR);

    // Check Local the existence of Action Files in the following order
    // 1) in the current directory
    // 2) in the GECO directory
    // 3) in Das directory

    cout << actionFilePAR << endl;

    if (stat(actionFilePAR, &FileStat) < 0) {

        strcpy(actionFilePAR, "");
        sprintf(actionFilePAR, "%s/ganacq_manip/%s/GECO/%s/ACQ/%s",
                HomeDir, CM->Get("ExpName").c_str(), CM->Get("ExpName").c_str(),  actionFilePAR2);
        cout << actionFilePAR << endl;

        if (stat(actionFilePAR, &FileStat) < 0) {

            strcpy(actionFilePAR, "");
            sprintf(actionFilePAR, "%s/ganacq_manip/%s/das-save/%s",
                    HomeDir, CM->Get("ExpName").c_str(),  actionFilePAR2);

            cout << actionFilePAR << endl;
            if (stat(actionFilePAR, &FileStat) < 0)
            {

                Char_t Message[100];
                sprintf(Message,"Could not Find ACTION File : ");
                MErr * Er = new MErr(WhoamI,0,0, Message);
                throw Er;
            }
        }
    }

    cout << " Found Parameter file : " << actionFilePAR <<endl;

    GetParametersFromActionFile(Par,actionFilePAR);

    END;
}

//void MFMDataFile::UnPack(Analysis* An, MFMCommonFrame *AFrame) {
//    START;

//    SetIsCtrl(false);
//    // Int_t FrameType = AFrame->GetFrameTypeAttribut();
//    Int_t FrameType = AFrame->GetFrameType();

//    //  AFrame->HeaderDisplay();
//    // cout << "Frame Type " << FrameType << endl;
//    UShort_t NItems = 0;
//    Int_t Channel = -1;
//    Int_t Board = -1;
//    Int_t Sample = -1;
//    UShort_t Energy = 0;
//    Int_t Time = -1;
//    Int_t PileUp = -1;
//    Int_t DataValid = -1;
//    ULong64_t TS = 0;
//    uint16_t value, label;
//    int nbinsideframe = 0;
//    switch (FrameType) {
//    case MFM_EBY_EN_FRAME_TYPE:
//    case MFM_EBY_TS_FRAME_TYPE:
//    case MFM_EBY_EN_TS_FRAME_TYPE:
//        SetIsCtrl(true);
//        ebyframe->SetAttributs(AFrame->GetPointHeader());
//        NItems = ebyframe->GetNbItems();
//        for (UShort_t i = 0; i < NItems; i++) {
//            UShort_t *LblDataBuf;
//            LblDataBuf = GetDataAt(i);
//            //     cout << "<" << *(LblDataBuf) << ", " << *(LblDataBuf+1) << ">, " << endl;;
//            An->Add(GetDataAt(i));
//        }
//        break;
//    case MFM_VAMOSIC_FRAME_TYPE:
//        SetIsCtrl(true);
//        pVamosICFrame->SetAttributs(AFrame->GetPointHeader());
//        Channel = pVamosICFrame->GetTGCristalId();
//        Board = pVamosICFrame->GetBoardId();
//        Energy = pVamosICFrame->GetEnergy();
//        DataValid =  pVamosICFrame->GetStatus(0);
//        PileUp =  pVamosICFrame->GetStatus(1);
//        TS  = pVamosICFrame->GetTimeStamp();
//#ifdef DEBUG
//        if(  Board == 157)
//        {
//            cout << "VAMOS IC " << endl;
//            pVamosICFrame->HeaderDisplay();
//            cout << "\nIC B : " << Board << " Chan " << Channel << " Energy " << Energy << " DataValid : " <<  DataValid  << " Pileup " <<  PileUp <<  " --> TS " << TS <<endl;
//        }
//#endif

//#ifdef WITH_NUMEXO
//        An->AddNUMEXO(Board,Channel,-1,Energy,TS,DataValid,PileUp);
//#endif

//	break;
//       case MFM_REA_GENE_FRAME_TYPE:
//            SetIsCtrl(true);
//           pREAFrame->SetAttributs(AFrame->GetPointHeader());
//           Channel = pREAFrame->GetTGCristalId();
//           Board = pREAFrame->GetBoardId();
//           Energy = pREAFrame->GetEnergy();
//           Time = pREAFrame->GetTime();
//           DataValid =  pREAFrame->GetStatus(0);
//           PileUp =  pREAFrame->GetStatus(1);
//           TS  = pREAFrame->GetTimeStamp();
//#ifdef DEBUG
//           cout << "------------- VAMOS REA " << endl;
//           pREAFrame->HeaderDisplay();
//           cout << "\nIC B : " << Board << " Chan " << Channel << " Energy " << Energy << " Time " << Time << " DataValid : " <<  DataValid  << " Pileup " <<  PileUp <<  " --> TS " << TS <<endl;
//#endif

//#ifdef WITH_NUMEXO
//           if(Board == 159) // Tacs
//               An->AddNUMEXO(Board,Channel,-1,Time,TS,DataValid,PileUp,Time); // Time in Energy !
//           else
//               An->AddNUMEXO(Board,Channel,-1,Energy,TS,DataValid,PileUp,Time);
//#endif

//        break;

//    case MFM_VAMOSPD_FRAME_TYPE:
//        SetIsCtrl(true);
//        pVamosPDFrame->SetAttributs(AFrame->GetPointHeader());
//        TS  = pVamosPDFrame->GetTimeStamp();
//        Channel = pVamosPDFrame->GetTGCristalId(); // Not Relevant, As Smaple Tell you whant should be used
//        Board = pVamosPDFrame->GetBoardId();
//#ifdef DEBUG
//        cout << "VAMOS PD " << endl;
//        pVamosPDFrame->ay();
//#endif
//        for (int i = 0; i < VAMOSPD_NB_VALUE; i++) {
//            pVamosPDFrame->GetParameters(i, &label, &value);
//            if (label == 57072) // 0xDEF0 invalid sequence missing clocks
//            {
//                cout << " /!\ ERROR in NUMEXO Demultiplexing "
//                     << "\n Missing Clocks "
//                     << endl;
//                break;

//            }
//            else if (label != 0xffff) {
//                // Recalculate Sample Number in Natural order 0 -> 96
//                // pos  = (label - 1) / 16 + 1 + ((label - 1) % 16)*96 - 1;
//                Channel = (label - 1) % 16;
//                Sample = (label - 1) / 16;

//#ifdef DEBUG
//                cout << "IC B : " << Board << " label " << label << " Chan " << Channel << " Sample " << Sample <<endl;
//                cout << i << "/" << VAMOSPD_NB_VALUE << " : " << Sample << " => " << value << endl;
//#endif
//#ifdef WITH_NUMEXO
//                An->AddNUMEXO(Board,Channel,Sample,value,TS);
//#endif
//            }
//            // else
//            //   {

//            //     cout << "VAMOS PD Frame " << Board << " label " << label << " " << 0xffff <<endl;

//            //   }
//        }
//        break;
//#ifdef WITH_PARIS
//    case MFM_PARIS_FRAME_TYPE:
//        cout << "\PARIS Frame" << endl;
//        SetIsCtrl(true);
//        pParisFrame->SetAttributs(AFrame->GetPointHeader());
//        TS  = pParisFrame->GetTimeStamp();
//#ifdef DEBUG
//        cout << "------------- Paris Frame " << endl;
//        cout << "\n Location Id " << pParisFrame->GetCristalId()
//             << " QShort : " << pParisFrame->GetQShort()
//             << " QLong  : " << pParisFrame->GetQLong()
//             << " CFD  : " << pParisFrame->GetCfd()
//             << " --> TS " << TS <<endl;
//        pParisFrame->HeaderDisplay();
//#endif
//        //An->AddParisData(pParisFrame);

//        break;
//#endif

//    case MFM_NEDA_FRAME_TYPE:
////#ifdef DEBUG
//        cout << "MFM_NEDA_FRAME_TYPE Frame Not Controlled" << endl;
//        AFrame->HeaderDisplay();
////#endif
//        break;

//    case MFM_DIAMANT_FRAME_TYPE:
////#ifdef DEBUG
//        cout << "MFM_DIAMANT_FRAME_TYPE Frame Not Controlled" << endl;
//        AFrame->HeaderDisplay();
////#endif
//        break;

//    case MFM_EXO2_FRAME_TYPE:
//#ifdef DEBUG
//        cout << "\Exogam2 Frame " << endl;
//        AFrame->HeaderDisplay();
//#endif
//        SetIsCtrl(true);

//        break;
//    case MFM_MERGE_EN_FRAME_TYPE:
//    case MFM_MERGE_TS_FRAME_TYPE:
//        SetIsCtrl(true);
//        nbinsideframe = 0;
//        pMergeFrame->SetAttributs(AFrame->GetPointHeader());
//        //nbinsideframe = pMergeFrame->GetNbItemsAttribut();
//        nbinsideframe = pMergeFrame->GetNbItems();
//#ifdef DEBUG
//        cout << "-- MFM_MERGE_XX_FRAME_TYPE with Nb of inside frames = "
//             << nbinsideframe << "\n";
//#endif

//        pMergeFrame->ResetReadInMem();
//        for (int i = 0; i < nbinsideframe; i++) {
//            pMergeFrame->ReadInFrame(pInsideFrame);
//            //                pInsideFrame->HeaderDisplay((char*)"-- Header of Inside Frame --");
//            UnPack(An, pInsideFrame);
//        }

//        break;
//#ifdef WITH_BLM

//    case MFM_S3_DEFLECTOR_FRAME_TYPE :
//         SetIsCtrl(true);
//        pDeflectorFrame->SetAttributs(AFrame->GetPointHeader());
//        TS  = pDeflectorFrame->GetTimeStamp();
//        Channel = pDeflectorFrame->GetTGCristalId();
//        Board = pDeflectorFrame->GetBoardId();
//#ifdef DEBUG
//        cout << "------------- S3 Deflector Frame " << endl;
//        cout << "\n Board " << Board << " Channel " << Channel << " " << "--> TS " << TS <<endl;
//        pDeflectorFrame->HeaderDisplay();
//#endif

//#ifdef WITH_NUMEXO
//        An->AddNUMEXO(Board,Channel,-1,Energy,TS,0,0);
//#endif


//        break;
//    case MFM_S3_SYNC_FRAME_TYPE :
//        SetIsCtrl(true);
//        pSynchroFrame->SetAttributs(AFrame->GetPointHeader());
//        TS  = pSynchroFrame->GetTimeStamp();
//        Channel = pSynchroFrame->GetTGCristalId();
//        Board = pSynchroFrame->GetBoardId();
////        cout << "\n\nSynchro \n" << endl;
////                DBLM->NewCycle(TS);
//#ifdef DEBUG
//        cout << "------------- S3 Synchro  " << endl;
//        cout << "\n Board " << Board << " Channel " << Channel << " --> TS " << TS <<endl;
//       pSynchroFrame->HeaderDisplay();
//       std::cout << std::dec;
//#endif

//#ifdef WITH_NUMEXO
//        An->AddNUMEXO(Board,Channel,-1,Energy,TS,0,0);
//#endif
//        break;

//    case MFM_S3_BAF2_FRAME_TYPE :
//        SetIsCtrl(true);
//        pBAF2Frame->SetAttributs(AFrame->GetPointHeader());
//        TS  = pBAF2Frame->GetTimeStamp();
//        Energy = pBAF2Frame->GetEnergy();
//        Channel = pBAF2Frame->GetTGCristalId();
//        Board = pBAF2Frame->GetBoardId();

//#ifdef DEBUG
//        cout << "------------- S3BAF2 REA " << endl;
//        cout << "\n Board " << Board << " Channel " << Channel << " Q : " << Energy << " --> TS " << TS <<endl;
//        pBAF2Frame->HeaderDisplay();
//#endif

//#ifdef WITH_NUMEXO
//        An->AddNUMEXO(Board,Channel,-1,Energy,TS,0,0);
//#endif
//        break;
//#endif
//    case MFM_MESYTEC_FRAME_TYPE:
//        cout << "\n Mesytec Frame " << endl;

//        break;
//    case MFM_XML_DATA_DESCRIPTION_FRAME_TYPE:
//        cout << "\nHeader Data Descr. Frame " << endl;
//        AFrame->HeaderDisplay();
//        break;
//    case MFM_XML_FILE_HEADER_FRAME_TYPE:
//        cout << "\nHeader Frame " << endl;
//        AFrame->HeaderDisplay();
//        break;
//    case MFM_HELLO_FRAME_TYPE:
//        //    cout << "\nHello Frame" << endl;
//        //    AFrame->HeaderDisplay();
//        //    cout << "Frame Type " << FrameType << endl;
//        break;
//    default:
//        cout << "\nUnknown Frame" << endl;
//        AFrame->HeaderDisplay();
//        cout << "Frame Type " << FrameType << endl;
//        break;
//    }

//    END;
//}




Bool_t MFMDataFile::Next()
{
    START;


#ifdef D_MUST2
    const short NTrigBranch = 4;
    Char_t TrigName[NTrigBranch][20] = {"MUST","BTD","TGV","VAMOS"};
    UShort_t fTrigLbl[NTrigBranch] = {1,5000,6000,7000};
#endif
#ifdef D_E641S
    const short NTrigBranch = 4;
    Char_t TrigName[NTrigBranch][20] = {"VXI_1","VXI_2","TGV","VAMOS"};
    UShort_t fTrigLbl[NTrigBranch] = {1,1000,3000,2000};
#endif

#ifdef D_AGATA
#ifdef D_E710
    const short NTrigBranch = 5;
    Char_t TrigName[NTrigBranch][20] = {"VAMOS","DC1","AGAVA","DC2","SPIDER"};
    UShort_t fTrigLbl[NTrigBranch] = {1,1000,2000,3000,5004};
#else
#ifdef D_E753
    const short NTrigBranch = 6;
    Char_t TrigName[NTrigBranch][20] = {"VAMOS","DC1","AGAVA","DC2","MW","SPIDER"};
    UShort_t fTrigLbl[NTrigBranch] = {1,1000,2000,3000,4000,6000};
#else

    const short NTrigBranch = 5;
    Char_t TrigName[NTrigBranch][20] = {"VAMOS","DC1","AGAVA","DC2","MW"};
    UShort_t fTrigLbl[NTrigBranch] = {1,1000,2000,3000,4000};
#endif

#endif
#else
    // No Agava
    const short NTrigBranch = 4;
    Char_t TrigName[NTrigBranch][20] = {"VAMOS","DC1","DC2","MW"};
    UShort_t fTrigLbl[NTrigBranch] = {1,1000,3000,4000};
#endif

#ifdef TRIG_PAT

    Int_t PatMask =0;
    Pattern = 0;
    for(UShort_t k=0; k< NTrigBranch;k++)
        PatMask |= (1<<k) ;

#endif


    SetIsCtrl(false);
    framesize = frame->ReadInFile(&fLun, pvector, &vectorsize);
    BytesRead += framesize;
    if (framesize <= 0)
        return 0;
    nbofevent++;
    //  int type = frame->GetFrameTypeAttribut();
    int type = frame->GetFrameType();



#ifdef DEBUG
    cout << "<MFMDataFile><Next>" << endl;
    cout << "Frame type :" << type  <<  " " << MFM_EXO2_FRAME_TYPE<<endl;
    frame->HeaderDisplay();
#endif
    
    if (type == MFM_COBO_FRAME_TYPE) {
        cout << "<MFMDataFile><Next> MFM COBO Frame" << endl;
        cout << "   --> Not Controlled Frame !" << endl;
        SetIsCtrl(false);
    }

#ifdef WITH_PARIS
    if (type == MFM_PARIS_FRAME_TYPE) {
        cout << "<MFMDataFile><Next> MFM PARIS Frame" << endl;
        cout << "   -->  Controlled Frame !" << endl;
        SetIsCtrl(true);
    }
#endif

    // if (type == MFM_COBOF_FRAME_TYPE) {
    //   cout << "<MFMDataFile><Next> MFM COBO Full Frame" << endl;
    //   cout << "   --> Not Controlled Frame !" << endl;
    //   SetIsCtrl(false);
    //   // MFMCoboFrame* pCoboFrame;
    //   // pCoboFrame = new MFMCoboFrame();
    //   // pCoboFrame->SetAttributs(frame->GetPointHeader());
    //   // coboidx=pCoboFrame->CoboGetCoboIdx();
    //   // asadidx=pCoboFrame->CoboGetAsaIdx();
    //   // int nbitems=pCoboFrame->GetNbItems();
    //   // Event=pCoboFrame->GetEventNumber();
    //   // ASADHIT->Fill(asadidx);
    //   // short iChan=0;
    //   // short iBuck=0;
    //   // short iAget=0;
    //   // pCoboFrame->HeaderDisplay((char*)"-- Header of Cobo Frame --");
    //   // cout << "----- >Evt Num " << pCoboFrame->GetEventNumber() << endl;
    // }

    if (type == MFM_EXO2_FRAME_TYPE) {
//        cout << "<MFMDataFile><Next> MFM ExoGam2 Frame" << endl;
//        cout << "   --> Not Controlled Frame !" << endl;
        SetIsCtrl(true);
    }

    // EbyE Ganil data format
    if ((type == MFM_EBY_EN_FRAME_TYPE) ||
            (type == MFM_EBY_TS_FRAME_TYPE) ||
            (type == MFM_EBY_EN_TS_FRAME_TYPE))
    {
        SetIsCtrl(true);

        ebyframe->SetAttributs(frame->GetPointHeader());
        
        uint16_t label, value;

        SetEventNumber(ebyframe->GetEventNumber());
        //SetNItems(ebyframe->GetNbItemsAttribut());
        //SetLength(ebyframe->GetNbItemsAttribut());
        SetNItems(ebyframe->GetNbItems());
        SetLength(ebyframe->GetNbItems());

        CurrentTS = ebyframe->GetTimeStamp();
#ifdef DEBUG
        cout << " ------------------------------------------ " << endl;
        cout << " EventNum " << ebyframe->GetEventNumber() << endl;
        cout << " Time Stamp " << CurrentTS << endl;
        cout << " Time Stamp since start " << (CurrentTS-FirstTS)*1e-5 << " msecs "  << endl;
        cout << " DeltaTS " << CurrentTS-PreviousTS << endl;
#endif

        if(PreviousTS == 0)
        {
            FirstTS = CurrentTS;
            // cout << "First TS" << endl;
        }

        PreviousTS = CurrentTS;


#ifdef TRIG_PAT
        for(Int_t k=0;k<GetNItems();k++)
        {
            ebyframe->EbyedatGetParameters(k, &label, &value);
            for(UShort_t i = 0; i<NTrigBranch ; i++)
            {
                if(label == fTrigLbl[i] && value > 0)
                {
                    Pattern |= (1<<i) ;
                }
            }
        }
#endif

#ifdef DEBUG
        for (int i = 0; i < GetNItems(); i++) {
            ebyframe->EbyedatGetParameters(i, &label, &value);
            cout << " Couple of label/value (" << label << "/" << value
                 << ") in hexa (" << hex << label << "/" << value
                 << dec << ")\n";
        }
#endif


#ifdef TRIG_PAT

        if(Pattern != PatMask)
        {
            // cout << "\033[1;31m /!\\ Error : \033[0m  ! Error in Merging - Evt number : " << GetEventNumber() << " " <<  GetNItems() << endl;
            // for(UShort_t i = 0; i<NTrigBranch ; i++)
            //   cout << TrigName[i] << " : " << ((Pattern>>i)  & 1) << " - " ;

            // cout << GetTrigPat() << " expecting " <<  PatMask << endl;
            fBadMerge++;
        }

#endif

    }

    if ((type == MFM_MERGE_EN_FRAME_TYPE) ||
            (type == MFM_MERGE_TS_FRAME_TYPE))
    {
        SetIsCtrl(true);
    }

    if ((type == MFM_VAMOSIC_FRAME_TYPE) ||
            (type == MFM_VAMOSPD_FRAME_TYPE)||
            (type == MFM_REA_GENE_FRAME_TYPE))
    {

        SetIsCtrl(true);


    }
#ifdef WITH_BLM
    if ((type == MFM_S3_SYNC_FRAME_TYPE) ||
    (type == MFM_S3_BAF2_FRAME_TYPE) ||
    (type == MFM_S3_DEFLECTOR_FRAME_TYPE) )
      {
        SetIsCtrl(true);
      }
#endif
    if ((type == MFM_HELLO_FRAME_TYPE))
    {
        SetIsCtrl(true);
    }

#ifdef SCALERS
    if ( type == MFM_SCALER_DATA_FRAME_TYPE )
    {
        cout << "<MFMDataFile><Next> MFM Scaler" << endl;
        cout << "   --> Not Controlled Frame !" << endl;
        SetIsCtrl(false);
        MFMScalerDataFrame* pScalerFrame;
        pScalerFrame = new MFMScalerDataFrame();
        pScalerFrame->SetAttributs(frame->GetPointHeader());

        cout << " ----- " << endl;
        cout << "NItemps "<< pScalerFrame->GetNbItems() << endl;
        cout << pScalerFrame->GetDumpData();
        delete pScalerFrame;
    }
#endif
    if ( type == MFM_MESYTEC_FRAME_TYPE )
    {
        //cout << "<MFMDataFile><Next> Mesytec Frame Type" << endl;
        SetIsCtrl(true);
    }

    if ( type == MFM_XML_FILE_HEADER_FRAME_TYPE )
    {
        cout << "<MFMDataFile><Next> Xml Header" << endl;
        cout << "   --> Not Controlled Frame !" << endl;
        SetIsCtrl(false);
        // MFMScalerDataFrame* pScalerFrame;
        // pScalerFrame = new MFMScalerDataFrame();
        // pScalerFrame->SetAttributs(frame->GetPointHeader());
        // cout << pScalerFrame->GetDumpData();
    }
    if ( type == MFM_XML_DATA_DESCRIPTION_FRAME_TYPE )
    {
        cout << "<MFMDataFile><Next> Xml Data description" << endl;
        cout << "   --> Not Controlled Frame !" << endl;
        SetIsCtrl(false);
        // MFMScalerDataFrame* pScalerFrame;
        // pScalerFrame = new MFMScalerDataFrame();
        // pScalerFrame->SetAttributs(frame->GetPointHeader());
        // cout << pScalerFrame->GetDumpData();
    }

    if(!GetIsCtrl())
    {
      
      if(type != MFM_HELLO_FRAME_TYPE && type != MFM_XML_FILE_HEADER_FRAME_TYPE && type != MFM_XML_DATA_DESCRIPTION_FRAME_TYPE )
      {
         Char_t Message[100];
         sprintf(Message,"\nUnknown MFM Frame TYPE : 0x%x ", type);
         MErr * Er = new MErr(WhoamI,0,0, Message);
         cout << Message << endl;
         fEventSkippedCount++;
         //#ifdef DEBUG
         //throw Er;
         //#endif

      }

    }
    else
        fEventCount++;


    return 1;

    END;
}
