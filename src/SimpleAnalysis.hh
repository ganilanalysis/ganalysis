/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef ANALYSIS_CLASS
#define ANALYSIS_CLASS
/**
 * @file   SimpleAnalysis.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date  April, 2021
 * @brief  Analysis Class
 *
 */
#include <sys/stat.h>
#include <dirent.h>
#include <bitset>

#include "Defines.hh"
#include "EnvManager.hh"
#include "LogManager.hh"
#include "DetManager.hh"
#include "ConfManager.hh"
#include "Parameters.hh"
#include "ParManager.hh"
#include "Map.hh"
#include "HistoManager.hh"
#include "BaseDetector.hh"
#include "TimeStamp.hh"

#include <chrono>
#ifdef WITH_NUMEXO
#include "NUMEXOParameters.hh"
#include "NumexoTopology.hh"
#endif
#ifdef WITH_GET
#include "GETParameters.hh"
#include "GETTopology.hh"
#endif
#ifdef WITH_MFM
#include "MFMDataFile.hh"
#include "MFMTypes.h"
#include "MFMCommonFrame.h"
#include "MFMBasicFrame.h"
#include "MFMBlobFrame.h"
#include "MFMExogamFrame.h"
#include "MFMEbyedatFrame.h"
#include "MFMMergeFrame.h"
#include "MFMCoboFrame.h"
#include "MFMOscilloFrame.h"
#include "MFMVamosPDFrame.h"
#include "MFMVamosICFrame.h"
#include "MFMReaGenericFrame.h"
#include "MFMMesytecFrame.h"
#ifdef WITH_PARIS
#include "MFMParisFrame.h"
#endif
#else
#include "GanilDataFile.hh"
#endif

#ifdef WITH_MESYTEC_DATA
#include "mesytec_buffer_reader.h"
#include "mesytec_experimental_setup.h"
#include "MesytecParameters.hh"
#include "MesytecTopology.hh"
#endif

#ifdef WITH_PISTA
#include "Pista.h"
#endif

#ifdef WITH_NFS
#include "NFSNeutronDet.hh"
#include "Medley.hh"
#include "NFSTDC.hh"
#include "NFSPPACS.hh"
#include "NFSPPACSN.hh"
#include "NFSPPACS_tdc.hh"
#endif

#ifdef WITH_FALSTAFF
#include "FST_Chio.hh"
#include "FST_Position.hh"
#include "FST_ID.hh"
#endif

#ifdef WITH_BLM
#include "BLM.hh"
#endif

#ifdef WITH_PISTA
#include "Pista.h"
#endif

#ifdef WITH_PARIS
#include "Paris.hh"
#include "ParisCluster.hh"
#endif
#ifdef WITH_EXOGAM
#include "Exogam.hh"
#endif

#ifdef WITH_VAMOS
#include "Identification.hh"
#include "Reconstruction.hh"
#include "IonisationChamberNUMEXO.hh"
#include "TargetPos.hh"
#include "FPMWPattern.hh"
#include "FocalPositionMW.hh"
#include "nDetector.hh"
#include "MTDC.hh"
#include "MTOF.hh"
#include "TargetSi.hh"
#endif
#ifdef WITH_LISE
#include "SiAnnular.hh"
#include "Caviar.hh"
#include "IdLISE.hh"
#include "DriftChamberZDD.hh"
#endif
#ifdef WITH_CATS
#include "CATS.hh"
#include "CATSTracker.hh"
#endif
#ifdef WITH_NPTOOL
#include "NPinterface.hh"
#endif

/*! Analysis */

class Analysis
{
private:

    clock_t start, start_run, end;
    string AnalysisPath;
    UShort_t CurrentFile;
    Bool_t fOption;
    Bool_t fConfName;
    Bool_t fOptionWhich[2];
    Int_t fAnMode;
    Bool_t fModeWhich[5];
    ULong64_t fNEvents;
    std::string ConfigFile;

    vector <string> IFName;
    vector <string> * IFN;

    vector <string> ParFName;
    vector <string> * ParFN;

    vector <string> OFName;
    vector <string> * OFN;

    vector <Float_t> BrhoRef;


    vector <BaseDetector *> Detectors;
    vector <BaseDetector *> * Det;


    MOFile *L; //! LogFIle
    Parameters *PL; //! Parameter List
#ifdef WITH_NUMEXO
    NUMEXOParameters *PL_NUMEXO; //! Parameter List
    NumexoTopology *NTopo;
#endif
#ifdef WITH_GET
    GETParameters *PL_GET; //! Parameter List
    GETTopology *GTopo;
#endif

    ParManager *PM;
    Map *MyMap;
#ifdef WITH_MFM
    MFMDataFile* GDF;
    MFMCommonFrame * frame;
    MFMCommonFrame * pInsideFrame;
    MFMEbyedatFrame * ebyframe;
    MFMVamosICFrame* pVamosICFrame;
    MFMVamosPDFrame* pVamosPDFrame;
    MFMReaGenericFrame* pREAFrame;
    MFMMergeFrame* pMergeFrame;
    MFMExogamFrame *pExogamFrame;
    MFMCoboFrame *pCoboFrame;
    MFMMesytecFrame *pMesytecFrame;
#ifdef WITH_PARIS
    MFMParisFrame* pParisFrame;
#endif
#else
    GanilDatafile *GDF;
#endif


#ifdef WITH_ROOT
    HistoManager *H; //! Pointer to Histogram Manager
    TFile *OutTFile; //! Pointer output Root file
    TTree *OutTTree; //! Pointer output Root tree
    TFile *InTFile; //! Pointer input Root file
    TTree *InTTree; //! Pointer input Root tree
#endif
#ifdef WITH_MESYTEC_DATA
    mesytec::experimental_setup setup;
    mesytec::buffer_reader fMesytecReader;
    void InitMesytecData(string MesytecPath="./"){
        START;
        cout << "Mesytec Path : " << MesytecPath << endl;
        fMesytecReader.read_crate_map(MesytecPath + "crate_map.dat");
        fMesytecReader.read_detector_correspondence(MesytecPath + "detector_correspondence.dat");
        END;
    }
    MesytecParameters *MesytecPar;
    MesytecTopology *MesytecTopo;

public :
    void AddMesytecData(const Int_t& Addr,
                        const Int_t& Bus,
                        const Int_t& Channel,
                        const UShort_t& Data,
                        const ULong64_t& TS=0,
                        const UShort_t& Time=0); //! Add MesyTec Data

#endif

    UShort_t *LblDataBuf; //! Buffer to Label/Data Couples
    string  Analysis_ExpName;

public:

    // Constructor
    Analysis(            int argc, //!< Number of Arguments
                         char** argv //!< Arguments Value
                         );

#ifdef WITH_ROOT
    // Constructor for NarVal
    Analysis(
            Parameters* P, //!< Parameters List
            HistoManager* HM = NULL,  //!< Histogram Manager
            Int_t AnMode = MODE_WATCHER  //!< Analysis Mode
            );
    // Constructor for GammaWare
    Analysis(
            TTree *t, //!< OutPut tree
            Parameters *P,
            HistoManager* HM  //!< Analysis Mode
            );
#endif
    // Constructor for Actor
    Analysis(
            MOFile *L, //!< Log File
            Parameters* P, //!< Parameters List
            Int_t AnMode = MODE_WATCHER  //!< Analysis Mode
            );


    // Destructor
    ~Analysis(void);

    template <typename T>
    T* GetDetPtr(const Char_t *Name);

    //! Setup Analysis
    void SetUpAnalysis();
    //! Setup Detectors From Config File
    void SetUpDetectorsFromFile(string DetFileName);

    //! To start noinit_counter if not using RunModeN
    void EndInit(){start_run = clock();}

#ifdef WITH_ROOT
    void SetOutTFile(TFile* OF);
    void AddBranches(TTree *t); //! Add all exiting branches ater setupdetector to a Tree
#endif
    void FillHistograms(void);     //!< Fill Histograms
    void FillTree(void);     //!< Fill OutPut Tree

    void Treat(void); //!< Treat the data
    void UpdateCounters(void);  //!< Update Counters
    void Reset(void);  //!< Reset Detectors and Counters
    void DisplayRate(); //! Dispaly rate;
    void PrintRateSummary(void); //! Print Rate Summary
    void PrintCounters(void); //! Print Counters
    // Counters
    ULong_t TotalEvt;
    ULong_t RunEvt;


protected:
    void AddComponent(BaseDetector * Cpnt){
        START;
        Det->push_back(Cpnt);
        END;
    };
public:

#ifdef WITH_MFM
    void AddMFMData(
            MFMEbyedatFrame * Data //!< Pointer to EbyEDat MFMframe
            );

    MFMEbyedatFrame * GetCalcFrame(void); // Return Calculated data in a frame;


    //! Treat MFM Buffer
    void TreatMFMBuffer(char* InBuf, // Input Buffer
                        unsigned int size_of_input_buffer,
                        char* OutBuf, // Output Bufer
                        unsigned int size_of_output_buffer,
                        unsigned int *used_size_of_output_buffer
                        );

    void  BufferOut(char* OutBufPtr,
                    unsigned int OutBufSize,
                    unsigned int *used_size_of_output_buffer,
                    MFMEbyedatFrame * inFrame,
                    MFMEbyedatFrame * CalcFrame); // Make Merged Buffer

    void UnPack(MFMCommonFrame *AFrame); //!< Unpack Method


#endif
    void Add(UShort_t *Data, //!< Pointer to Label/Data
             const ULong64_t& TS=0);
#ifdef WITH_NUMEXO
    void AddNUMEXO(const UShort_t& Board, //<! Board
                   const UShort_t& Channel, //<! Channel
                   const Int_t& Sample, //<! Sample
                   const UShort_t& Data, //<! Data
                   const ULong64_t& TS, //<! TimeStamps)
                   const Int_t& DataValid = -1, //<! DataValid Flag;
                   const Int_t& PileUp = -1, //<! PileUp Flag;
                   const Int_t& Time = -1 //<! PileUp Flag;
            );
#endif

#ifdef WITH_GET
    void AddGET(const Int_t& CoboId,
                const Int_t& AsadId,
                const Int_t& AgetId,
                const Int_t& ChannelId,
                const Int_t& SampleId,
                const UShort_t& Data,
                const ULong64_t& TS,
                const Int_t& Time = -1);

    void AddCoboData(MFMCoboFrame *frame, bool reducedFrame = false);

#endif

#ifdef WITH_PARIS
    void AddParisData(MFMParisFrame *frame);
#endif

    void Clear(void);     //!< Clear
    inline Map* GetMap(void){return MyMap;};     //!< Return Detector Map

public :
    Float_t GetBrhoRef(UShort_t RunNr);   //!< Get Reference Brho from BrhoList

    //! Get Run Directory Name (AGATA)
    string GetDirName(
            UShort_t RunNumber, //!< RunNumber
            const Char_t * Path //!< Path
            );

protected:
    //! Add Files
    void AddFiles(
            UShort_t Run
            );
    //! Add Files
    void AddFiles(
            string IF,
            string OF
            );
    void CloseInputTree(void); //!< Close Input Root Tree

    void DeleteDetectors(void);   //!< Delete Detectors
    //! Check if File Exists
    Bool_t FileExists(
            const char* FName //!< File Name
            );
    Float_t GetBrhoRef(string IF);        //!< Get Reference Brho from inputfile
    Int_t GetRunNr(string IF);         //!< GetRun Nr from file Name from inputfile
    Float_t GetCurrentBrhoRef(void);      //!< Get Current Brho for Current File
    void GetEnv(void);   //!< Get Environment variables
    //! Get File Name
    string GetFileName(
            UShort_t RunNumber, //!< RunNumber
            const Char_t * Path //!< Path
            );
    //! Get File Name
    string GetParFileName(
            UShort_t RunNumber, //!< RunNumber
            const Char_t * Path //!< Path
            );
    //! Get Multiple file names
    void GetFileNames(
            int argc,
            char** argv
            );
    //! Get file names
    void GetFileNamesForRuns(char* RName);
    //! Get file names
    void GetFileNamesForRuns(
            UShort_t Run,
            UShort_t Run1=0
            );
    //! Get option for cmd line arguments
    void GetOptions(
            int argc,
            char** argv
            );
    string GetCurrentInputFile(void);  //!< Return Current input file
    string GetCurrentOutputFile(void); //!< Return Current output file
    string GetCurrentParFile(void);    //!< Return Current parameter file
    void InitialiseOptions(void);      //!< Initialize Options
    Bool_t NextFile(void);             //!< Go to next File
    void OpenInputTree(void); //!< Open Input Root Tree

public:
    //! Setup Brho to fMode argument
    void SetBrhoWatcher(Float_t Brho);
    void PrintFileNames(void);         //!< Print File Name
    void PrintRaw(void);               //!< Print Raw Data
    void PrintCal(void);               //!< Print Cal Data
private:
    void ProcessFiles(void);           //!< Process Files
    //!< Reset File Counter
    void ResetFileCounter(void){CurrentFile=0;};

#ifdef NARVAL
    //! Get Watcher option from cmd line arguments
    void GetWatcherOptions(
            int argc,
            char** argv
            );

    void RunWatcher(void);  //!< Run Online Narval Watcher
#endif
    void Run(void);  //!< Run Mode 1

    //! Setup Analysis Variable according to fMode argument
    void SetUpGeneralParameters(UShort_t fMode);

    //! Create a Detector with all unasigned parameters declared in the header and not associated with declared detector
    void SetUpUnAssignedParams(void);

    template <typename T>
    T* SetUpDet(UShort_t fMode, Char_t *DName, UShort_t NDet, const Char_t *DNameExt = "");
#ifdef WITH_NUMEXO
    template <typename T>
    T* SetUpNUMEXODet(UShort_t fMode, Char_t *DName, UShort_t NDet, const Char_t *DNameExt = "",UShort_t MaxMult=1);
#endif
    void Usage(void); //!< Print Usage

    //! Environment Manager
    EnvManager *EM;
    //! Configuration Manager
    ConfManager *CM;
    //! Configuration Manager
    DetManager *DM;

    ULong64_t DuplicateCtr;
#ifdef D_AGAVA_VAMOS
    TimeStamp* VamosTS;
#endif

    UShort_t UnpackLevel;
#ifdef WITH_FALSTAFF
    const int FalstaffParOffSet = 20000;
#endif

#ifdef WITH_PARIS
    Paris* ParisDet;
#endif
#ifdef WITH_PISTA
    Pista* PistaDet;
#endif
};

template <typename T>
T* Analysis::SetUpDet(UShort_t fMode, Char_t *DName, UShort_t NDet,const Char_t *DNameExt)
{
    START;
    T *Det = NULL;
    Bool_t RawData = false;
    Bool_t CalData = false;
    Bool_t DefCal = true;
    if(fMode == MODE_WATCHER)
    {
        RawData= true;
        CalData= true;
    }
    else if(fMode == MODE_D2R)
    {
        RawData = true;
        CalData = false;
    }
    else if(fMode == MODE_D2A || fMode == MODE_R2A || fMode == MODE_CALC )
    {
        RawData = true;
        CalData = true;
    }
    else if(fMode == MODE_RECAL)
    {
        CalData = true;
    }
    else {
        Char_t Message[200];
        sprintf(Message,"Trying to Detector named  \"%s%s\" detector in an unknown Mode (%d) !",DName,DNameExt,fMode);
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    Det = new T(DName,NDet,RawData,CalData,DefCal,DNameExt);
    if(!Det)
    {
        Char_t Message[200];
        sprintf(Message,"Could not allocate memory to hold %s%s detector !", DName,DNameExt);
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    Det->SetAnalysisMode(fMode);
    Det->SetParameters(PL, MyMap);
#ifdef WITH_ROOT
    Det->SetIOOptions(OutTFile,DName,H,OutTTree,InTTree);
#endif
    AddComponent((BaseDetector*) Det);

    return Det;
    END;
}

#ifdef WITH_NUMEXO
template <typename T>
T* Analysis::SetUpNUMEXODet(UShort_t fMode, Char_t *DName, UShort_t NDet,const Char_t *DNameExt, UShort_t MaxMult)
{
    START;
    T *Det = NULL;
    Bool_t RawData = false;
    Bool_t CalData = false;
    Bool_t DefCal = true;
    if(fMode == MODE_WATCHER)
    {
        RawData= true;
        CalData= true;
    }
    else if(fMode == MODE_D2R)
    {
        RawData = true;
        CalData = false;
    }
    else if(fMode == MODE_D2A || fMode == MODE_R2A || fMode == MODE_CALC )
    {
        RawData = true;
        CalData = true;
    }
    else if(fMode == MODE_RECAL)
    {
        CalData = true;
    }
    else {
        Char_t Message[200];
        sprintf(Message,"Trying to Detector named  \"%s%s\" detector in an unknown Mode (%d) !",DName,DNameExt,fMode);
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    Det = new T(DName,NDet,RawData,CalData,DefCal,DNameExt,MaxMult);
    if(!Det)
    {
        Char_t Message[200];
        sprintf(Message,"Could not allocate memory to hold %s%s detector !", DName,DNameExt);
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    Det->SetAnalysisMode(fMode);
    Det->SetParameters(PL_NUMEXO, MyMap);
#ifdef WITH_ROOT
    Det->SetIOOptions(OutTFile,DName,H,OutTTree,InTTree);
#endif
    AddComponent((BaseDetector*) Det);

    return Det;
    END;
}
#endif

template <typename T>
T* Analysis::GetDetPtr(const Char_t *Name)
{
    START;
    T *Ptr=NULL;

    for(UShort_t i=0;i<Det->size();i++)
    {
        if(strcmp(Name, Det->at(i)->GetName())==0)
        {
            Ptr = (T*) Det->at(i);
#ifdef DEBUG
            cout << "Found <"  << Name << "> at pos "<< i << " -- Ptr " << Ptr << endl;
#endif
            break;
        }
    }

    if(!Ptr)
    {
        Char_t Message[100];
        sprintf(Message,"Could not Retrieve Detector Object named %s",Name);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }


    return Ptr;
    END;
}


#endif
