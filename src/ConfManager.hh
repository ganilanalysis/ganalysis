/****************************************************************************
 *    Copyright (C) 2020 by Antoine Lemasson
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
/**
 * @file   ConfManager.hh
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   April 2014
 * @brief  Configuration Manager Class Header
 * @class  ConfManager
 *
 */
#ifndef CONFMANAGER_H
#define CONFMANAGER_H

#include "LocalDefines.hh"
#include "Defines.hh"
#include <map>

/*! ConfManager */

class ConfManager
{

public:
    ConfManager(void)
    {
        START;
	IsInit = false;
        END;
    }

    ~ConfManager(void) noexcept(false) {
        START;
        instance = nullptr;

        END;
    }


    void Init(string ConfFile)
    {
        START;
	IsInit = true;
	Parse(ConfFile);

        END;
    }

    //!
    static ConfManager* getInstance(void){
        START;
        if (instance == NULL) {
            instance = new ConfManager();
            cout << "New instance of ConfManager" << instance << endl;
        }
        else
        {
            cout << "Foud existing instance of ConfManager" << instance << endl;
        }
        return instance;
        END;
    }

    void AddEnvValue(const string EnvName, const  string EnvValue){
        START;
        Env.insert(std::pair<string,string>(EnvName,EnvValue));
        END;
    }

    void UpdateEnvValue(string EnvName,string EnvValue)
    {
        START;

        END;
    }
    void PrintEnv()
    {
        START;
        cout << "================================== " << endl;
        cout << " Analysis Environement  "<< endl;
        cout << "================================== " << endl;
	cout << " Config from " << ConfFileName << endl;
        for (auto itr = Env.begin(); itr != Env.end(); ++itr) {
            cout <<  " " << itr->first << " \t : " << itr->second << '\n';

        }
        cout << "================================== " << endl;

        END;
    }

    void Parse(string fname)
    {
        START;
        ConfFileName = fname;
        cout << "Parsing Configuration File : " << ConfFileName << endl;
        Char_t name[255];
        Char_t val[255];
        Char_t * Ptr = NULL;
        Char_t Line[255];
        UShort_t Len = 255;
        MIFile IF(ConfFileName.c_str());

        while(IF.GetLine(Line,Len)){
            if( (Ptr = strstr(Line,"//")))
            {
                //cout << Line << endl;

            }
            else
            {
                if((sscanf(Line,"%s : %s", name, val) == 2))
                {
                    //  cout << Line << " -> " << name << " --- " << val <<endl; ;
                    AddEnvValue(name,val);
                }
            }

        }

        IF.Close();

        END;
    }


    static void Kill ()
    {
        START;
        if (NULL != instance)
        {
            delete instance;
            instance = NULL;
        }
        END;
    }

    string Get(const string Name)
    {
        START;
        auto  it = Env.find(Name);
        if (it != Env.end())
        {
         //     cout << "Found at Pos " << it->first << " " << it->second <<endl;
            return it->second;
        }
        else
        {
            char Error[256];
            sprintf(Error,"The key \"%s\" was not found in the Environment !",Name.c_str());
            cout << "Error " << Error << endl;
            MErr * Er = NULL;
            Er = new(Er) MErr(WhoamI,0,0, Error);
            throw Er;
        }

        return "";
        END;
    }

  // 
  Bool_t IsInit;

private:
    // The static instance of the Parameters class:
    static ConfManager* instance;

    // Configuration filename
    string ConfFileName;

    // Environment Variables <EnvName,EnvValue>
    std::map<string, string > Env;

};

#endif // CONFMANAGER_H
