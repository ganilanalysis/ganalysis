/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *       
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "GanilDataFile.hh"


void dump(char *p, int nb)
{
int i, j, k;
unsigned short *pShort;
unsigned char *pChar;
   j = nb / 16;
   pShort = (unsigned short *)p;
   pChar = (unsigned char *)p;
   for (i = 0; i < j; i++)
   {
      printf("\n%8s:", pChar);
      for (k = 0; k < 8; k++)
         printf(" %04x", *pShort++);
      printf("    ");
      for (k = 0; k < 16; k++)
      {
         if ((*pChar >= 32) && (*pChar < 127))
            printf("%c", *pChar);
         else
             printf(".");
         pChar++;
      }
   }
   printf("\n\r");
}

bool AutoswBuf,Swbufon;
Int_t BufSize;


GanilDataFile::GanilDataFile(const string filename)
{
  START;
  InitDefault();
  fFileName=filename;

  struct stat st;
  stat(fFileName.c_str(), &st);
  fFileSize = st.st_size;

  strncpy(fDevice->DevName,fFileName.c_str(),MAX_CARACTERES);
  Open();
  InitScaler(filename);
  END;  
}

GanilDataFile::~GanilDataFile             (void)
{
  START;
  CloseScaler();
  Close();

  if(fDevice) delete fDevice;
  if(fBuffer) delete fBuffer;
  if(fStructEvent) delete[] fStructEvent;
  if(fEventBrut) delete[] fEventBrut;
  fDevice = NULL;
  fBuffer = NULL;
  fStructEvent = NULL;
  fEventBrut = NULL;
  END;  
}

void GanilDataFile::InitDefault(const Int_t argc, char **argv)
{
  START;
  fStatus            = ACQ_OK;
  fEventNumber       = -1;
  fEventCount        = 0;
  fEventSkippedCount = 0;
  fIsCtrl         = false;

  fDevice         = new gan_tape_desc;
  fBuffer         = new in2p3_buffer_struct;
  fStructEvent    = new char[STRUCTEVENTSIZE];

  fBufferNumber = 0;
  EventLength = 0; 
  NItems = 0; 
  fBadMerge = 0;
  
  val_ret DeRetour;   // Union to communicate with ganil_tape lib routines
  // Getting Logical Unit ID number
  fStatus = acq_get_param_env (lun_id,&DeRetour,argc,argv );
  if ( DeRetour.Val_INT >= 0 ) fLun = DeRetour.Val_INT;
  else fLun=10; // Random
  fDevice->Lun=fLun;
  // Getting tape/file name
  fStatus = acq_get_param_env ( device_id, &DeRetour, argc, argv );
  strncpy(fDevice->DevName, DeRetour.Val_CAR,MAX_CARACTERES);
  fFileName=DeRetour.Val_CAR;
  
  // GLOBAL VARIABLES ASSIGNED HERE
  fStatus = acq_get_param_env ( autoswbuf_id, &DeRetour, argc, argv );
  AutoswBuf = DeRetour.Val_BOL;
            
  fStatus = acq_get_param_env ( swbufon_id, &DeRetour, argc, argv );
  Swbufon = DeRetour.Val_BOL;

  // Variable/fix length events 
  fStatus = acq_get_param_env ( ctrlform_id, &DeRetour, argc, argv );
  fCtrlForm = DeRetour.Val_INT;
  
  fStatus = acq_get_param_env ( density_id, &DeRetour, argc, argv );
  fDensity = DeRetour.Val_INT;

  fStatus = acq_get_param_env ( bufsize_id, &DeRetour, argc, argv );
#ifdef D_MUST2
   fBufSize = BufferSize;
#else
   fBufSize = DeRetour.Val_INT;
#endif
  cout << fBufSize<< endl ;
  // GLOBAL VARIABLES ASSIGNED HERE
  BufSize=fBufSize;

  fStatus = acq_get_param_env ( evbsize_id, &DeRetour, argc, argv );
  fEvbsize = DeRetour.Val_INT;

  fStatus = acq_get_param_env ( evcsize_id, &DeRetour, argc, argv );
  fEvcsize = DeRetour.Val_INT;

  fEventBrut = NULL;
  fEventBrut = new UShort_t[fEvbsize];
  fEventCtrl = NULL;

#ifdef TRIG_PAT
  Pattern = 0;
#endif
  END;  
}



void GanilDataFile::Open(void)
{
  START;
  
  Char_t Message[100];
  fStatus=acq_dev_is_alloc_c(*fDevice); // Allocation test
  if (fStatus==ACQ_OK) fAllocated=true;
  else {
	 sprintf(Message,"test d'allocation");
    gan_tape_erreur(fStatus,Message);
    return;
  }

  fStatus=acq_mt_alloc_c(*fDevice);     // Allocation
  if (fStatus!=ACQ_OK) {
    if (fStatus==ACQ_NOTALLOC) 
      cout << "This tape drive is already in use !"<<endl;
	 
	 sprintf(Message,"allocation");
    gan_tape_erreur(fStatus,Message);
    return;
  }

  fStatus=acq_mt_mount_c(*fDevice,fDensity,fBufSize);
  if (fStatus!=ACQ_OK) {  // There might be some imprecisions here
    if (fStatus == ACQ_ALREADYMOUNT)
      cout << "The tape you want to use is already mounted"<<endl;
	 sprintf(Message,"Mount");
	 gan_tape_erreur(fStatus,Message);
    return;
  }
  
  fStatus = acq_mt_open_c(fDevice, o_read, &fLun);
  if (fStatus!=ACQ_OK) {  // There might be some imprecisions here
	 sprintf(Message,"Open");
    gan_tape_erreur(fStatus,Message);
    return;
  }
  fRunNumber=0;
  Int_t structEvent_size=STRUCTEVENTSIZE; // bidon
  fStatus = acq_mt_ini_run_c ( fLun, fBuffer, fBufSize, &fRunNumber,
										 fStructEvent,structEvent_size );

  if ( fStatus != ACQ_OK ) {
 	 sprintf(Message,"Init Run");
    gan_tape_erreur (fStatus,Message);
    return;
  }
  cout << "  <GanilDataFile> Openned " << fFileName  <<endl;
  END;  
}

void GanilDataFile::Close(void)
{
  START;
  Char_t Message[100];
  if(fDevice)
	 {
		fStatus = acq_mt_close_c(*fDevice);
		if (fStatus!=ACQ_OK) {
		  sprintf(Message,"Close");
		  gan_tape_erreur(fStatus,Message);

		}
		
		fStatus = acq_mt_dealloc_c(*fDevice);
		if (fStatus!=ACQ_OK) {
		  if (fStatus==ACQ_NOTALLOC) 
			 cout << "Could Not Close the Device  as not allocated!"<<endl;
		  sprintf(Message,"De-Allocation");
		  gan_tape_erreur(fStatus,Message);
		}
		delete fDevice;
		fDevice = NULL;
	 }
  //  if(fEventSkippedCount)
  cout << endl <<
		"From GanilDataFile: "<<
    "Events Read :" << fEventCount << " -- Events Skipped :" << fEventSkippedCount << " -- BadMerge :" << fBadMerge 
			<< endl;
  
  END;
}


bool GanilDataFile::ReadScalerBuffer(void)
{
  START;
  scale_struct Scaler;
  long int Val, Freq, Val1, Freq1;
  int Channels;
  fprintf(fpoScaler,"Scaler Buffer : %010ld\n", ++nScaler);
  fprintf(fpoScaler,"========================================================\n");;
  Channels = fBuffer->les_donnees.cas.scale.Nb_channel;
  if(Channels>maxScalerChannels)
    {    
      printf("\nToo many scaler channels %d!\n",Channels) ;
      return true;
	//      exit(-1);
    }
  for(int i=0;i<Channels/2;i++) 
    {
      Scaler = ( scale_struct ) fBuffer->les_donnees.cas.scale.jbus_scale.UnScale[i+1] ;
      Val = Scaler.Count ;
      Freq = Scaler.Freq ;
      Scaler = ( scale_struct ) fBuffer->les_donnees.cas.scale.jbus_scale.UnScale[i+Channels/2] ;
      Val1 = Scaler.Count ;
      Freq1 = Scaler.Freq ;
      //printf("%10s %012ld %07ld %10s %012ld %07ld\n", chScaler[i], Val, Freq, chScaler[i+Channels/2] , Val1, Freq1);

      fprintf(fpoScaler,"%10s %012ld %07ld %10s %012ld %07ld\n", chScaler[i], Val, Freq, chScaler[i+Channels/2] , Val1, Freq1); 

	 }
  fprintf(fpoScaler,"========================================================\n");;
  return(true);
  END;  
}

bool GanilDataFile::Next(void)
{
  START;

  if (fEventNumber==-1)  ReadBuffer();
  if (fEventNumber==-2) fEventNumber=-1;
    
  if (fStatus) return(false); // Maybe more to say here

  if (strcmp (fHeader , SCALER_Id ) == 0 ) 
	 {
		fIsCtrl = false;
		fEventNumber=-1;
		return(ReadScalerBuffer()); 
	 }
  if ( strcmp (fHeader , EVENTDB_Id ) == 0 )  {
    fIsCtrl = false;
    return(ReadNextEvent());
  }
  else if ( strcmp (fHeader , EVENTCT_Id ) == 0 ) {
    fIsCtrl = true;
    return(ReadNextEvent());   
  }
  else if ( strcmp (fHeader , EBYEDAT_Id ) == 0 ) {
    fIsCtrl = true;
    return(ReadNextEvent_EBEYEDAT());   
  }
  else if ( strcmp (fHeader , "0EBYEDAT" ) == 0 ) {
    fIsCtrl = true;
    return(ReadNextEvent_EBEYEDAT());   
  }
  else if ( strcmp (fHeader , ENDRUN_Id ) == 0 ) {
    fIsCtrl = false;
    cout << endl << "ENDRUN"<< endl;
    return(false);   
  }
  else if ( strcmp (fHeader , " TERMINE" ) == 0 ) {
    fIsCtrl = false;
    cout << endl << "ENDRUN"<< endl;
    //Try to check if there is one scaler buffer stil behind the end
    ReadBuffer();
    if (strcmp (fHeader , SCALER_Id ) == 0 ) 
      {
		  ReadScalerBuffer(); 
      }
    return(false);   
  }
  cout <<endl <<"Unknown header in GanilDataFile::Next: " << fHeader << " Buffer size " << fBufSize <<endl;
  return(true);
  END;  
}

char * GanilDataFile::CopyParam (char *Dest, char *Source) const
{
  char c;
  int len = 0;
  do {
	 len++;
	 c = *Source++;
	 if ( (c == ',') || (c == 0x0d) )
		c = '\0';
	 if ( len <= 20)
		*Dest++ = c;
	 else
		*Dest = '\0';
  } while ( c != '\0' );
  /* Verifier qu'on n'a pas plusieurs 0x0D de suite */
  while ( *Source == 0x0d )
	 { Source = '\0'; Source ++;}
  return( Source );
}
void GanilDataFile::CloseScaler()
{
  START;
  fclose(fpoScaler);
  END;  
}
void GanilDataFile::InitScaler(const string filename)
{
  START;


  fScalerFile = filename;
  myreplace(fScalerFile,"/runold/","/Scalers/");
  fScalerFile=fScalerFile+".sca";

  cout << "  <GanilDataFile> Opening " << fScalerFile << endl;
  fpoScaler=fopen(fScalerFile.c_str(),"w");

  if(!fpoScaler)
    {
      Char_t Message[200];
      sprintf(Message,"  <GanilDataFile><Scaler> Could not open file :  %s", fScalerFile.c_str());
      MErr * Er= new MErr(WhoamI,0,0, Message);
      throw Er;
    }
  else
    {
      cout << "  <GanilDataFile> " << fScalerFile << " opened" << endl;      
    }
  cout << "  <GanilDataFile> Opening Scalers.sav " << endl;
  fpiScaler=fopen("Scalers.sav","r");

  if(!fpiScaler)
    {
      cout << "  <GanilDataFile> Scalers.sav not found !" << endl;
      for(int i=0;i<maxScalerChannels;i++)
		  {
			 sprintf(chScaler[i], "Chan_%02d",i);
			 //	  cout << chScaler[i] << endl;
		  }
    }
  else
    {
      char tmp[20];
      cout << "  <GanilDataFile>  Scalers.sav opened" << endl;      
		for(int i=0;i<80;i++)
		  {
			 for(int j=0;j<4;j++)
				if(fscanf(fpiScaler, "%s", tmp) == 1)
				  {
					 ;
				  }
			 
			 sscanf(tmp+9, "%s", chScaler[i]);
			 //	  cout << chScaler[i] << endl;
			 
			 if(fscanf(fpiScaler, "%s", tmp) == 1)
				{
				;
				}
		  }
    }

  nScaler=0;
  END;  
}
  
bool GanilDataFile::ReadNextEvent(void)
{
  START;
  Char_t Message [100];
  if (!fIsCtrl)
	 {
		fStatus = get_next_event(fBuffer,fBufSize,
										 (short*)fEventBrut,fEvbsize,&fEventNumber );
		if ( fStatus == ACQ_OK ) {
		  fStatus = s_evctrl ((short*)fEventBrut,(short*)fEventCtrl,
									 fStructEvent,&fCtrlForm );
		  if ( fStatus == GR_OK )
			 return(GetEvent());
		  else {
			 puts ("\n>>> Erreur de reconstruction");
			 return(false);
		  }
		}
		else if (fStatus == ACQ_ENDOFBUFFER ) {
		  fEventNumber=-1;
		  return(Next());
		}
		else {
		  sprintf(Message,"While getting and Event");
		  gan_tape_erreur (fStatus,Message);
		  return(false);
		}
	 }
  else
	 {
		fStatus = get_next_event(fBuffer,fBufSize,(short*)fEventCtrl,
										 fEvcsize,&fEventNumber );
		if ( fStatus == ACQ_OK ) {
		  return(GetEvent());
		}
		else if ( fStatus == ACQ_ENDOFBUFFER ) {
		  fEventNumber=-1;
		  return(Next());
		}
		else {
		  sprintf(Message,"While getting and Event");
		  gan_tape_erreur (fStatus,Message);
		  return(false);
		}
	 }
  END;  
}

bool GanilDataFile::ReadNextEvent_EBEYEDAT(void)
{
  START;
  Char_t Message [100];
  if (fIsCtrl)
	 {
		fStatus = acq_ebyedat_get_next_event( (UNSINT16 *)fBuffer,
														  &fEventCtrl,
														  &fEventNumber,EVCT_VAR );
		// dump((char *)fBuffer,128);
		if ( fStatus == ACQ_OK ) {
		  return(GetEvent());
		}
		else if ( fStatus == ACQ_ENDOFBUFFER ) {
		  fEventNumber=-1;
		  return(Next());
		}
		else {
		  sprintf(Message,"While getting and Event");
		  gan_tape_erreur (fStatus,Message);
		  return(false);
		}
	 }
  return(false);
  
  END;  
}


void GanilDataFile::ReadBuffer(void)
{
  START;
  fStatus = acq_mt_read_c ( fLun, fBuffer->Buffer, &fBufSize );
  if ( fStatus != ACQ_OK )
    {
      cout << endl << "Not OK"<< endl; // Maybe stuff to do to handle the error
      cout << "Maybe the file is not closed yet ..." << endl;
    }
  strncpy(fHeader,fBuffer->les_donnees.Ident, 8);
  fHeader[8]='\0';
  fBufferNumber++;

  END;  
}

bool GanilDataFile::GetEvent(void)
{
  START;
  CTRL_EVENT *pCtrlEvent=(CTRL_EVENT *)fEventCtrl;
  Data = (UShort_t * ) (&(pCtrlEvent->ct_par));;
  EventLength = pCtrlEvent->ct_len-6;
  NItems = EventLength/2;
  
  Bool_t fPADNOK;
  fPADNOK = false;
  
  Bool_t fOK;
  fOK = true;

#ifdef D_MUST2
  const short NTrigBranch = 4;
  Char_t TrigName[NTrigBranch][20] = {"MUST","BTD","TGV","VAMOS"};
  UShort_t fTrigLbl[NTrigBranch] = {1,5000,6000,7000};
#endif
#ifdef D_E641S
 const short NTrigBranch = 4;
  Char_t TrigName[NTrigBranch][20] = {"VXI_1","VXI_2","TGV","VAMOS"};
  UShort_t fTrigLbl[NTrigBranch] = {1,1000,3000,2000};
#endif

#ifdef D_AGATA
  const short NTrigBranch = 5;
  Char_t TrigName[NTrigBranch][20] = {"VAMOS","DC1","AGAVA","DC2","MW"};
  UShort_t fTrigLbl[NTrigBranch] = {1,1000,2000,3000,4000};
#endif
#ifdef TRIG_PAT

  Int_t PatMask =0;
  Pattern = 0;
  for(UShort_t k=0; k< NTrigBranch;k++)
     PatMask |= (1<<k) ;

#endif  
  fEventCount++;

  for(Int_t k=0;k<EventLength;k+=2)
    {
      if(Data[k] ==0)
	{
	  fOK = false;
	  char Error[256];
	  sprintf(Error,"LABEL Error : Label 0 found in the data LABEL: %d DATA : %d at Pos %d !", Data[k],Data[k+1],k);
	  MErr * Er = NULL;
	  Er = new(Er) MErr(WhoamI,0,0, Error);
	  throw Er;
	}
	for(Int_t l=k+2;l<EventLength;l+=2)
	  if(Data[k] == Data[l])
	    {
	      
	      cout << "\n i: " << k << " i+2 :" << l << " ct_nb : " << pCtrlEvent->ct_nb << " Length : " <<pCtrlEvent->ct_len <<" Label : " << Data[k] << " Data : " << Data[k+1] << " Data+1: " << Data[l+1] <<" " << endl;
	      fOK=false;
	    }

#ifdef TRIG_PAT
	for(UShort_t i = 0; i<NTrigBranch ; i++)
	  if(Data[k] == fTrigLbl[i] && Data[k+1] > 0) 
	    {
	      Pattern |= (1<<i) ;
	    }

#endif
    }
  
 
#ifdef TRIG_PAT
  
//  if(Pattern != PatMask)
//    {
//      cout << "\n ! Error in Merging - Evt number : " << fEventNumber << " " << EventLength << endl;
//      for(UShort_t i = 0; i<NTrigBranch ; i++)
//	cout << TrigName[i] << " : " << ((Pattern>>i)  & 1) << " - " ;

//      cout << "expecting " <<  PatMask << endl;
//      fBadMerge++;
//    }

#endif     
  
  if(!fOK)
    {
      cout << "\nEVENT wrong -- Duplcated Labels : Event :" << fEventNumber << endl;
      fEventSkippedCount++;
      return(Next());
    }

  if(fPADNOK)
    {
      cout << "\n ! Possible Error : " << fEventNumber << " " << endl;           
      for(Int_t k=0;k<EventLength;k+=2)
	cout << " Label : " << Data[k] << " Data : " << Data[k+1] << endl;

    }

  return(fOK);
  END;
}


void GanilDataFile::GetParameters(Parameters *Par) 
{
  START;
  vector<string> *ParName = new vector<string>();
  vector<UShort_t> *ParId = new vector<UShort_t>();
  vector<UShort_t> *ParSize = new vector<UShort_t>();
	 
  Int_t NPars = 0;
  char *CurrPointer;
  char tmp[20];

  do {
	 ReadBuffer();
	 if (strcmp (fHeader, PARAM_Id) == 0 )
		{        
		  CurrPointer = (char *) fBuffer->les_donnees.cas.Buf_param;
		  while ( strncmp(CurrPointer, "    ",4) != 0 &&  
					 strncmp(CurrPointer, "!!!!",4) != 0 )
			 {
				NPars++;
				CurrPointer = CopyParam(tmp, CurrPointer);
				ParName->push_back(tmp);
				CurrPointer =  CopyParam(tmp, CurrPointer);
				ParId->push_back(atoi(tmp));
				CurrPointer =  CopyParam(tmp, CurrPointer);
				ParSize->push_back(atoi(tmp));
				
			 }
		}
  } while (strcmp (fHeader, PARAM_Id) == 0);

  if(NPars ==0)
    {
      Char_t Message[200];
      sprintf(Message,"  <GanilDataFile><GetParameter> No Parameter Buffer Found in EbyEdat data file");
      MErr * Er= new MErr(WhoamI,0,0, Message);
      throw Er;
      
    }
  //Here one buffer too much read so it is in the memory
  fEventNumber=-2; //to pass the information to Next()
  
  Par->SetNParameters(NPars);
  
  for(UShort_t i=0; i< ParName->size(); i++)
	 {
		Par->Add(ParId->at(i),ParName->at(i),ParSize->at(i));
	 }
  
  delete	 ParName;
  delete	 ParId;
  delete	 ParSize;

  END;
}



Int_t GanilDataFile::GetRunNumber(void) const
{
  return fRunNumber;
}
