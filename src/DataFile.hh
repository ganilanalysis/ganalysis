/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *       
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _DATAFILE_CLASS
#define _DATAFILE_CLASS
/**
 * @file   DataFile.hh
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   April 2014
 * @brief  DataFile Class Header
 * 
 * 
 */
#include "LocalDefines.hh"
#include "Parameters.hh"

class Analysis;

class DataFile
{
public:
  
  /// Create the reading class over the given file with all defaults values
  DataFile(const string filename);
  ~DataFile(void);
 
  /// Close data file
  virtual Bool_t Close(void){return 0;};
  /// Return Label/Data couple at Pos
  virtual UShort_t *GetDataAt(UShort_t Pos){return(Data+Pos);}; 
  // /// Read a Label/Data pair and put it into Data array and return true until end of event is reached
  // bool GetEvent(void);
  /// Return EventNumber
  virtual Int_t  GetEventNumber(void){return(fEventNumber);};
  /// Return Number of Sub-Items
  virtual UShort_t GetNItems(void){return NItems;}; 
  /// Return event length
  virtual UShort_t GetEventLength(void){return EventLength;}; 
  /// Get Parameter Labels from File Header
  virtual void GetParameters(Parameters *Par){};
  /// Get Parameter Labels from Action File
  virtual void GetParametersFromActionFile(Parameters *Par,const Char_t *file);
  /// Return Tape status
  virtual Int_t GetStatus (void) const {return(fStatus);}
  /// Returns current run number
  virtual Int_t GetRunNumber (void) const {return(fRunNumber);};
  /// Init Class and defult Tape parameters
  virtual void InitDefault ();
  /// Read an event on tape/file and put in into the event array return true until read fails 
  virtual Bool_t Next(void){return 0;}; 
  /// Open data file
  virtual Bool_t Open(void){return 0;};
  /// Return BytesRead
  inline Float_t GetMBytesRead (void) const {return(BytesRead/1024./1024.);}
  // /// Return BytesRead
  inline ULong64_t GetFileSize (void) const {return(fFileSize/1024./1024.);}
  inline const char* GetFileName (void) {return fFileName.c_str();}
  inline Int_t GetBadMerge (void) const {return(fBadMerge);}
  inline void SetEventNumber (Int_t evt) {fEventNumber=evt;}
  inline void SetLength(Int_t len) {EventLength=len;}
  inline void SetNItems(Int_t n) {NItems=n;}
  //< Set Controled Status
  inline void SetIsCtrl(Bool_t v) {fIsCtrl=v;}
  //< Get Controled Status
  inline Bool_t GetIsCtrl (void) const {return(fIsCtrl);}

  virtual void UnPack(Analysis *An);
  

#ifdef TRIG_PAT
  virtual  inline UShort_t GetTrigPat (void)  {return(Pattern);}
#endif

public:
  ULong64_t BytesRead; /// Nb of bytes read

private: 
  string fFileName;  ///< Filename, can be a tape drive
  Int_t  fStatus;  ///< Status, 0 is OK, any other value suspect
  Int_t  fBufSize;  /// Buffer size
  Int_t  fCtrlForm;  ///< Fix/variable length events
  Int_t  fEvbsize;  ///< Size of the brut data buffer
  Int_t  fEvcsize;  ///< Size of the ctrl data buffer
  Int_t  fRunNumber;  ///< current file run number
  char   fHeader[9];  ///< Buffer header
  Int_t  fEventNumber; ///< Local event number in current buffer (should be renamed)
  Int_t  fBufferNumber; ///< Buffer Number
  Int_t  fEventCount;  ///< Our event counter
  Int_t  fEventSkippedCount; ///< Our skipped event counter
  bool   fIsCtrl; ///< We are currently in a CTRL buffer
  UShort_t *Data; ///< Label/Data couple
  Int_t  EventLength; ///< Eventlength
  Int_t  NItems; ///< Number of Sub-Items
  ULong64_t  fFileSize;  ///< Data FileSize
  Int_t  fBadMerge;  ///< Number of Events FileSize
#ifdef TRIG_PAT
  UShort_t Pattern; //< Trigger pattern
#endif

};
#endif
