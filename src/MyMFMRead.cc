/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *       
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "LocalDefines.hh"
#include "MFMDataFile.hh"

#include <iostream>
using namespace std;
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include "MFMBasicFrame.h"
#include "MFMBlobFrame.h"
#include "MFMExogamFrame.h"
#include "MFMEbyedatFrame.h"
#include "MFMMergeFrame.h"
#include "MFMCoboFrame.h"
#include "MFMOscilloFrame.h"
#include <cstring>
#include <cstdlib>

int main(int argc, char** argv)
{
   try
	 {	
     MOFile* L = new MOFile("Test.log");

     Parameters* PL = new Parameters(16385,L);

      MFMDataFile *GDF = new MFMDataFile("./mfmfile.dat");
		cout << "---- READ MFMFrame from file----------\n";

      GDF->GetParameters(PL);
      PL->Print();
      while(GDF->Next())
        {
          
          cout << GDF->GetFileSize() << " FS"  << GDF->GetFileName()  << endl;
          int Length =GDF->GetEventLength();
          for (int i = 0; i < Length; i++) {
            UShort_t *Data =  GDF->GetDataAt(i);
            cout << "<" << *(Data) << ", " << *(Data+1) << ">, " << endl;;
          }          
        }

      
      GDF = CleanDelete(GDF);


	 }
  catch(MErr * Er)
	 {
		if(Er->FunctionCode==1&&Er->ErrorCode==1)
		  ;//O.K. to exit without a Message
		else
		  {
			 Er->Set(WhoamI);
			 Er->Print();
		  }
		Er = CleanDelete(Er);
	 }
  catch(exception &e)
	 {
		cout << "Caught Exception : " << e.what() << endl;
	 }
  catch(...)
	 {
		cout << "Unkown Exception : "  << endl;
	 }
  return 0;

}
