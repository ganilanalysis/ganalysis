/****************************************************************************
 *    Copyright (C) 2020 by Antoine Lemasson
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "LocalDefines.hh"
#include <GNetClientNarval.h>
#include <GNetServerRoot.h>
#include "GUser.hh"

using namespace std;

int main(int argc, char** argv)
{
   int  nEvents;
   UShort_t  PortNum;
   char OFName[100];
   char ServerName[100];
   char IFName[100];
   char Line[200];
   char *Exp;
   TFile *MyFile;
   GUser * action;
   GNetClientNarval *fileN;
   GNetServerRoot  *net;
   GFile *file;


   try{

      EnvManager *EM = EnvManager::getInstance();
      std::string Analysis_PathVar = "ANALYSIS";
      EM->setPathVar(Analysis_PathVar);
      cout << "Env Manager PATH in GetEnv " << EM->getPathVar() << endl;

      char *Path;
      char *Exp;
      Path = getenv((EM->getPathVar()).c_str());
      if(Path == NULL)
      {
          MErr * Er = new MErr(WhoamI,0,0, "Could not Find Anaylsis Path Env!");
          throw Er;

      }
      char cfile[100];
      sprintf(cfile,"%s/Confs/Watcher.conf",Path);
      ConfManager *CM = ConfManager::getInstance();
      CM->Init(cfile);
      CM->PrintEnv();
      // EM->setWatcherServerVar(WatcherServer_Var);
      EM->setWatcherServerVar(CM->Get("Watcher_Server"));


      //    string Watcher_Server = getenv((EM->getWatcherServerVar()).c_str());
      EM->print();


      string Watcher_Server = CM->Get("Watcher_Server");


      cout << "    **********************************************" <<endl;
      cout << "    *                   Watcher                  *" << endl;
      cout << "    **********************************************" <<endl<< endl;
      if(argc < 1 || argc > 3)
      {
         cout <<
                 "Usage: ";
         cout << argv[0] ;
         cout << " <Port Number> (10201 for ganp609 - 10202 for ganp617)" 	    << endl << endl ;
         exit(-1);
      }
      else if(argc == 1)
      {
         nEvents = 0;

         PortNum = atoi(CM->Get("Watcher_Port").c_str());
         sprintf(ServerName,"%s",Watcher_Server.c_str());
         cout << "Processing All Events" << endl;
         cout << "New Watcher on port " << PortNum << " on " << ServerName << endl;

      }
      else if(argc >= 2)
      {
         sscanf(argv[1],"%d",&PortNum);
         nEvents = 0;
         cout << "Processing All Events" << endl;
         if(argc == 3)
         {
            sscanf(argv[2],"%s",ServerName);
            cout << "ServName: " << ServerName << endl;
         }
         else
         {
            sprintf(ServerName,"%s",Watcher_Server.c_str());
         }

     if((PortNum > 10201) && (PortNum < 10250))
         {
            cout << "New Watcher on port " << PortNum << " on " << ServerName << endl;
         }
         else
         {
            cout << "Specify watcher port Number: (10201 master, 10202 slave or 10203 GFP) " << PortNum << endl;
            exit(-1);
         }
      }
      sprintf(OFName,"Watcher_%d.root",PortNum);


      cout << "  <Analysis> Analysis Exp Name : ";


      char ExpName[50];
      sprintf(ExpName,"%s",CM->Get("ExpName").c_str());

      MyFile = new TFile(OFName,"recreate");
      if(argc<4)
      {
         fileN = new GNetClientNarval(ServerName);
     //fileN->SetBufferSize(16384);
         action = new GUser(fileN);
      }

      action->SetUserMode(1);
      action->SetSpectraMode(0);
      action->InitUser();

      if(argc<4)
      {
         fileN->SetPort(PortNum);
         fileN->Open();
      }
      action->Infos();



      if(argc<4)
      {
#ifdef WITH_MFM
         action->EventInit(ExpName,"mfm");
#else
         action->EventInit(ExpName,"ganil");
#endif
      }
      else
      {
#ifdef WITH_MFM
         action->EventInit(ExpName,"mfm");
#else
         action->EventInit();
#endif
      }

      Int_t SpectraServerPort = atoi(CM->Get("SpectraServerPort").c_str());

      action->GetAnalysis()->SetOutTFile(MyFile);
      net = new GNetServerRoot (SpectraServerPort,action);
      net->StartServer();
      action->DoRun(nEvents);
      TDatime date;
      printf(" End at Date :%d Time : %d \n",date.GetDate(),date.GetTime());

      MyFile->Write();
      net->StopServer();
      if(argc<4)
      {
         fileN->Close();
      }
      else
      {
         file->Close();
      }
      action->EndUser();
      net = CleanDelete(net);

      // a = CleanDelete(a);

      MyFile->Close();
      delete MyFile;

      if(argc<4)
      {
         delete fileN;
      }
      else
      {
         delete file;
      }

      if(argc==2)
      {
         sprintf(Line,"rm -f %s","Watchertoto.root");
         gSystem->Exec(Line);
      }

      CM->Kill();


   }
   catch(MErr * Er)
   {
      if(Er->FunctionCode==1 && Er->ErrorCode==113)
      {
         cout << "You pressed qqq -> exiting" << endl;
         TDatime date;

         printf(" End at Date :%d Time : %d \n",date.GetDate(),date.GetTime());
         MyFile->Write();
         net->StopServer();
         fileN->Close();
         action->EndUser();
         net = CleanDelete(net);
         //	  a = CleanDelete (a);

         MyFile->Close();
         delete MyFile;

         if(argc<4)
         {
            fileN->Close();
            delete fileN;
         }
         else
         {
            file->Close();
            delete file;
         }

         if(argc==2)
         {
            sprintf(Line,"rm -f %s","Watchertoto.root");
            cout << Line << endl;
            gSystem->Exec(Line);
         }

      }
      else
      {
         Er->Set(WhoamI);
         Er->Print();
      }
   }
}



