/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *       
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef __DEFINESGT_DEF
#define __DEFINESGT_DEF
extern "C"
{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <GEN_TYPE.H>
#include <ERR_GAN.H>
#include <gan_tape_erreur.h>
#include <gan_tape_general.h>
#include <gan_tape_param.h>
#include <gan_tape_test.h>
#include <gan_tape_alloc.h>
#include <gan_tape_mount.h>
#include <gan_tape_file.h>
#include <acq_mt_fct_ganil.h>
#include <acq_ebyedat_get_next_event.h>
#include <gan_tape_get_parametres.h>
#include <STR_EVT.h>

#ifndef __SCALE_DEF
#define __SCALE_DEF
  typedef struct SCALE
  {
    UNSINT32 Length; /* Nb bytes utils suivant ce mot */
    UNSINT32 Nb_channel; 
    UNSINT32 Acq_status;
    UNSINT32 Reserve[2];
    union JBUS_SCALE
    {
      scale_struct UnScale[NB_MAX_CHANNEL];
      UNSINT16 Info_jbus  [NB_MAX_JBUS];NFS
    } jbus_scale;
  } scale;
#endif

#ifndef __GanAcqBuf_H
  
  typedef union {int bidon;} in2p3_buffer_struct;
  
#endif
#ifndef __GAN_TAPE_GENERAL_H
  typedef struct {int bidon;} gan_tape_desc;
#endif
}
#endif
