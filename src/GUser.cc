/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *       
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "GUser.hh"
#include "TROOT.h"
#include <TProfile.h> 
#include <TRandom.h>
#include <GParaCaliXml.h>
#include <TRint.h>
#include <TObject.h>
#include <TString.h>
#include <TH1.h>
#include <TMath.h>

GUser::GUser (GDevice* _fDevIn, GDevice* _fDevOut)
{ 
  START;

  fDevIn         = _fDevIn;
  fDevOut        = _fDevOut;
  NbOfHits=0;
  NbEvents=0;
  CalcMode = false;
  Float_t BrhoRef;

  L=NULL;
  An=NULL;
  PL = NULL;
  MyTree = NULL;

  LogManager *LM = LogManager::getInstance();
  LM->SetLogFile("Watcher.log");
  L = LM->GetFilePtr();

  //PL = new Parameters(16387);
  PL = new Parameters(30000);


#ifdef WITH_ROOT
  if(!CalcMode)
    H = new HistoManager();
  else 
    H = NULL;
#endif
  An = new Analysis(PL,H);

#ifndef WITH_NFS
#ifndef WITH_BLM
  cout << endl << "==========================================" << endl <<
    "Brho PLEASE : ";
  cin >> BrhoRef;
  if(BrhoRef >0. && BrhoRef < 4.4)
    An->SetBrhoWatcher(BrhoRef);
  else
    {
      cout <<"WRONG Brho !!" << endl;
      exit(-1);
    }
#endif
#endif
#ifdef WITH_MFM
  CurrentTS = 0;
  PreviousTS = 0;
  FirstTS = 0;

  frame = NULL;
  frame = new MFMCommonFrame();
  ebyframe = NULL;
  ebyframe= new MFMEbyedatFrame();

#endif
  
  fBadMerge=0;
#ifdef WITH_AGATA
  Agata = NULL;
  VTS = NULL;
#endif
  END;   
}

//_____________________________________________________________________________

GUser::~GUser()  {
  START;
  start = clock();
  
#ifdef WITH_MFM
  frame = CleanDelete(frame);
  ebyframe = CleanDelete(ebyframe);
#endif
  //Destructor of class GUser   
  gROOT->cd();
  An= CleanDelete(An);
  PL = CleanDelete(PL);
#ifdef WITH_ROOT
  if(!CalcMode)
    H = CleanDelete(H);
#endif
  L = CleanDelete(L);
  END;
}
void GUser::RetriveHistograms()
{
  START;
  // Loop over histogram of HistoManager
#ifdef WITH_ROOT
  if(!CalcMode)
    {
      cout << "Number of histos " << H->GetSize() << endl;
      for(Int_t i = 0; i < H->GetSize(); i++)
        if(H->GetHistogram(i))
          {
            GetSpectra()->AddSpectrum(H->GetHistogram(i),
                                      H->GetFolder(i).c_str()
                                      );
          }
    }
  else
    {
      Char_t Message[100];
      sprintf(Message,"Trying to Retrieve Histogram in Calc Mode");
      MErr * Er = new MErr(WhoamI,0,0, Message);
      throw Er;      
    }
#endif
  END;
}


void GUser::RetrivePointers()
{
  START;
  string Name;

  DetManager *DM = DetManager::getInstance();


  END;
}
UShort_t* GUser::GetDataAt(UShort_t Pos) 
{
  START;
  // Length is number of items compared to size

  ebyframe->EbyedatGetParameters(Pos, LabelData, LabelData+1);
  
  return LabelData;
  END;
}



void GUser::CheckAndAddEBYEData(Analysis* An, MFMEbyedatFrame *ebyframe)
{
  START;

  // Check That there are no zero Labels
  // Make and Check trigger Pattern 
  // CHeck Eventbuilder2MFM TS is equal to AGATA TS
  
  Int_t size = ebyframe->GetNbItems();
#ifdef TRIG_PAT  
  m_Pattern = 0;
#endif
  m_MFM_TS = 0;
 
#ifdef DEBUG
  cout << " ------------------------------------------ " << endl;
  cout << " EventNum " << ebyframe->GetEventNumber() << endl;
  cout << " EventSize " << size << endl;
  CurrentTS = ebyframe->GetTimeStamp();
  cout << " Time Stamp " << CurrentTS << endl;
  cout << " Time Stamp since start " << (CurrentTS-FirstTS)*1e-5 << " msecs "  << endl;
  cout << " DeltaTS " << CurrentTS-PreviousTS << endl;
  if(PreviousTS == 0)
    {
      FirstTS = CurrentTS;
      cout << "First TS" << endl;
    }
#endif
  
      PreviousTS = CurrentTS;

      // Loop Over Event and loof or Duplicates/Zero Labels
      
      for (Int_t i = 0; i < size; i++) { 
        ebyframe->EbyedatGetParameters(i, &Data[0], &Data[1]); 
        
//        cerr<<"loop ctrl "<<i<<"/"<<size-1 <<" Data[0]="<<Data[0]<<" Data[1]="<<Data[1] <<endl;	      
        if(Data[0] == 0) {  
          char Error[256];
          sprintf(Error,"LABEL Error : Label 0 found in the data MABEL: %d DATA : %d !",Data[0],Data[1]);
          printf("LABEL Error : Label 0 found in the data MABEL: %d DATA : %d ! :: I continue anyhow (FATIMA block)\n",Data[0],Data[1]);
          MErr * Er = NULL;
          Er = new(Er) MErr(WhoamI,0,0, Error);
          An->Clear();
          //  throw Er;

          cout << "\nEVENT wrong -- Zero  Label" << endl;

        } 
#ifdef DEBUG
          cout << "LABEL DATA " << Data[0] << " " << Data[1] << " size " << size << endl;
#endif
        An->Add(Data);


#ifdef TRIG_PAT
        for(UShort_t j = 0; j<m_TrigLbl.size(); j++)
          if(Data[0] == m_TrigLbl[j] && Data[1] > 0) 
            {
              m_Pattern |= (1<<j) ;
            }
        
#endif
        
      }

#ifdef TRIG_PAT
  //Get Trigger Pattern
  if((m_Pattern != m_PatMask))
    {
      fBadMerge++;
      cout << "\n ! Error in Merging "   << endl;
      for(UShort_t i = 0; i<m_TrigLbl.size() ; i++)
          cout << m_TrigName[i] << " : " << ((m_Pattern>>i)  & 1) << " - " ;  
      
      cout << m_Pattern << " -> Expecting : " << m_PatMask << "  (#" << fBadMerge << ")" << endl;       
      cout <<  endl;
      if(fBadMerge % 100 == 1)
        {
              if (system(NULL)) puts ("Ok");
              else exit (EXIT_FAILURE);
              Char_t command[2000];
              sprintf(command,"notify-send VamosWatcher \"Merge issue in VAMOS - %d \"",fBadMerge);
              int i=system (command);
              printf ("The value returned was: %d.\n",i);
        }
      
    }

      Data[0] = 16384;
      Data[1] = m_Pattern;
      //Add Data to Analysis 
      An->Add(Data);     
#endif

#ifdef WITH_MFM	
   m_MFM_TS = ebyframe->GetTimeStamp();
   // Store Time Stamp as parameter
   Data[0] = 16385;
   Data[1] = (UShort_t) (m_MFM_TS*1.e-5) ; // in mSec
#ifdef DEBUG
   cout <<  "Time Stamp" << Data[0] << " " << Data[1]  << " " <<  An->GetMap()->GetAddress(*Data) << endl;;
#endif
   An->Add(Data);
  
   Data[0] = 16386;
   Data[1] = (UShort_t) (ebyframe->GetEventNumber()) ; // in mSec
#ifdef DEBUG
   cout << "EventNum" <<  Data[0] << " " << Data[1]  << " " <<  An->GetMap()->GetAddress(*Data) << endl;
#endif
   An->Add(Data);
  
#endif


  END;
}

//______________________________________________________________

void GUser::InitUser()
{
  START;
  // Initialisation for global  user treatement
 
  END;

}
//______________________________________________________________

void GUser::InitUserRun()
{
  START;
  start_run = clock();
  
  Int_t NumberOfParameters = 0;
#ifdef WITH_MFM
  try
  {
      ConfManager *CM = ConfManager::getInstance();

      PL->GetParameters( CM->Get("ExpName").c_str());
      NumberOfParameters = PL->GetNParameters();
  }
  catch(MErr * Er)
  {

      Er->Print();
    NumberOfParameters = 0;
  }
#else
  // Initialisation for user treatemeant for each  run  
  // For specific user treatement
  cout << "Get NumberOfParameters " << endl <<flush;
  
  NumberOfParameters = GetEvent()->GetDataParameters()->GetNbParameters();
  
  for(Int_t i = 0; i < NumberOfParameters; i++) 
	 {
#ifdef DEBUG
      cout << i << "/" << NumberOfParameters << " " << GetEvent()->GetDataParameters()->GetLabel(i)<< " " 
           << GetEvent()->GetDataParameters()->GetParName(i) << " " 
           << GetEvent()->GetDataParameters()->GetNbits(i) << endl;
#endif
		PL->Add(GetEvent()->GetDataParameters()->GetLabel(i),
              GetEvent()->GetDataParameters()->GetParName(i),
              GetEvent()->GetDataParameters()->GetNbits(i)
              );
	 }

#endif

#ifdef DEBUG
  PL->Print();
#endif


#ifdef TRIG_PAT
  PL->Add(16384,"DAQ_PATTERN",13);
  NumberOfParameters++;

  // Build TrigPatter Vector

#ifdef TRIG_PAT
  m_Pattern =0;
#ifdef WITH_MUST2
  m_TrigName.push_back("MUST");
  m_TrigLbl.push_back(1);
  m_TrigName.push_back("BTD");
  m_TrigLbl.push_back(5000);
  m_TrigName.push_back("TGV");
  m_TrigLbl.push_back(6000);
  m_TrigName.push_back("VAMOS");
  m_TrigLbl.push_back(7000);

  // const short NTrigBranch = 4;
  // Char_t TrigName[NTrigBranch][20] = {"MUST","BTD","TGV","VAMOS"};
  // UShort_t fTrigLbl[NTrigBranch] = {1,5000,6000,7000};
#endif
#ifdef WITH_E641S
  m_TrigName.push_back("VXI_1");
  m_TrigLbl.push_back(1);
  m_TrigName.push_back("VXI_2");
  m_TrigLbl.push_back(1000);
  m_TrigName.push_back("TGV");
  m_TrigLbl.push_back(3000);
  m_TrigName.push_back("VAMOS");
  m_TrigLbl.push_back27000);
// const short NTrigBranch = 4;
//  Char_t TrigName[NTrigBranch][20] = {"VXI_1","VXI_2","TGV","VAMOS"};
//  UShort_t fTrigLbl[NTrigBranch] = {1,1000,3000,2000};
#endif

#ifdef WITH_AGATA
#ifdef WITH_E710
m_TrigName.push_back("VAMOS");
m_TrigLbl.push_back(1);
m_TrigName.push_back("DC1");
m_TrigLbl.push_back(1000);
m_TrigName.push_back("AGAVA");
m_TrigLbl.push_back(2000);
m_TrigName.push_back("DC2");
m_TrigLbl.push_back(3000);
m_TrigName.push_back("SPIDER");
m_TrigLbl.push_back(5004);
// const short NTrigBranch = 5;
// Char_t TrigName[NTrigBranch][20] = {"VAMOS","DC1","AGAVA","DC2","SPIDER"};
// UShort_t fTrigLbl[NTrigBranch] = {1,1000,2000,3000,5004};
#else  
#ifdef WITH_E667
m_TrigName.push_back("VAMOS");
m_TrigLbl.push_back(1);
m_TrigName.push_back("DC1");
m_TrigLbl.push_back(1000);
m_TrigName.push_back("AGAVA");
m_TrigLbl.push_back(2000);
m_TrigName.push_back("DC2");
m_TrigLbl.push_back(3000);
m_TrigName.push_back("MW");
m_TrigLbl.push_back(4000);
m_TrigName.push_back("DSA");
m_TrigLbl.push_back(6000);

// const short NTrigBranch = 6;
// Char_t TrigName[NTrigBranch][20] = {"VAMOS","DC1","AGAVA","DC2","MW","SPIDER"};
// UShort_t fTrigLbl[NTrigBranch] = {1,1000,2000,3000,4000,6000};
#else
  m_TrigName.push_back("MUST_MASTER");
  m_TrigLbl.push_back(1);
  m_TrigName.push_back("MUST_SLAVE1");
  m_TrigLbl.push_back(3000);
  m_TrigName.push_back("MUST_SLAVE2");
  m_TrigLbl.push_back(9000);
  m_TrigName.push_back("MUST_SLAVE3");
  m_TrigLbl.push_back(6000);
  m_TrigName.push_back("AGAVA_VAMOS");
   m_TrigLbl.push_back(13000);
   m_TrigName.push_back("AGAVA_AGATA");
   m_TrigLbl.push_back(14000);
   m_TrigName.push_back("VXI_VAMOS");
   m_TrigLbl.push_back(15000);
   // m_TrigName.push_back("CATS1");
   // m_TrigLbl.push_back(9000);
   // m_TrigName.push_back("CATS2");
   // m_TrigLbl.push_back(10000);

// const short NTrigBranch = 5;
// Char_t TrigName[NTrigBranch][20] = {"VAMOS","DC1","AGAVA","DC2","MW"};
// UShort_t fTrigLbl[NTrigBranch] = {1,1000,2000,3000,4000};
#endif
 
#endif
#endif

#ifdef WITH_AGATA2
m_TrigName.push_back("VAMOS");
m_TrigLbl.push_back(1);
m_TrigName.push_back("DC1");
m_TrigLbl.push_back(1000);
m_TrigName.push_back("AGAVA");
m_TrigLbl.push_back(2000);
m_TrigName.push_back("DC2");
m_TrigLbl.push_back(3000);
// const short NTrigBranch = 4;
// Char_t TrigName[NTrigBranch][20] = {"VAMOS","DC1","AGAVA","DC2"};
// UShort_t fTrigLbl[NTrigBranch] = {1,1000,2000,3000};
#endif

m_PatMask =0;
for(UShort_t k=0; k< m_TrigLbl.size();k++)
  m_PatMask |= (1<<k) ;

#endif
  


#endif		
#ifdef WITH_MFM	
  PL->Add(16385,"MFM_TS",13);
  NumberOfParameters++;
  PL->Add(16386,"MFM_EVTNUM",13);
  NumberOfParameters++; 
#endif		

  PL->SetNParameters(NumberOfParameters);

#ifdef DEBUG
  PL->Print();
#endif

  //  Data operation
  An->GetMap()->DataOp(PL);
  An->SetUpAnalysis();



  
#ifdef DEBUG
  An->GetMap()->Print();
#endif
  if(!CalcMode)
    RetriveHistograms();


  // Retrieve Pointer to object of Interest
      RetrivePointers();
  


  END;
}

//______________________________________________________________
void GUser::User()
{
//   cout <<"--------------< User Treatement >------------------\n";
  Bool_t IsCtrl = false;
  Double_t Time = 0.0;

  An->Clear();
  Bool_t fOK =true;
 
#ifdef WITH_GAMMAFP
  // Data Trig GFP Label
  UShort_t DTRIG_GFP = 5004;

#endif  


int size=0;  
  
#ifdef WITH_MFM
  MFMBasicFrame *frame = ((GEventMFM*) GetEvent())->GetFrame();
  int type = frame->GetFrameType();
  m_MFM_TS = 0;
  m_hasEBYEDAT = false;
  ULong64_t ATS = 0;
  

#ifdef DEBUG
  cout << "<MFMDataFile><Next>" << endl;
  cout << "Frame type :" << type  <<  endl;
#endif

  try{
      An->UnPack(frame);
    }
  catch(MErr *Er)
  {
      cout<< "Caught Exception at Event " << NbEvents << endl;
      Er->Print();
      cout << "Skipping Event" << endl;
      delete Er;
      // Skip Event From Fill
      return ;
  }

#else
  size =  GetEventArrayLabelValueSize()/2;
  
  for (Int_t i = 0; i < size; i++) { 
    Data[0] = GetEventArrayLabelValue_Label(i);
    Data[1] = GetEventArrayLabelValue_Value(i);
    An->Add(Data);
  }
#endif 
    
   An->Treat();
     
#ifndef WITH_NFS
#ifndef WITH_BLM
#ifdef WITH_AGATA
#ifdef WITH_MFM
   if(m_hasEBYEDAT)
     {
       //ATS = VTS->GetTS();// from 9-12-14 LTS in MFM frame instead od VTS
       ATS = LTS->GetTS(); // Local TS Used
//       if(ATS != m_MFM_TS)
//         cout << " \033[1;31m /!\\ Error : \033[0m Time Stamp Mismatch [ Event# "<<  NbEvents << "] [MFM : " << m_MFM_TS << "] - [AGAVA VTS: "<<  ATS << "]  [Diff = " << m_MFM_TS-ATS <<"]"<< endl;
     }
#endif
#endif
#endif
#endif
#ifdef DEBUG
  An->PrintRaw();
#endif

  if(!CalcMode)
    An->FillHistograms();

  NbEvents++;
 
   if(NbEvents%1000 == 0)
    { 
      end = clock();
      Time = ((double) (end-start_run)/CLOCKS_PER_SEC);
      cout << "\r    [] Events    : " << NbEvents  
           << " --- " << std::setprecision(2) << std::fixed << (double)NbEvents/Time 
           << " Evt/sec" 
           << flush;
     
    }

   if(NbEvents%100 == 0)
     {
       while(KBHit())        
         {
           if(getchar()==113) 
             {
               NbOfHits++;
               if(NbOfHits==3)
                 {
                   MErr * Er = new MErr(WhoamI,1,113, "You pressed qqq !");
                   throw Er;      
                 }
             }
           else
             NbOfHits=0;
         }
     }   


  // }
}
//______________________________________________________________
  //______________________________________________________________
  void GUser::EndUserRun()
{
  //  end of run ,  executed a end of each run

    cout <<"--------------< End User Run >------------------\n";
   
}

//______________________________________________________________
void GUser::EndUser()
{
  // globlal final end executed a end of runs
  // must be explicitly called! 

  
}
//______________________________________________________________________________

void GUser::InitTTreeUser()
{
  // User method for specfic initialisation of TTree
  // It can be usefull for example multi-hit detections
  // or to have a TTree with only few parameters ( for low compute)
  // to run this method , you have to slect mode 3 in  SetTTreeMode
  // ex : a->SetTTreeMode(3,"/space/MyTTree.root");
}

Int_t GUser::KBHit(void)
{
  struct termios oldt, newt;
  int ch;
  int oldf;

  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

  ch = getchar();

  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);

  if(ch != EOF)
  {
    ungetc(ch, stdin);
    return 1;
  }

  return 0;
}
