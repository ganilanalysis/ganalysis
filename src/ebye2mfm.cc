
#include "GanilDataFile.hh"
#include "LocalDefines.hh"
#include "Parameters.hh"
#include "ParManager.hh"
#include "EnvManager.hh"
#include "LogManager.hh"


#include "MFMDataFile.hh"
#include "MFMTypes.h"
#include "MFMCommonFrame.h"
#include "MFMBasicFrame.h"
#include "MFMBlobFrame.h"
#include "MFMEbyedatFrame.h"
#include "MFMMergeFrame.h"
#include "MFMScalerDataFrame.h"

int main(int argc, char** argv)
{

    UInt_t EvtNr =0;
    clock_t start, start_run, end;

    uint32_t unitBlock_size = 2;
    int type = MFM_EBY_EN_FRAME_TYPE;
    uint32_t revision = 1;
    uint32_t  itemsize = 4 ;
    uint32_t  headersize = EBYEDAT_EN_HEADERSIZE;
    uint32_t  nbitem = 0 ;
    uint32_t framesize = 0;
    MFMEbyedatFrame *ebyframe= new MFMEbyedatFrame();
    int WriteStatus = 0;
    MFMScalerDataFrame *scalerframe = new MFMScalerDataFrame();
    //    //scalerframe->SetWantedItemsNumber(16);
    string InFileName;
    string OutFileName;
    string OutACTIONFileName;
    try
    {
        if(argc == 4)
        {

            InFileName = argv[1];
            OutFileName = argv[2];

            OutACTIONFileName = argv[3];

            cout << "InFileName : " << InFileName << endl;
            cout << "OutFileName : " << OutFileName << endl;
            cout << "OutACTIONFileName : " << OutACTIONFileName << endl;


        }
        else
        {
            cout << argv[0] << " InFile OutFile" <<endl;
            exit(-1);
        }


        LogManager *LM = LogManager::getInstance();
        string LogFileName = "Log";
        LogFileName += ".log";
        LM->SetLogFile(LogFileName.c_str());
        //            L = LM->GetFilePtr();
        Parameters *PL = new Parameters(16387);
        Int_t BufferSize = 16384;

        GanilDataFile * GDF = new GanilDataFile(InFileName);
        ULong64_t FS = GDF->GetFileSize();

        GDF->GetParameters(PL);



        FILE *   OutACTIOMFMFile = fopen(OutACTIONFileName.c_str(), "w");
        if (OutACTIOMFMFile == NULL ) {
            cout << "Error cold not ooen " << OutACTIONFileName << endl;
            exit(-1);
        }


        cout << "\t Generating ACTION File " << OutACTIONFileName << endl;
        for(UShort_t i=0; i< PL->GetMaxParameters(); i++)
        {
            if(PL->GetParSize(i) > 0)
            {
//                cout << "ParamId : " << i << " - Name : " << PL->GetParName(i) << " \t (" << PL->GetParSize(i) << " bytes)" << endl;
                fprintf(OutACTIOMFMFile,"%s\t%d\t%d\t%d\n", PL->GetParName(i).c_str(), i,PL->GetParSize(i), 1 );
            }
        }
        fclose(OutACTIOMFMFile);

        Int_t OutMFMFile = open(OutFileName.c_str(), (O_RDWR | O_CREAT | O_TRUNC), 0644);
        if (OutMFMFile <= 0) {
            cout << "Error cold not ooen " << OutFileName << endl;
            exit(-1);
        }
        start_run = clock();


        while(GDF->Next())
        {
            //ebyframe->Clear();

            if(EvtNr%1000==0)
            {
                end = clock();

                cout << "\r    Events    : " << EvtNr
                     << " --- Read : "<< std::setprecision(2) << std::fixed <<  (double)GDF->GetMBytesRead()/((double)(FS))*100. << "%"
                     << " --- " << (double)GDF->GetMBytesRead()/((double)(end-start_run)/CLOCKS_PER_SEC)
                     << " Mb/sec " << flush;
            }
            WriteStatus = 0;
            nbitem = GDF->GetNItems();
            headersize = EBYEDAT_EN_HEADERSIZE;
            framesize = headersize +  nbitem * itemsize;

            //            cout << "frame size " << framesize << endl;;
            ebyframe->MFM_make_header(unitBlock_size, 0, type, revision,(framesize/ unitBlock_size), (headersize / unitBlock_size), itemsize,nbitem );
            ebyframe->SetAttributs();

            //            cout << "reading "<< GDF->GetIsCtrl() <<  endl;
            if(GDF->GetIsCtrl())
            {
                EvtNr++;
                ebyframe->SetEventNumber(EvtNr);
                for(Int_t k=0 ;k < nbitem;k++)
                {
                    //                    cout << "k " << k << " " << *(GDF->GetDataAt(k))  << " " << *(GDF->GetDataAt(k)+1) << endl;
                    ebyframe->EbyedatSetParameters(k,*(GDF->GetDataAt(k)),*(GDF->GetDataAt(k)+1));
                }
                //                cout <<  "----- " << ebyframe->GetEventNumber()<<endl;;
                //                ebyframe->HeaderDisplay();
                //                cout << ebyframe->GetDumpData() <<endl;


                WriteStatus = write(OutMFMFile, ebyframe->GetPointHeader(), framesize);
                if (WriteStatus != framesize)
                {

                    cout << "Bad Write " << endl;
                    exit(-1);
                }
            }

            if(strcmp (GDF->fHeader , SCALER_Id ) == 0 )
            {
                scale_struct Scaler;
                //cout << "Found Scaler " << endl;
                u_int32_t Val, Freq, Val1, Freq1;
                int ubs= SCALER_STD_UNIT_BLOCK_SIZE;

                int Channels = GDF->fBuffer->les_donnees.cas.scale.Nb_channel;
                //cout << endl;

                framesize = Channels*sizeof(MFM_ScalerData_Item)+SCALER_DATA_HEADERSIZE ;
                headersize = SCALER_DATA_HEADERSIZE;
                scalerframe->MFM_make_header(ubs, 0, MFM_SCALER_DATA_FRAME_TYPE, 1,(framesize/ ubs), (headersize/ ubs), itemsize,Channels );
                // scalerframe->SetFrameSize(framesize);
                // scalerframe->SetBufferSize(framesize*ubs,false);
                scalerframe->SetNbItem(Channels);
                scalerframe->SetPointers();
                scalerframe->SetAttributs();
                for(int i=0;i<Channels/2;i++)
                {
                    Scaler = ( scale_struct ) GDF->fBuffer->les_donnees.cas.scale.jbus_scale.UnScale[i+1] ;
                    Val = Scaler.Count ;
                    Freq = Scaler.Freq ;
                    Scaler = ( scale_struct ) GDF->fBuffer->les_donnees.cas.scale.jbus_scale.UnScale[i+Channels/2] ;
                    Val1 = Scaler.Count ;
                    Freq1 = Scaler.Freq ;
                    // cout << i << " " << Val << " " << Freq << " \t\t " << i+Channels/2 << " " << Val1 << " " << Freq1 << endl;
                    // scalerframe->SetValues(i, 1, 1, 1,1,1, 1);
                    // scalerframe->SetValues(2*i, 0, 0, 0,0,0,0);
                     scalerframe->SetValues(i, Val, Freq, 0,0,0, 0);
                     scalerframe->SetValues(i+Channels/2, Val1, Freq1, 0,0,0,0);
                     // ((MFM_ScalerData_Item*)scalerframe->GetItem(i))->Label = (unsigned int) i;
                    // ((MFM_ScalerData_Item*)scalerframe->GetItem(i))->Count = (unsigned int) Val;
                    // ((MFM_ScalerData_Item*)scalerframe->GetItem(i))->Frequency = Freq;
                    // ((MFM_ScalerData_Item*)scalerframe->GetItem(i))->Status = 1;
                    // ((MFM_ScalerData_Item*)scalerframe->GetItem(i))->Tics = 0;
                    // ((MFM_ScalerData_Item*)scalerframe->GetItem(i))->AcqStatus = 0;

                    // ((MFM_ScalerData_Item*)scalerframe->GetItem(i+Channels/2))->Label = i+Channels/2;
                    // ((MFM_ScalerData_Item*)scalerframe->GetItem(i+Channels/2))->Count = Val1;
                    // //  ((MFM_ScalerData_Item*)scalerframe->GetItem(i+Channels/2))->Frequency = Freq1;
                    // ((MFM_ScalerData_Item*)scalerframe->GetItem(i+Channels/2))->Status = 1;
                    // ((MFM_ScalerData_Item*)scalerframe->GetItem(i+Channels/2))->Tics = 0;
                    // ((MFM_ScalerData_Item*)scalerframe->GetItem(i+Channels/2))->AcqStatus = 0;
                  

                }
                // cout << scalerframe->GetDumpData('d'); 
                
                // for(int i=0;i<Channels;i++)
                //   {
                //     cout <<  (scalerframe->GetItem(i)) << endl;
                //     cout <<  ((MFM_ScalerData_Item*)scalerframe->GetItem(i))->Label 
                //          << "\t" << ((MFM_ScalerData_Item*)scalerframe->GetItem(i))->Count 
                //     << "\t" << ((MFM_ScalerData_Item*)scalerframe->GetItem(i))->Frequency 
                //     << "\t" << ((MFM_ScalerData_Item*)scalerframe->GetItem(i))->Status
                //     << "\t" << ((MFM_ScalerData_Item*)scalerframe->GetItem(i))->Tics  
                //     << "\t" << ((MFM_ScalerData_Item*)scalerframe->GetItem(i))->AcqStatus << endl;
                //   }
                // cout << "Found Scaler End" << endl;
                // cout << scalerframe->GetDumpData(); 

                WriteStatus = write(OutMFMFile, scalerframe->GetPointHeader(), framesize);
                if (WriteStatus != framesize)
                {

                    cout << "Bad Write " << endl;
                    exit(-1);
                }

            }
        }

        cout << "Number of Event " << EvtNr << endl;

        delete ebyframe;
        delete scalerframe;
        delete PL;
        delete GDF;

        OutMFMFile = close(OutMFMFile);

    }
    catch(MErr * Er)
    {
        if(Er->FunctionCode==1&&Er->ErrorCode==1)
            ;//O.K. to exit without a Message
        else
        {
            Er->Set(WhoamI);
            Er->Print();
        }
        Er = CleanDelete(Er);
    }
    catch(exception &e)
    {
        cout << "Caught Exception : " << e.what() << endl;
    }
    catch(...)
    {
        cout << "Unkown Exception : "  << endl;
    }
    return 0;

}

