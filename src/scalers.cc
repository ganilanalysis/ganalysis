#include "MFMFileReader.h"
#include "MFMMergeFrameManager.h"
#include "MFMScalerDataFrame.h"
#include "MFMEbyedatFrame.h"

//! Dump to stdout contents of frame. If dump=false, just print header.
//! If frame is a merge frame, we recursively dump the contents
template <typename FrameHandler>
void RecursivelyDisplayFrameRead(FrameHandler& fr, bool dump=false)
{
   static MFMMergeFrameManager mfm;
   if(dump) fr.PrintFrameRead();
   else fr.PrintFrameReadHeader();
   if(fr.IsFrameReadMerge())
   {
      mfm.SetMergeFrame(fr.GetFrameRead());
      while( mfm.ReadNextFrame() ){
         RecursivelyDisplayFrameRead(mfm,dump);
      }
   }
}

int main(int argc, char** argv)
{
   if(argc<2){
      printf("mfm_test: Read all or some frames in an MFM data file\n\n");
      printf("Usage: mfm_test [options] [runfile]\n");
      printf("\nOptions:\n");
      printf("    -n [N]   : number of frames to read\n");
      printf("               if N<0, only print frame headers\n");
      printf("    -N [N]   : print after reading N frames to end\n");
      printf("               if N<0, only print frame headers\n");
      printf("    -a [dir] : directory to search for Ebyedat ACTIONS files\n");
      printf("               by default, look in same directory as data\n");
      return 0;
   }

   int nframes = -1;
   string actions_file;
   string filename;
   bool dump=true;
   bool read_to_end=false;
   if(argc>2) {
      int argn = 1;
      while( argn < argc ){
         cout << argn << " |" << argv[argn] << "|\n";
         if(string(argv[argn])=="-a")
         {
            actions_file=argv[argn+1];
            argn+=2;
         }
         else if(string(argv[argn])=="-n")
         {
            nframes = atoi(argv[argn+1]);
            if(nframes<0){
               dump=false;
               nframes=-nframes;
            }
            argn+=2;
         }
         else if(string(argv[argn])=="-N")
         {
            nframes = atoi(argv[argn+1]);
            if(nframes<0){
               dump=false;
               nframes=-nframes;
            }
            read_to_end=true;
            argn+=2;
         }
         else
         {
            filename=argv[argn];
            argn++;
         }
      }
   }
   MFMFileReader fr(filename);
   if(actions_file!="")
   {
      fr.GetFrameLibrary().SetEbyedatActionsDirectory(actions_file);
   }
   int i=0;
   while( fr.ReadNextFrame() )
   {
      ++i;
      std::vector <uint64_t> vcounts;
      std::vector <uint32_t> vlabels;

      if( fr.GetFrameReadType() == MFM_SCALER_DATA_FRAME_TYPE )
    {
      MFMScalerDataFrame & sca = fr.GetFrameRead<MFMScalerDataFrame>();
      sca.DumpData(); // do EBYEDAT-specific work...


      uint64_t count;
      uint32_t label;
      int32_t status;
      uint64_t frequency;
      uint64_t tics;
      int32_t acqstatus;
      int reste;
      char one = '1', zero = '0';
      int DecNumber = 0;

      for (i = 0; i < sca.GetNbItems(); i++) {
        label = 0;
        count = 0;
        status = 0;
        frequency = 0;
        tics = 0;
        vcounts.clear();
        vlabels.clear();

        sca.GetValues(i, &label, &count, &frequency, &status, &tics, &acqstatus);

        {
          vcounts.push_back(count);
          vlabels.push_back(label);
          cout << i << " " << label << " " << count <<  endl;
        }
      }

    }
   }

}
