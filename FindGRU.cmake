# - Find GRU (Ganil ROOT Utilities) instalation
# This module tries to find the GRU installation on your system.
# It tries to find the gru-config script which gives you all the needed information.
# If the system variable GRUDIR is set this is straight forward.
# If not the module uses the pathes given in GRU_CONFIG_SEARCHPATH.
# If you need an other path you should add this path to this variable.  
# The root-config script is then used to detect basically everything else.
# This module defines a number of key variables and macros.

# Antoine Lemasson 1/4/2013


MESSAGE(STATUS "Looking for GRU (Ganil ROOT Utilities)...")

SET(GRU_CONFIG_SEARCHPATH
  $ENV{GRUDIR}/bin
  $ENV{GRUDIR}/config
  )


SET(GRU_DEFINITIONS "")

SET(GRU_INSTALLED_VERSION_TOO_OLD FALSE)

SET(GRU_CONFIG_EXECUTABLE GRU_CONFIG_EXECUTABLE-NOTFOUND)

#FIND_PROGRAM(GRU_CONFIG_EXECUTABLE NAMES gru-config PATHS
FIND_PROGRAM(GRU_CONFIG_EXECUTABLE NAMES GRU PATHS
  ${GRU_CONFIG_SEARCHPATH}
  NO_DEFAULT_PATH)


IF (${GRU_CONFIG_EXECUTABLE} MATCHES "GRU_CONFIG_EXECUTABLE-NOTFOUND")
#  MESSAGE( FATAL_ERROR "GRU not installed in the searchpath and GRUDIR is not set. Please
  MESSAGE( "GRU not installed in the searchpath and GRUDIR is not set. Please
 set GRUDIR or add the path to your GRU installation in the Macro FindGRU.cmake in the
 subdirectory cmake/modules.")
ENDIF (${GRU_CONFIG_EXECUTABLE} MATCHES "GRU_CONFIG_EXECUTABLE-NOTFOUND")  

IF (GRU_CONFIG_EXECUTABLE)

IF(WITH_GRUCONF)
  SET(GRU_FOUND FALSE)
   MESSAGE(STATUS "Looking for GRU... - found $ENV{GRUDIR}/")
   
   EXEC_PROGRAM(${GRU_CONFIG_EXECUTABLE} ARGS "--version" OUTPUT_VARIABLE GRUVERSION)
   IF (NOT GRUVERSION)
      MESSAGE(FATAL_ERROR "Could not determine the version of GRU")
   ENDIF (NOT GRUVERSION)
   MESSAGE(STATUS "Looking for GRU... - version ${GRUVERSION} ")   
   
   # we need at least version 13/03, could be set outside
   IF (NOT GRU_MIN_VERSION)
     SET(GRU_MIN_VERSION "13.03")
   ENDIF (NOT GRU_MIN_VERSION)
 
   # now parse the parts of the user given version string into variables
   STRING(REGEX REPLACE "^([0-9][0-9]+)\\.[0-9][0-9]+" "\\1" req_gru_major_vers "${GRU_MIN_VERSION}")
   STRING(REGEX REPLACE "^[0-9][0-9]+\\.([0-9][0-9])+" "\\1" req_gru_minor_vers "${GRU_MIN_VERSION}")
   #  STRING(REGEX REPLACE "^[0-9]+\\.[0-9][0-9]+\\/([0-9][0-9]+)" "\\1" req_gru_patch_vers "${GRU_MIN_VERSION}")
 
   # and now the version string given by gru-config
   STRING(REGEX REPLACE "^([0-9][0-9]+)\\.[0-9][0-9]+" "\\1" found_gru_major_vers "${GRUVERSION}")
   STRING(REGEX REPLACE "^[0-9][0-9]+\\.([0-9][0-9])+" "\\1" found_gru_minor_vers "${GRUVERSION}")
   #  STRING(REGEX REPLACE "^[0-9]+\\.[0-9][0-9]+\\/([0-9][0-9]+).*" "\\1" found_gru_patch_vers "${GRUVERSION}")
 
   IF (found_gru_major_vers LESS 12)
     MESSAGE( FATAL_ERROR "Invalid GRU version \"${GRUVERSION}\", at least major version 12 is required, e.g. \"12.00\"")
   ENDIF (found_gru_major_vers LESS 12)
   # compute an overall version number which can be compared at once
   MATH(EXPR req_vers "${req_gru_major_vers}*100 + ${req_gru_minor_vers}")
   MATH(EXPR found_vers "${found_gru_major_vers}*100 + ${found_gru_minor_vers}")
   IF (found_vers LESS req_vers)
     SET(GRU_FOUND FALSE)
 	 MESSAGE( FATAL_ERROR "Invalid GRU version \"${GRUVERSION}\", minimum vesion \"${GRU_MIN_VERSION}\" is required")
     SET(GRU_INSTALLED_VERSION_TOO_OLD TRUE)
   ELSE (found_vers LESS req_vers)
     SET(GRU_FOUND TRUE)
   ENDIF (found_vers LESS req_vers)

ELSE(WITH_GRUCONF)	
  SET(GRU_FOUND TRUE)	
ENDIF(WITH_GRUCONF) 
ENDIF (GRU_CONFIG_EXECUTABLE)


IF (GRU_FOUND)

#### Temporary
IF(WITH_GRUCONF)

   # Set GRU_LIBRARY_DIR
   EXEC_PROGRAM( ${GRU_CONFIG_EXECUTABLE}
     ARGS "--libdir"
     OUTPUT_VARIABLE GRU_LIBRARY_DIR_TMP )
   
   IF(EXISTS "${GRU_LIBRARY_DIR_TMP}")
     SET(GRU_LIBRARY_DIR ${GRU_LIBRARY_DIR_TMP} )
   ELSE(EXISTS "${GRU_LIBRARY_DIR_TMP}")
     MESSAGE(FATAL_ERROR "${GRU_CONFIG_EXECUTABLE} reported ${GRU_LIBRARY_DIR_TMP} as library path, but ${GRU_LIBRARY_DIR_TMP} does NOT exist, GRU must NOT be installed correctly.")
   ENDIF(EXISTS "${GRU_LIBRARY_DIR_TMP}") 
   
   # Set GRU_BIN_DIR
   EXEC_PROGRAM(${GRU_CONFIG_EXECUTABLE}
     ARGS "--bindir"
     OUTPUT_VARIABLE gru_bins )
   SET(GRU_BIN_DIR ${gru_bins})
 	 
   # Set GRU_INCLUDE_DIRS
   EXEC_PROGRAM( ${GRU_CONFIG_EXECUTABLE}
     ARGS "--incdir" 
     OUTPUT_VARIABLE gru_headers )
   SET(GRU_INCLUDE_DIRS ${gru_headers})
   SET(GRU_INCLUDES ${gru_headers})
   
   EXEC_PROGRAM( ${GRU_CONFIG_EXECUTABLE}
     ARGS "--libs" 
     OUTPUT_VARIABLE gru_flags )
   
   SET(GRU_LIBRARIES ${gru_flags})
   MESSAGE(STATUS "Looking for GRU... - found $ENV{GRUDIR}/bin/GRU")


ELSE(WITH_GRUCONF)
  # Set GRU_LIBRARY_DIR
  SET(GRU_LIBRARY_DIR $ENV{GRUDIR}/lib)
  # Set GRU_BIN_DIR
  SET(GRU_BIN_DIR $ENV{GRUDIR}/bin)
  # Set GRU_INCLUDE_DIRS
  SET(GRU_INCLUDES $ENV{GRUDIR}/include)
  SET(GRU_INCLUDE_DIRS
  $ENV{GRUDIR}/include
  $ENV{GRUDIR}/sources
  $ENV{GRUDIR}/vigru
  )
  
  SET(GRU_LIBRARIES "-L${GRU_LIBRARY_DIR} -lGRU")
  MESSAGE(STATUS "Looking for GRU... - found $ENV{GRUDIR}/bin/GRU")
  MESSAGE(STATUS " INCLUDE  ${GRU_INCLUDE_DIRS}")
  
ENDIF(WITH_GRUCONF)

  # Make variables changeble to the advanced user
  MARK_AS_ADVANCED( GRU_LIBRARY_DIR GRU_INCLUDE_DIRS GRU_DEFINITIONS)

ENDIF(GRU_FOUND)
