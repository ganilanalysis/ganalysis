DOXY_DIR=ganalysis.doxygen
DOXY_FILE=ganalysis-dox-file.cfg

if [[ -v GANALYSIS_SOURCE ]]; then
echo $GANALYSIS_SOURCE
else
GANALYSIS_SOURCE=$PWD/..
echo $GANALYSIS_SOURCE
fi

mkdir -p ./$DOXY_DIR
mkdir -p $DOXY_DIR/images

cp $GANALYSIS_SOURCE/doc/$DOXY_FILE ./
cp $GANALYSIS_SOURCE/doc/css/*.css ./
cp $GANALYSIS_SOURCE/doc/layout/* ./
cp -r $GANALYSIS_SOURCE/doc/mdfiles $DOXY_DIR/
#cp -r $GANALYSIS_SOURCE/doc/images $DOXY_DIR
#cp $GANALYSIS_SOURCE/icons/* $DOXY_DIR/images/
cp $GANALYSIS_SOURCE/doc/images/logo.png $DOXY_DIR/images/logo.png

mkdir -p $DOXY_DIR/examples
cp -r $GANALYSIS_SOURCE/src $DOXY_DIR

cp $GANALYSIS_SOURCE/doc/mdfiles/*.md ./

find $DOXY_DIR/src \( -name '*.hpp' -o -name '*.json' -o -name '*LinkDef.h' -o -name '*.md'  -o -name '*.txt' -o -name '*.cmake' -o -name 'sqlite3' \) -exec rm -rf {} +
doxygen $DOXY_FILE
