/** @page install Download and Install

## Prerequisites

### Operating systems

Linux (Ubuntu, Scientific Linux, CentOS, Debian, ...) and MacOS X operating systems are supported. No support for Windows yet.

### Basic software

You will need :

* [git](http://git-scm.com/) to download the sources

* [cmake](https://cmake.org/) to configure the build (at least version 3.5) 

* a compiler supporting [C++17](https://en.cppreference.com/w/cpp/compiler_support/17)

* if linked with [ROOT](https://root.cern/), a ROOT version at least version 6.20

## Getting the sources

GAnalysis is available to download and build from source from our [GitLab repository](https://gitlab.in2p3.fr/ganilanalysis/ganalysis).

Assuming installation of GAnalysis is the following repository:

```shell
cd /path/to/mysofts/GAnalysis
```

First *clone* the repository by typing the following command:

```shell
git clone https://gitlab.in2p3.fr/ganilanalysis/ganalysis.git sources
```

This will create a directory called **sources** containing the source code.

## Build and install

### cmake configuration

In the folder GAnalysis (see above section), create the build and install directories:

```shell
mkdir build install
cd build
cmake -DCMAKE_INSTALL_PREFIX=../install ../sources
```

Expected output would be similar to:

```
...
sample output

```

### Advanced options

The following options can be given at the cmake configuration level (with default values):

```cmake
option(WITH_ROOT "compile GAnalysis using ROOT (if installed)" ON)
option(WITH_MFM  "Complile GAnalysis using MFM libraries (if installed" ON)
option(WITH_MESYTEC_DATA "Compile GAnalysis with Mesytec data (if installed)" OFF)
option(WITH_VAMOS  "Compile GAnalysis with VAMOS libraries from Ganidetector Libraries (if installed)" OFF)
option(WITH_LISE  "Compile GAnalysis with LISE libraries from Ganidetector Libraries (if installed)" OFF)
option(WITH_NFS   "Compile GAnalysis with NFS libraries from Ganidetector Libraries (if installed)" OFF)
option(WITH_FALSTAFF  "Compile GAnalysis with FALSTAFF libraries from Ganidetector Libraries (if installed)" OFF)
option(WITH_PISTA  "Compile GAnalysis with PISTA libraries from Ganidetector Libraries (if installed)" OFF)
```

For example, to not link GAnalysis with ROOT and used VAMOS and FALSTAFF libraries, use:

```bash
cmake -DCMAKE_INSTALL_PREFIX=../install -DWITH_ROOT=ON -DWITH_VAMOS=ON -DWITH_FALSTAFF=ON ../sources
```

### Compile and install

```bash
make -jN install
```

With N being the number of processors available. On a single-core machine, just type `make install`. On a multi-processor machine, the build process can be considerably speeded-up by performing the compilation in parallel. For example, on a quad-core machine, type `make -j4 install` etc...

### Possible compilation issues

In case you compiler does not support c++17, or if ROOT has been compiled with c++11, a patch can be applied to compile GAnalysis with C++11:

```bash
cmake -DCMAKE_INSTALL_PREFIX=../install -DUSE_CXX11=ON ../sources
```

## Setting up the environment

After installing, you can use one of the following scripts to set up the necessary paths on your system to find the executables and run-time libraries. Choose the right script for your shell:

```shell
source /path/to/mysofts/GAnalysis/install/Env.csh      [(t)csh shell]
source /path/to/mysofts/GAnalysis/install/Env.sh       [(ba)sh shell]
```

After doing this you should be able to do e.g.:

```
GAna

```

## Default Configuration Files
Can be found in *share* directory.


